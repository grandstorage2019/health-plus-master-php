<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/setLang/{get_lang}', 'LocalizationController@setLang');
//----login----
Route::get('/login', 'Auth\LoginController@admin_login_form')->name('login');
Route::post('/login', 'Auth\LoginController@admin_login');

Route::get('/',function(){
  return redirect('login');
});

// Route::get('/setLang/{get_lang}', 'LocalizationController@setLang');
 


// Route::group(['prefix'=>'Country'],function(){
//     Route::resource('/', 'CountryController');
//     Route::post('/store', 'CountryController@store');
//     Route::post('/update', 'CountryController@update');
//     Route::get('/delete/{id}', 'CountryController@destroy');
//     Route::get('/search/{val}', 'CountryController@search');
// });

Route::group(['prefix'=>'Governorate'],function(){
    Route::resource('/', 'GovernorateController');
    Route::get('/get_by_country_id/{val}', 'GovernorateController@get_governora_by_country_id');
});
