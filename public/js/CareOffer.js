

$(document).ready(function() {
  $('#create_form').validate();
  $('#edit_model').validate();
  $('.select2').select2({ width: '100%' });
});

$('#btn_create').click(function(event) {
  $('#create_model').modal('show');
});


function edit_model(id,link,image,care_id)
{
    $('#edit_model').modal('show');
    $('#edit_id').val(id);
    $('#edit_link').val(link);
    $('#edit_care_id').val(care_id); console.log(care_id);
    $('#edit_img_temp').attr('src',CareOffer_image_path+'/'+image);  // image---
}


function change_image(input)
{
     if (input.files && input.files[0])
     {
         var reader = new FileReader();

         reader.onload = function (e) { 
              $(input).parents('form').find('img').attr('src', e.target.result);
         }
         reader.readAsDataURL(input.files[0]);
     }
 }
