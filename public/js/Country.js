

$(document).ready(function() {
  $('#create_form').validate();
  $('#edit_model').validate();  
});

$('#btn_create').click(function(event) {
  $('#create_model').modal('show');
});


function edit_model(id,name_ar,name_en)
{
    $('#edit_model').modal('show');
    $('#edit_id').val(id);
    $('#edit_name_ar').val(name_ar);
    $('#edit_name_en').val(name_en);
}
