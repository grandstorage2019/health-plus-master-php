
var myVue = new Vue({
    el: '#myVue',
    data:{
      specialization: get_specialization_id,
      sub_specializations: get_sub_specialization,
      Clicnic_images: doctor_ClinicPicture,
      specific_date: false,
      ddl_available_date: get_available_date,
      elected_subSpecialization_ids: doctor_selected_subSpecialization_ids,
      licencses: get_Licenses, 
    },//End data
    mounted(){
      $(".datepicker").datepicker({format: 'yyyy-mm-dd'});
      $('#create_form').validate({
        rules: {
            password_again: {
              equalTo: "#password"
            }}
      });
      if (this.ddl_available_date == 'specific')
      {
          this.specific_date = true;
      }

    },//End mounted
    methods:{
        change_specialization: function(){
              this.sub_specializations = [];
            $.get(get_SubSpecialization_api+'/'+myVue.specialization,function(responce){
                myVue.sub_specializations = responce;
            });
        },
        add_Clicnic_images_row: function(){
            this.Clicnic_images.push({ id:'',image:'' });
        },
        del_Clicnic_images: function(index){
            // this.Clicnic_images.splice(index,1);
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
        },
        add_licencse_images_row: function(){
             if(this.licencses.length <5)
             {
               this.licencses.push([]);
             }
        },
        del_licencse_images: function(index){
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
        },
        change_available_date: function(){
            this.specific_date = false;
            if (this.ddl_available_date == 'specific')
            {
                this.specific_date = true;
            }
        },
        time_type_change: function(input){
          console.log(input);
          // console.log( $(input).val() );
        }
    }//End methods
});

$(document).ready(function() {
    $('.available_time_table tr').each(function(index, el) {
          var inp_wating_time = $(this).find('.inp_wating_time');
          var inp_num_resrvation = $(this).find('.inp_num_resrvation');
          var ddl_time_type = $(this).find('.ddl_time_type');
          if (ddl_time_type.val() == 'specific time' )
          {
              inp_num_resrvation.prop('disabled', true);
          }
          else if ( ddl_time_type.val() == 'first reservation' )
          {
              inp_wating_time.prop('disabled', true);
          }
    });
});//End ready

$('.ddl_time_type').change(function(event) {
    if ( $(this).val() == 'first reservation' )
    {
        $(this).parents('tr').find('.inp_wating_time').prop('disabled', true);
        $(this).parents('tr').find('.inp_num_resrvation').prop('disabled',false  );
    }
    else {
      $(this).parents('tr').find('.inp_wating_time').prop('disabled', false);
      $(this).parents('tr').find('.inp_num_resrvation').prop('disabled', true );
    }
});


//----------------------------------------
function show_image(input,from)
{
       if (input.files && input.files[0])
       {
           var reader = new FileReader();

           reader.onload = function (e) {
               if (from == 'profile_image')
               {
                    $('#img_profile_image').prop('src', e.target.result);
               }
               else if (from == 'licencse')
               {
                    $('#licencse').prop('src', e.target.result);
               }
               else if (from == 'Clicnic_images')
               {
                    $(input).parents('tr').find('img').prop('src', e.target.result);
               }
               else if (from == 'license_images')
               {
                    $(input).parents('tr').find('img').prop('src', e.target.result);
               }
           }
           reader.readAsDataURL(input.files[0]);
       }
}

//---------------------map------------------
function initMap()
{

      var myLatlng = new google.maps.LatLng(get_lat,get_lang);
      var mapOptions = {
       zoom: 13,
       center: myLatlng
      }

      var map = new google.maps.Map(document.getElementById("google_ptm_map"), mapOptions);

      var marker = new google.maps.Marker({
         position: myLatlng,
         map: map,
         draggable:true,
      });

      google.maps.event.addListener(marker, 'dragend', function(event) {
        var myLatLng = event.latLng;
         var lat = myLatLng.lat();
         var lng = myLatLng.lng();

         document.getElementById("latitude").value = lat;

         document.getElementById("longitude").value = lng;
      });
}
