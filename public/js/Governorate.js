var myVue = new Vue({
  'el': '#myVue',
  'data':{
      ddl_country_id: '' ,
      show_content: false,
      governora_list: []
  },
  mounted(){
      $('.select2').select2();
      $('#ddl_country_id').change(function(event) {
          myVue.ddl_country_id = $(this).val();
          myVue.change_country();
      });
      $('#create_form').validate();
  },//End mounted()
  methods:{
      change_country: function(){
          this.governora_list = [];
          this.show_content = false;
          $('#table_Governorate tr').html('');
          $.get(get_governora_by_country_id+'/'+myVue.ddl_country_id, function(responce){ console.log(responce);
              myVue.governora_list = responce;
              myVue.show_content = true;
          });
      },
      add_Governorate_row: function(){
          myVue.governora_list.push({ name_en:'' ,name_ar:'',status:true });
          // $('#table_Governorate tbody').append(row);
      },
      del_row: function(index){
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
            // this.governora_list.splice(index,1);
      },
      do_submit: function(event){
          $('input').removeClass('inp_error');
          if ( $('#table_Governorate tr').length == 0 )
          {
              event.preventDefault();
          }
          else
          {
              var is_valid = true;
              $('#table_Governorate input[required]').each(function(index, el) {
                    if ( $(this).val().trim() == '' )
                    {
                        is_valid = false;
                        $(this).addClass('inp_error');
                    }
              });
              if (is_valid == false)
              {
                event.preventDefault();
              }
              if (is_valid)
              {
                  $("#create_form").find('input[type="checkbox"]').each( function () {
                      var checkbox_this = $(this);
                      if( checkbox_this.is(":checked") == true ) {
                          checkbox_this.prop('value','on');
                      } else {
                          checkbox_this.prop('checked',true);
                          //DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA
                          checkbox_this.prop('value','off');
                      }
                      checkbox_this.hide();
                  });
              }
          }
      }
  }//End methods
})
