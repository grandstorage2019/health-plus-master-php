

$(document).ready(function() {
  $('#create_form').validate();
  $('#edit_model').validate();
});

$('#btn_create').click(function(event) {
  $('#create_model').modal('show');
});


function edit_model(id,title_en,title_ar,link,is_promited,image)
{
    $('#edit_model').modal('show');
    $('#edit_id').val(id);
    $('#edit_title_en').val(title_en);
    $('#edit_title_ar').val(title_ar);
    $('#edit_link').val(link);

    $('#edit_img_temp').attr('src',Advertising_image_path+'/'+image);  // image---

    if(is_promited == '1')
      { $('#edit_is_promited').prop('checked', true); }
    else {
      $('#edit_is_promited').prop("checked",false);
    }
}


function change_image(input)
{
     if (input.files && input.files[0])
     {
         var reader = new FileReader();

         reader.onload = function (e) {
              // $('#create_img_temp').attr('src', e.target.result);
              $(input).parents('form').find('img').attr('src', e.target.result);
         }
         reader.readAsDataURL(input.files[0]);
     }
 }
