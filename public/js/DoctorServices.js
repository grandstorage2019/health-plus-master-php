var myVue = new Vue({
  'el': '#myVue',
  'data':{
      list_items: get_list,
  },
  mounted(){
      $('.select2').select2();
      $('#create_form').validate();
  },//End mounted()
  methods:{
      add_row: function(){
          myVue.list_items.push({
            name_en:'', name_ar:'' , status:1
          });
      },
      del_row: function(index){
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
      },
      do_submit: function(event){
          $('input').removeClass('inp_error');
          if ( $('#my_table tr').length == 0 )
          {
              event.preventDefault();
          }
          else
          {
              var is_valid = true;
              $('#my_table input[required]').each(function(index, el) {
                    if ( $(this).val().trim() == '' )
                    {
                        is_valid = false;
                        $(this).addClass('inp_error');
                    }
              });
              if (is_valid == false)
              {
                event.preventDefault();
              }
          }
          if (is_valid)
          {
              $("#create_form").find('input[type="checkbox"]').each( function () {
                  var checkbox_this = $(this);
                  if( checkbox_this.is(":checked") == true ) {
                      checkbox_this.prop('value','on');
                  } else {
                      checkbox_this.prop('checked',true);
                      //DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA
                      checkbox_this.prop('value','off');
                  }
                  checkbox_this.hide();
              });
          }
      }
  }//End methods
});



//----------------------------------------------
