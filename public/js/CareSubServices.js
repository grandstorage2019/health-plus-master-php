var myVue = new Vue({
  'el': '#myVue',
  'data':{
      ddl_services_id: '',
      ddl_care_id: '',
      sub_services_list: [],
      show_content: ''
  },
  mounted(){
      $('.select2').select2();
      // $('#ddl_country_id').change(function(event) {
      //     myVue.ddl_country_id = $(this).val();
      //     myVue.change_country();
      // });
      $('#create_form').validate();
  },//End mounted()
  methods:{
      add_SubService_row: function(){
          myVue.sub_services_list.push({
            name_en:'', name_ar:'', quantity:'', price:'', status:1 , image:'',unit_en:'',unit_ar:''
          });
      },
      del_row: function(index){
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
      },
      change_services: function(){
          $.get(get_SubService_by_id+'/'+myVue.ddl_services_id,function(responce){
              myVue.sub_services_list = responce;
              myVue.show_content = true;
          });
      },
      do_submit: function(event){
          $('input').removeClass('inp_error');
          if ( $('#table_Governorate tr').length == 0 )
          {
              event.preventDefault();
          }
          else
          {
              var is_valid = true;
              $('#table_Governorate input[required]').each(function(index, el) {
                    if ( $(this).val().trim() == '' )
                    {
                        is_valid = false;
                        $(this).addClass('inp_error');
                    }
              });
              if (is_valid == false)
              {
                event.preventDefault();
              }
          }
          if (is_valid)
          {
              $("#create_form").find('input[type="checkbox"]').each( function () {
                  var checkbox_this = $(this);
                  if( checkbox_this.is(":checked") == true ) {
                      checkbox_this.prop('value','on');
                  } else {
                      checkbox_this.prop('checked',true);
                      //DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA
                      checkbox_this.prop('value','off');
                  }
                  checkbox_this.hide();
              });
          }
      }
  }//End methods
});



//----------------------------------------------

$("#ddl_care_id").change(function(event) {
          myVue.ddl_services_id = $(this).val();
          myVue.show_content = false;
        // $('#btn_submit').addClass('hide');
        // var is_empty = true;
        $.get(get_services_by_id+'/'+$(this).val(), function(data){
            console.log(data);
            var name, select, option ;
            select = document.getElementById('ddl_services_id');
            // Clear the old options
            select.options.length = 0;
            // foreach(data as key =)
            select.options.add(new Option('' , '')); //choose sub category
            for( var i=0; i < data.length; i++ )
                { select.options.add(new Option(data[i].text , data[i].id)); }
        });
});//--end  change--


//--------ddl_area_id--------------
$("#ddl_services_id").change(function(event) {
        myVue.ddl_services_id = $(this).val();
        myVue.change_services();
});//--end  change--



//----------------------------------------
function show_image(input)
{ console.log('m');
       if (input.files && input.files[0])
       {
           var reader = new FileReader();

           reader.onload = function (e) {
             $(input).parents('tr').find('img').prop('src', e.target.result);
           }
           reader.readAsDataURL(input.files[0]);
       }
}
