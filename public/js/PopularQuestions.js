$('#create_form').validate();

$('#btn_create').click(function(event) {
    $('#create_model').modal('show');
});

function show_model(body,answer,status)
{
    $('#show_model').modal('show');
    $('#show_body').text(body);
    $('#show_answer').text(answer);

    if(status == '1')
      { $('#show_status').text('نعم'); }
    else {
      $('#show_status').text('لا');
    }

}

function edit_model(id,body,answer,status)
{
    $('#edit_id').val(id);
    $('#edit_model').modal('show');
    $('#edit_body').val(body);
    $('#edit_answer').val(answer);

    if(status == '1')
      { $('#edit_status').prop('checked', true); }
    else {
      $('#edit_status').prop('checked', false);
    }

}
