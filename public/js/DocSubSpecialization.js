

$(document).ready(function() {
  $('#create_form').validate();
  $('#edit_model').validate();
  $('.select2').select2({ width: '100%' });
});

$('#btn_create').click(function(event) {
  $('#create_model').modal('show');
});


function edit_model(id,text_ar,text_en,specialization_id)
{
    $('#edit_model').modal('show');
    $('#edit_id').val(id);
    $('#edit_name_ar').val(text_ar);
    $('#edit_name_en').val(text_en);
    $('#edit_specialization').val(specialization_id);
}
