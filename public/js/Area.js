var myVue = new Vue({
  'el': '#myVue',
  'data':{
      ddl_country_id: '' ,
      ddl_area_id: 0 ,
      show_content: false,
      area_list: []
  },
  mounted(){
      $('.select2').select2();
      $('#ddl_country_id').change(function(event) {
          myVue.ddl_country_id = $(this).val();
          myVue.change_country();
      });
      $('#create_form').validate();
  },//End mounted()
  methods:{
      change_country: function(){

      },
      change_governorate: function(){
          this.area_list = [];
          this.show_content = false;
          $('#table_Governorate tr').html('');
          $.get(get_area_by_id+'/'+$('#ddl_governorate_id').val(), function(responce){
              myVue.area_list = responce;
              myVue.show_content = true;
          });
      },
      add_Area_row: function(){
          myVue.area_list.push([]);
      },
      del_row: function(index){
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
            // this.governora_list.splice(index,1);
      },
      do_submit: function(event){
          $('input').removeClass('inp_error');
          if ( $('#table_Governorate tr').length == 0 )
          {
              event.preventDefault();
          }
          else
          {
              var is_valid = true;
              $('#table_Governorate input[required]').each(function(index, el) {
                    if ( $(this).val().trim() == '' )
                    {
                        is_valid = false;
                        $(this).addClass('inp_error');
                    }
              });
              if (is_valid == false)
              {
                event.preventDefault();
              }
          }

          // $("#create_form").submit(function () {
            if (is_valid)
            {
                $("#create_form").find('input[type="checkbox"]').each( function () {
                    var checkbox_this = $(this);
                    if( checkbox_this.is(":checked") == true ) {
                        checkbox_this.prop('value','on');
                    } else {
                        checkbox_this.prop('checked',true);
                        //DONT' ITS JUST CHECK THE CHECKBOX TO SUBMIT FORM DATA
                        checkbox_this.prop('value','off');
                    }
                    checkbox_this.hide();
                });
            }

              // });//End form
      }
  }//End methods
});



$("#ddl_country_id").change(function(event) {
          myVue.area_list = [];
          myVue.show_content = false;
        // $('#btn_submit').addClass('hide');
        // var is_empty = true;
        $.get(get_governora_by_country_id_pluck+'/'+$(this).val(), function(data){
            console.log(data);
            var name, select, option ;
            select = document.getElementById('ddl_governorate_id');
            // Clear the old options
            select.options.length = 0;
            // foreach(data as key =)
            select.options.add(new Option('' , '')); //choose sub category
            for( var i=0; i < data.length; i++ )
                { select.options.add(new Option(data[i].text , data[i].id)); }
        });
});//--end  change--


//--------ddl_area_id--------------
$("#ddl_governorate_id").change(function(event) {
        myVue.change_governorate();
});//--end  change--
