
var myVue = new Vue({
    el: '#myVue',
    data:{
      Care_images: [],
      services: [],

    },//End data
    mounted(){
      $('#create_form').validate();
    },//End mounted
    methods:{
        add_Care_images_row: function(){
            this.Care_images.push({});
        },
        add_Care_service_row: function(){
            this.services.push({ name_en:'', name_ar:'' });
        },
        del_Care_images: function(){
            $(document).on('click','.del',function(){
                   $(this).parents('tr').remove();
            });
        },
        do_submit: function(event){
            $(this).removeClass('inp_error');

            if ($('#create_form').valid())
            {
                if ($('table.add_services input')[0])
                {
                    event.preventDefault();
                    var is_vaild = true;
                    $('table.add_services input').each(function(index, el) {
                        if ($(this).prop('type') == 'file')
                        {
                            if ($(this)[0].files.length == 0)
                            {
                                $(this).addClass('inp_error');
                                is_vaild = false;
                            }
                        }
                        else
                        {
                            if ($(this).val().trim() == '')
                            {
                                $(this).addClass('inp_error');
                                is_vaild = false;
                            }
                        }
                    });
                    if (is_vaild == true)
                      {   $('#create_form').unbind().submit(); }
                }
            }//end if ($('#create_form').valid())
        }//End do_submit

    }//End methods
});




//----------------------------------------
function show_image(input,from)
{  console.log(from);
       if (input.files && input.files[0])
       {
           var reader = new FileReader();

           reader.onload = function (e) {
               if (from == 'profile_image')
               {
                    $('#img_profile_image').prop('src', e.target.result);
               }
               else if (from == 'Care_images')
               {
                    $(input).parents('tr').find('img').prop('src', e.target.result);
               }
           }
           reader.readAsDataURL(input.files[0]);
       }
}



//------for map---------------
function initMap()
{

      var myLatlng = new google.maps.LatLng(get_lat,get_lang);
      var mapOptions = {
       zoom: 13,
       center: myLatlng
      }

      var map = new google.maps.Map(document.getElementById("google_ptm_map"), mapOptions);

      var marker = new google.maps.Marker({
         position: myLatlng,
         map: map,
         draggable:true,
      });
      //
      google.maps.event.addListener(marker, 'dragend', function(event) {
        var myLatLng = event.latLng;
         var lat = myLatLng.lat();
         var lng = myLatLng.lng();

         document.getElementById("latitude").value = lat;

         document.getElementById("longitude").value = lng;
      });
}
