

$(document).ready(function() {
  $('#create_form').validate();
  $('#edit_form').validate();
  $('.select2').select2({ width: '100%' });
});

$('#btn_create').click(function(event) {
  $('#create_model').modal('show');
});


function edit_model(id,name_en,name_ar,details_ar,details_en)
{
    $('#edit_model').modal('show');
    $('#edit_id').val(id);
    $('#edit_name_ar').val(name_ar);
    $('#edit_name_en').val(name_en);
    $('#edit_details_en').val(details_en); console.log(details_en);
    $('#edit_details_ar').val(details_ar);
}

function show_model(name_en,name_ar,details_ar,details_en)
{
    $('#show_model').modal('show');
    $('#show_name_ar').text(name_ar);
    $('#show_name_en').text(name_en); console.log(details_en);
    $('#show_details_en').text(details_en);
    $('#show_details_ar').text(details_ar);
}
