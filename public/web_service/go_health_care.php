<?php
$json =file_get_contents('php://input');
$data  = json_decode($json,true);
include 'MUT.php';
$finalResult = '{"state":';
$state = 104;
$result;

// Codes 
// 101 >> sucess
// 102 >> empty
// 103 >> wrong mail
// 104 >> failed
// 105 >> used 

$sucess = 101;
$empty = 102;
$wrong_mail = 103;
$failed = 104;
$used_mail = 105;
$wrong_password = 106;
$rent_before = 107;
$empty_m=108;
$result_text="result";

//error_reporting(E_ERROR | E_PARSE);

 
if($data['method']=="h_care_home"){

if($data['language']=="ar"){
	$selected_cols="id,image,title_ar as name , details_ar as details,link , is_promoted ";
}else {
	$selected_cols="id,image,title_en as name , details_en as details,link , is_promoted ";
}
 
$result = select_cols("care_advertising",$selected_cols);
if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}

for($i=0; $i<count($result); $i++){
	$result[$i]['id']= intval($result[$i]['id']);

		$result[$i]['is_promoted']= intval($result[$i]['is_promoted'])==1 ? true : false;
}
$result_text="h_care_home";
}else if($data['method']=="specializations"){

if($data['language']=="ar"){
	$selected_cols="id,name_ar as name";
}else {
	$selected_cols="id,name_en as name";
}

$result = select_cols("care_specialization",$selected_cols);
if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$wrong_mail;
	}

for($i=0; $i<count($result); $i++){
	$result[$i]['id']= intval($result[$i]['id']);
}
$result_text="specializations";
}
else if($data['method']=="search"){
$where = "where care.specialization_id = ".$data['specialization_id']." and care.area_id = ".$data['area_id'];
if($data['language']=="en"){
	$sql= "select care.id as care_id , avg(coalesce(care_rating.stars,0)) as rate ,care.facebook ,care.snapchat ,care.instagram , care.twitter , care.views, care.logo as care_logo ,care.id as care_id , governorate.name_en as governorate_name , care.phone , care.mobile , care.from_time , care.to_time , care.location_comment_en as location_comment , care.about_en as about , care.name_en as care_name ,area.name_en as area_name from care left join area on care.area_id = area.id left join governorate on area.governorate_id = governorate.id left JOIN care_rating ON care.id = care_rating.care_id ".$where." group by care.id";
	 }else {
	 $sql= "select care.id as care_id ,avg(coalesce(care_rating.stars,0)) as rate ,care.facebook ,care.snapchat ,care.instagram , care.twitter , care.views , care.logo as care_logo ,care.id as care_id , governorate.name_ar as governorate_name , care.phone , care.mobile , care.from_time , care.to_time , care.location_comment_ar as location_comment , care.about_ar as about , care.name_ar as care_name ,area.name_ar as area_name from care left join area on care.area_id = area.id left join governorate on area.governorate_id = governorate.id left JOIN care_rating ON care.id = care_rating.care_id ".$where." group by care.id";
	 }
	 $result = generate_sql($sql,"select",$con);
	 
	 for($i=0; $i<count($result); $i++){
	 
	 	$data['care_id']=$result[$i]['care_id'];
	 	$fav_result = select_cols("care_favourites","id","patient_id,care_id");
	
		if(empty($fav_result)){
		$result[$i]['is_favourite'] = false;
		}else {
		$result[$i]['is_favourite'] = true;
		}
		
	 	$result[$i]['rate']=doubleval($result[$i]['rate']);
	 	$result[$i]['care_id']=intval($result[$i]['care_id']);
	 	$result[$i]['from_time'] = date('h:i A',strtotime($result[$i]['from_time']));
	 	$result[$i]['to_time'] = date('h:i A',strtotime($result[$i]['to_time']));
	 	$result[$i]['views'] = intval($result[$i]['views']);
	 }
	 
	 
	 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	
	$result_text="cares";

}
else if($data['method']=="care_offers"){
$sql= "select link,image from care_offers where care_id = ".$data['care_id'];
$result = generate_sql($sql,"select",$con);
$result_text="care_offers";

 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}

}else if($data['method']=="send_message"){

	$data['created_at']=time();
 if(insert("contact_us","method")>0){
	$state=$sucess;
	}
	else{
	$state=$failed;
	}

}
else if($data['method']=="add_favourite"){

	$data['created_at']=time();
 if(insert("care_favourites","method")>0){
	$state=$sucess;
	}
	else{
	$state=$failed;
	}

}
else if($data['method']=="delete_favourite"){
	 
 if(delete("care_favourites","patient_id,care_id")>0){
	$state=$sucess;
	}
	else{
	$state=$failed;
	}

}
else if($data['method']=="services"){
 
if($data['language']=="en"){
$sql= "select id,name_en  as name ,logo from care_services where care_id = ".$data['care_id'];
}
else {
$sql= "select id,name_ar  as name ,logo from care_services where care_id = ".$data['care_id'];
}
$result = generate_sql($sql,"select",$con);

  for($i=0; $i<count($result); $i++){
 	$result[$i]['id']= intval($result[$i]['id']);
 	 if($data['language']=="en"){
	$sql= "select id,quantity,price,name_en as name from care_sub_services where service_id = ".$result[$i]['id'];
	}
	else {
	$sql= "select id,quantity,price,name_ar as name from care_sub_services where service_id = ".$result[$i]['id'];
	}
	 
	 $result2 = generate_sql($sql,"select",$con);
	   for($ii=0; $ii<count($result2); $ii++){
	   $result2[$ii]['id']= intval($result2[$ii]['id']);
	   $result2[$ii]['quantity']= doubleval($result2[$ii]['quantity']);
	   $result2[$ii]['price']= doubleval($result2[$ii]['price']);
	   }
	 $result[$i]['sub_services']=$result2;
	 }
	 	 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	
	
	$sql = "SELECT id from care_rating where care_rating.care_id = ".$data['care_id'];

	$rating_result = generate_sql($sql,"select",$con);
	
	$result_text="services";
	$finalResult .=$state .',"'."raters".'":'.count($rating_result).',"'.$result_text.'":'.json_encode($result)."}";
	print_r($finalResult);
	die();
	
	
}else if($data['method']=="rating"){
$sql = "SELECT patients.name , care_rating.stars , care_rating.comment FROM care_rating LEFT JOIN patients ON care_rating.patient_id = patients.id where care_rating.care_id = ".$data['care_id'];

$result = generate_sql($sql,"select",$con);
	 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$wrong_mail;
	}
$result_text="rating";
$total_rating=0;
for($i=0; $i<count($result); $i++){
	$result[$i]['stars'] = doubleval($result[$i]['stars']);
	$total_rating+=$result[$i]['stars'];
}
$total_rating = $total_rating/count($result);
$finalResult .=$state .',"'."raters".'":'.count($result).',"'."total_rating".'":'.$total_rating.',"'.$result_text.'":'.json_encode($result)."}";
print_r($finalResult);
die();
}else if($data['method']=="favourites"){

if($data['language']=="en"){
	$sql= "select avg(coalesce(care_rating.stars,0)) as rate ,care.facebook ,care.instagram , care.twitter , care.views, care.logo as care_logo ,care.id as care_id , governorate.name_en as governorate_name , care.phone , care.mobile , care.from_time , care.to_time , care.location_comment_en as location_comment , care.about_en as about , care.name_en as care_name ,area.name_en as area_name from care left join area on care.area_id = area.id left join governorate on area.governorate_id = governorate.id left JOIN care_rating ON care.id = care_rating.care_id  where care.id in (select care_id from care_favourites where patient_id = ".$data['patient_id'].") group by care.id";
	 }else {
	 $sql= "select avg(coalesce(care_rating.stars,0)) as rate ,care.facebook ,care.instagram , care.twitter , care.views , care.logo as care_logo ,care.id as care_id , governorate.name_ar as governorate_name , care.phone , care.mobile , care.from_time , care.to_time , care.location_comment_ar as location_comment , care.about_ar as about , care.name_ar as care_name ,area.name_ar as area_name from care left join area on care.area_id = area.id left join governorate on area.governorate_id = governorate.id left JOIN care_rating ON care.id = care_rating.care_id where care.id in (select care_id from care_favourites patient_id = ".$data['patient_id'].") group by care.id";
	 }
	 $result = generate_sql($sql,"select",$con);
	 
	 for($i=0; $i<count($result); $i++){
	 	$result[$i]['rate']=doubleval($result[$i]['rate']);
	 	$result[$i]['care_id']=intval($result[$i]['care_id']);
	 	$result[$i]['from_time'] = date('h:i A',strtotime($result[$i]['from_time']));
	 	$result[$i]['to_time'] = date('h:i A',strtotime($result[$i]['to_time']));
	 	$result[$i]['views'] = intval($result[$i]['views']);
	 	$result[$i]['is_favourite'] = true;
	 }
	 
	 
	 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	
	$result_text="cares";

}else if($data['method']=="notifications"){
$selected_cols = "message,image,created_at";
$result = select_cols("care_notification",$selected_cols);

$ago_result="";

if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	$result_text="notifications";
	
}
else if($data['method']=="care_pictures"){
$selected_cols = "image";
$result = select_cols("care_pictures",$selected_cols,"care_id");
$ago_result="";
if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	$result_text="care_pictures";
	
}
else if($data['method']=="about_us"){

if($data['language']=="en"){
$selected_cols= "title_en  as name , details_en as details" ;
	}
else {
$selected_cols= "title_en  as name , details_en as details";
	}
	$result = select_cols("about_us",$selected_cols);
	if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	$result_text="about_us";
}else if($data['method']=="chatters"){
if($data['language']=="en"){
$sql = "select  DISTINCT chat.doctor_id , chat.massage , chat.who , doctors.fname_en as fname , doctors.lname_en as lname , doctors.profile_image from chat LEFT JOIN doctors on doctors.id = chat.doctor_id  where chat.patient_id = ".$data['patient_id']."  group by chat.doctor_id order by chat.id DESC" ;
	}
else {
$sql = "select  DISTINCT chat.doctor_id , chat.massage , chat.who , doctors.fname_ar as fname , doctors.lname_ar as lname , doctors.profile_image from chat LEFT JOIN doctors on doctors.id = chat.doctor_id  where chat.patient_id = ".$data['patient_id']."  group by chat.doctor_id order by chat.id DESC" ;
}
$result = generate_sql($sql,"select",$con);
	 for($i=0; $i<count($result); $i++){
	 	$result[$i]['doctor_id']= intval($result[$i]['doctor_id']);
	 }
	 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
$result_text="chatters";

}
else if($data['method']=="chat"){
$selected_cols= "massage , who";

	$result = select_cols("chat",$selected_cols,"doctor_id,patient_id");
	if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	$result_text="chat";

}else if($data['method']=="reservations"){

if($data['language']=="ar"){
		$sql= "SELECT care_resvation.id as reservation_id , care_sub_services.name_ar as sub_services_name ,
       care_sub_services.quantity , care_sub_services.price ,
       care_services.name_ar as care_services_name ,  
 care.location_comment_ar as location_comment , care.area_id,care.lat , care.lang ,
 area.name_ar as area_name 
 ,care.logo , care.mobile 
FROM  care_resvation LEFT JOIN care_sub_services on care_resvation.care_sub_services_id = care_sub_services.id
    LEFT JOIN care_services on care_sub_services.service_id = care_services.id
	LEFT JOIN care ON care_services.care_id = care.id 
    LEFT JOIN area ON care.area_id = area.id
WHERE care_resvation.patient_id = ".$data['patient_id'];
	 }else {
	 $sql= "SELECT care_resvation.id as reservation_id , care_sub_services.name_en as sub_services_name ,
       care_sub_services.quantity , care_sub_services.price ,
       care_services.name_en as care_services_name ,  
 care.location_comment_en as location_comment , care.area_id,care.lat , care.lang ,
 area.name_en as area_name  ,care.logo , care.mobile 
FROM  care_resvation LEFT JOIN care_sub_services on care_resvation.care_sub_services_id = care_sub_services.id
    LEFT JOIN care_services on care_sub_services.service_id = care_services.id
	LEFT JOIN care ON care_services.care_id = care.id 
    LEFT JOIN area ON care.area_id = area.id
WHERE care_resvation.patient_id = ".$data['patient_id'];
	 }
	 

	 $result = generate_sql($sql,"select",$con);
	 
	 	 for($i=0; $i<count($result); $i++){
	 	$result[$i]['area_id']= intval($result[$i]['area_id']);
	 	$result[$i]['quantity']= doubleval($result[$i]['quantity']);
	 	$result[$i]['price']= doubleval($result[$i]['price']);
	 	$result[$i]['lat']= doubleval($result[$i]['lat']);
	 	$result[$i]['lang']= doubleval($result[$i]['lang']);
	 	}
	 if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	
	$result_text="reservations";		
}else if($data['method']=="rate"){

$result2=select("care_rating","care_id,patient_id");

if(empty($result2)){

if(insert("care_rating","method")>0){
		$state=$sucess;
	}
 }else {
 	if(update("care_rating","stars,comment","care_id,patient_id")>0){
		$state=$sucess;
	}	
 }
}else if($data['method']=="add_service"){

if(insert("care_resvation","method")>0){
		$state=$sucess;
	}else {
	$state=$failed;
	}
}else if($data['method']=="delete_reservation"){

if(delete("care_resvation","id")>0){
		$state=$sucess;
	}else {
	$state=$failed;
	}
}
else if($data['method']=="notifications"){
$selected_cols = "message,image,created_at";
$result = select_cols("care_notification",$selected_cols);


if(!empty($result)){
	$state=$sucess;
	}
	else{
	$state=$empty;
	}
	$result_text="notifications";
	
}
$finalResult .=$state .',"'.$result_text.'":'.json_encode($result)."}";

print_r($finalResult);
?>
