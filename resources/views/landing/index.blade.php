
<!DOCTYPE html>
<html lang="en">
<!--designed and developed by eng : ahmed-bakry -->
<!--https://www.facebook.com/ahmed.bakry.144734-->


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="هيلث بلاس.كوم" />
    <meta name="description" content="هيلث بلاس" />
    <meta name="author" content="iqonicthemes.in" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

    <title>هيلث بلاس</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('landing/images/logo.png')}}" />

    <!-- Google Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;Raleway:300,400,500,600,700,800,900">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{asset('landing/css/bootstrap.min.css')}}">

    <!-- owl-carousel -->
    <link rel="stylesheet" type="text/css" href="{{asset('landing/css/owl-carousel/owl.carousel.css')}}" />

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{asset('landing/css/font-awesome.css')}}" />

    <!-- Magnific Popup -->
    <link rel="stylesheet" type="text/css" href="{{asset('landing/css/magnific-popup/magnific-popup.css')}}" />

    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="{{asset('landing/css/animate.css')}}" />

    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('landing/css/ionicons.min.css')}}">

    <!-- Style -->
    <link rel="stylesheet" href="{{asset('landing/css/style.css')}}">

    <!-- Responsive -->
    <link rel="stylesheet" href="{{asset('landing/css/responsive.css')}}">

    <!-- Style customizer (Remove these two lines please) -->
    <link rel="stylesheet" href="#" data-style="styles">
    <link rel="stylesheet" href="{{asset('landing/css/style-customizer.css')}}" />

    <!-- custom style -->
    <link rel="stylesheet" href="{{asset('landing/css/custom.css')}}" />
<link href="{{asset('landing/fonts/font_GESSTwoMedium-Medium.css')}}" rel="stylesheet" type="text/css">

</head>

<body>

    <!-- loading -->

    <div id="loading" class="green-bg">
        <div id="loading-center">
            <div class="boxLoading"></div>
        </div>
    </div>

    <!-- loading End -->



    <!-- Header -->

    <header id="header-wrap" data-spy="affix" data-offset-top="55">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="navbar navbar-default">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">
                                <img src="{{asset('landing/images/logo.png')}}" alt="logo">
                            </a>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right bakry "  id="top-menu">







                               <li ><a href="#home">الرئيسية</a></li>

                               <li><a href="#about-us"> عن التطبيق</a></li>
                               <li id="hidden"><a href="#features">الخصائص</a></li>

                               <li><a href="#screenshots">صور التطبيق</a></li>
                                <li><a href="#team">فريق العمل</a></li>
                             <li><a href="#contact-us"> اتصل بنا</a></li>

              </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </nav>
                </div>
            </div>
        </div>
    </header>

    <!-- Header End -->

    <!-- Banner -->

    <section id="home" class="banner-03 iq-bg iq-bg-fixed iq-box-shadow iq-over-black-80" style=" background: url( {{asset('landing/images/banner/bg.jpg')}} );">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="iq-font-white iq-tw-8 text-center">تطبيق هيلث بلاس
                    <h5 style="text-align: center ; color: #FFFFFF">
                    طبيبك الخاص اصبح داخل تطبيق
						</h5>

                    </h1>
                    <div class="iq-mobile-app text-center">
                        <div class="iq-mobile-box">
                            <img class="iq-mobile-img" src="{{asset('landing/images/banner/02.png')}}" alt="#">
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-01" data-bottom="transform:translateX(50px)" data-top="transform:translateX(-100px);">
                                <img src="{{asset('landing/images/banner/04.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-02" data-bottom="transform:translateX(100px)" data-top="transform:translateX(0px);">
                                <img src="{{asset('landing/images/banner/05.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-03" data-bottom="transform:translateX(-50px)" data-top="transform:translateX(40px);">
                                <img src="{{asset('landing/images/banner/06.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-04" data-bottom="transform:translateX(-30px)" data-top="transform:translateX(0px);">
                                <img src="{{asset('landing/images/banner/07.png')}}" alt="#">
                            </span>
                            <span data-depth="0.8" class="layer iq-mobile-icon icon-05" data-bottom="transform:translateX(0px)" data-top="transform:translateX(-50px);">
                                <img src="{{asset('landing/images/banner/08.png')}}" alt="#">
                            </span>
                        </div>
                    </div>
                    <div class="link">
                        <h5 class="iq-font-white" data-animation="animated fadeInLeft">حمل التطبيق الأن</h5>
                        <ul class="list-inline" data-animation="animated fadeInUp">
                            <li><a href="#"><i class="ion-social-apple"></i></a></li>
                            <li><a href="#"><i class="ion-social-android"></i></a></li>
                            <li><a href="#"><i class="ion-social-windows"></i></a></li>
                        </ul>

                        <p class="iq-font-white iq-mt-10">
أفضل تطبيق طبي فى الوطن العربي حيث يعتبر مجمع طبي متكامل



                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Banner End -->

<div class="main-content">
    <!-- Feature -->

    <section id="about-us" class="overview-block-ptb iq-mt-50">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">تطبيق هيلث بلاس</h2>
                        <div class="divider"></div>
                        <p id="bakry">
                        تطبيق هيلث بلاس يوفر لك و لأسرتك الأمان
                          </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-4 col-lg-4">
                    <div class="iq-fancy-box-03 text-left">
                        <i aria-hidden="true" class="ion-ios-pie-outline iq-font-blue"></i>
                        <h4 class="iq-tw-6">سهولة الإستخدام </h4>
                        <div class="info-03">
                            <p class="info-details">
التطبيق يتسم بالسهولة لتوفير العناء على المرضي و تلبية جميع إحتياجاتهم الطبية

                          </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 re7-mt-30">
                    <div class="iq-fancy-box-03 text-left">
                        <i aria-hidden="true" class="ion-ios-color-filter-outline iq-font-blue"></i>
                        <h4 class="iq-tw-6">
							روعة التصميم
						</h4>
                        <div class="info-03">
                            <p class="info-details">

								التطبيق مصمم بعناية بحيث يكون مريح للعين و لنفسية المرضي
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4 re7-mt-30">
                    <div class="iq-fancy-box-03 text-left">
                        <i aria-hidden="true" class="ion-ios-gear-outline iq-font-blue"></i>
                        <h4 class="iq-tw-6"> التحكم بالإعدادات </h4>
                        <div class="info-03">
                            <p class="info-details">

التطبيق مصمم بسهولة و يسر لكي يوفر على المرضي الجهد فى إيجاد متطلباتهم
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Feature END -->

<hr>

    <!-- About Our App -->

    <section class="iq-about-03 overview-block-ptb">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title iq-mb-20">
                        <h2 class="title iq-tw-6">  تطبيق هيلث بلاس</h2>
                        <div class="divider"></div>
                        <div class="center-block iq-pt-30">
                            <span class="lead iq-tw-6">
                            التطبيق متوفر على جميع الأجهزة
							</span>
                        </div>
                </div>
            </div>
            <div class="col-sm-12 text-center">
                <p>
				يمكنك تحميل التطبيق من جميع المتاجر و تحميل التطبيق يكون مجاني لكل المستخدمين و ذلك للتسهيل اعلى المرضي و تخفيف ألاامهم
               </p>
                <div class="btn-group iq-mt-40" role="group" aria-label="">
                  <a class="button iq-mr-15" href="# "> <i class="ion-social-apple"></i> App Store</a>
                  <a class="button iq-mr-15" href="# "> <i class="ion-social-android"></i> Play Store</a>
                </div>

            </div>
        </div>
    </div>
    </section>

    <!-- About Our App END -->


    <!-- About Our App -->

    <section id="about-us-01" class="overview-block-ptb iq-about-1 grey-bg">
        <div class="container">
            <div class="row iq-mt-20">
                <div class="col-sm-12 col-md-6 col-lg-6 iq-mt-30 popup-gallery">
                    <h2 class="heading-left iq-tw-6 iq-mt-40" style="text-align: right">  شرح الفيديو </h2>
                    <div class="lead iq-tw-7 iq-mb-20">
يمكنتك مشاهدة الفيديو التعريفي للتطبيق لمعرفة خصائص التطبيق
					</div>

                   <div class="btn-group iq-mt-40" role="group" aria-label="">
                  <a class="button popup-youtube" href="video/01.mp4" >  مشاهدة الفيديو</a>
                </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <img class="img-responsive center-block" src="{{asset('landing/images/01.png')}}" alt="#">
                </div>
            </div>
        </div>
    </section>

    <!-- About Our App END -->


    <!-- Special Features -->

    <section id="features" class="overview-block-ptb iq-amazing-tab white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6"> تطبيق هيلث بلاس</h2>
                        <div class="divider"></div>
                        <p>

							يمكنك تصفح شاشات التطبيق من خلال الضغط عليها و مشاهدة المحتوي الداخلى لها
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active wow fadeInLeft" data-wow-duration="1s">
                            <a class="round-right" href="#design" aria-controls="design" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-home"></i>
                                    <h4 class="iq-tw-6"> الصفحة الإفتتاحية </h4>
                                    <div class="fancy-content-01">
                                        <p>
                                        تتسم الصفحة الإفتتاحية بالبساطة و بها حركة خفيفة حتي يبدأ التطبيق

                                        </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInLeft" data-wow-duration="1s" >
                            <a class="round-right" href="#resolution" aria-controls="resolution" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-search"></i>
                                    <h4 class="iq-tw-6">  الصفحة الرئيسية</h4>
                                    <div class="fancy-content-01">
                                        <p>
يمكنك ايجاد المستشفيات و العروض بكل سهولة
                                   </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInLeft" data-wow-duration="1s">
                            <a class="round-right" href="#ready" aria-controls="ready" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01 text-right">
                                    <i aria-hidden="true" class="ion-ios-copy-outline"></i>
                                    <h4 class="iq-tw-6">  صفحة المساعدة   </h4>
                                    <div class="fancy-content-01">
                                        <p>
يمكنك اختيار الدخول الى المستشفيات و الأطباء أو الدخول الى النوادى الصحية
                                   </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4 text-center hidden-sm hidden-xs">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="design"><img src="{{asset('landing/images/screenshots/01.jpg')}}" class="img-responsive center-block" alt="#"></div>
                        <div role="tabpanel" class="tab-pane" id="resolution"><img src="{{asset('landing/images/screenshots/02.jpg')}}" class="img-responsive center-block" alt="#"></div>
                        <div role="tabpanel" class="tab-pane" id="ready"><img src="{{asset('landing/images/screenshots/03.jpg')}}" class="img-responsive center-block" alt="#"></div>
                        <div role="tabpanel" class="tab-pane" id="fertures"><img src="{{asset('landing/images/screenshots/04.jpg')}}" class="img-responsive center-block" alt="#"></div>
                        <div role="tabpanel" class="tab-pane" id="face"><img src="{{asset('landing/images/screenshots/05.jpg')}}" class="img-responsive center-block" alt="#"></div>
                        <div role="tabpanel" class="tab-pane" id="codes"><img src="{{asset('landing/images/screenshots/06.jpg')}}" class="img-responsive center-block" alt="#"></div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation"  class=" wow fadeInRight" data-wow-duration="1s">
                            <a href="#fertures" aria-controls="fertures" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-android-map"></i>
                                    <h4 class="iq-tw-6"> صفحة الإعدادات </h4>
                                    <div class="fancy-content-01">
                                        <p>

يمكنك التحكم فى التطبيق و اللغة و تسجيل الدخول و طرق الدفع                                         </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#face" aria-controls="face" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-android-menu"></i>
                                    <h4 class="iq-tw-6"> القائمة الرئيسية</h4>
                                    <div class="fancy-content-01">
                                        <p>
القائمة الرئيسية تسهل على المرضى التنقل بين صفحات التطبيق بكل سهولة

										</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li role="presentation" class="wow fadeInRight" data-wow-duration="1s">
                            <a href="#codes" aria-controls="codes" role="tab" data-toggle="tab">
                                <div class="iq-fancy-box-01">
                                    <i aria-hidden="true" class="ion-ios-medical"></i>
                                    <h4 class="iq-tw-6">  الأطباء</h4>
                                    <div class="fancy-content-01">
                                        <p>
يمكنك حجز موعد مع طبيبك الخاص و مراجعة تقييمات الطبيب قبل الحجز

                                   </p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Special Features END -->


    <!-- App Screenshots -->

    <section id="screenshots" class="iq-app iq-bg iq-bg-fixed iq-font-white" style="background: url({{asset('images/banner/bg.jpg')}});" >
        <div class="container-fluid">
            <div class="row row-eq-height">
                <div class="col-md-6 text-left iq-ptb-80 green-bg">
                <div class="iq-app-info" style="text-align: right">
                    <h2 class="heading-left iq-font-white white iq-tw-6 "> صور التطبيق</h2>
                    <div class="lead iq-tw-6 iq-mb-20">
					أفضل تطبيق طبي فى الوطن العربي حيث يعتبر مجمع طبي متكامل


                   </div>
                    <h4 class="iq-mt-50 iq-font-white iq-tw-6 iq-mb-15">تطبيق هيلث بلاس</h4>
                    <p class="">
                    التطبيق يتسم بالسهولة لتوفير العناء على المرضي و تلبية جميع إحتياجاتهم الطبية



                    </p>
                </div>
                </div>
                <div class="col-md-6 iq-app-screen iq-ptb-80">
                    <div class="home-screen-slide" >
                        <div class="owl-carousel popup-gallery" data-nav-dots="true" data-nav-arrow="false" data-items="3" data-sm-items="3" data-lg-items="3" data-md-items="2" data-autoplay="false">
                            <div class="item"><a href="{{asset('landing/images/screenshots/01.jpg')}}" class="popup-img"><img class="img-responsive" src="{{asset('landing/images/screenshots/01.jpg')}}" alt="#"></a></div>
                            <div class="item"><a href="{{asset('landing/images/screenshots/02.jpg')}}" class="popup-img"><img class="img-responsive" src="{{asset('landing/images/screenshots/02.jpg')}}" alt="#"></a></div>
                            <div class="item"><a href="{{asset('landing/images/screenshots/03.jpg')}}" class="popup-img"><img class="img-responsive" src="{{asset('landing/images/screenshots/03.jpg')}}" alt="#"></a></div>
                            <div class="item"><a href="{{asset('landing/images/screenshots/04.jpg')}}" class="popup-img"><img class="img-responsive" src="{{asset('landing/images/screenshots/04.jpg')}}" alt="#"></a></div>
                            <div class="item"><a href="{{asset('landing/images/screenshots/05.jpg')}}" class="popup-img"><img class="img-responsive" src="{{asset('landing/images/screenshots/05.jpg')}}" alt="#"></a></div>
                            <div class="item"><a href="{{asset('landing/images/screenshots/06.jpg')}}" class="popup-img"><img class="img-responsive" src="{{asset('landing/images/screenshots/06.jpg')}}" alt="#"></a></div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!-- App Screenshots END -->


    <!-- The Appino Great Feature -->



    <!-- The Appino Great Feature END -->


    <!-- Amazing Feature -->



    <!-- Amazing Feature END -->


    <!-- Meet the Team -->

    <section id="team" class="overview-block-ptb white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">  الأطباء</h2>
                        <div class="divider"></div>
                        <p>
التطبيق تحت إشراف نخبة من الأطباء المتخصصين و يمكنك التواصل معهم من خلال التطبيق
                      </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-3 col-md-3">
                    <div class="iq-team">
                        <img src="{{asset('landing/images/team/01.jpg')}}" class="img-responsive center-block" alt="#">
                        <div class="iq-team-info text-center">
                            <h4 class="iq-font-white iq-tw-5"> احمد بكرى</h4>
                            <span class="team-post iq-tw-5">أخصائي جراحة</span>
                            <div class="share iq-mt-15">
                                <span> <i class="fa fa-share-alt iq-mr-10" aria-hidden="true"></i> إتصل بنا</span>
                                <nav>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                    <a href="#"><i class="fa fa-github"></i></a>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3 col-md-3">
                    <div class="iq-team re7-mt-50">
                        <img src="{{asset('landing/images/team/02.jpg')}}" class="img-responsive center-block" alt="#">
                        <div class="iq-team-info text-center">
                            <h4 class="iq-font-white iq-tw-5">يسرا محسن </h4>
                            <span class="team-post iq-tw-5">أخصائية أطفال</span>
                            <div class="share iq-mt-15">
                                <span> <i class="fa fa-share-alt iq-mr-10" aria-hidden="true"></i> إتصل بنا</span>
                                <nav>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                    <a href="#"><i class="fa fa-github"></i></a>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3 col-md-3 re-mt-30">
                    <div class="iq-team">
                        <img src="{{asset('landing/images/team/03.jpg')}}" class="img-responsive center-block" alt="#">
                        <div class="iq-team-info text-center">
                            <h4 class="iq-font-white iq-tw-5"> إسلام ماجد</h4>
                            <span class="team-post iq-tw-5">أخصائي الأمراض العصبية</span>
                            <div class="share iq-mt-15">
                                <span> <i class="fa fa-share-alt iq-mr-10" aria-hidden="true"></i> إتصل بنا</span>
                                <nav>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                    <a href="#"><i class="fa fa-github"></i></a>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3 col-md-3 re-mt-30">
                    <div class="iq-team">
                        <img src="{{asset('landing/images/team/04.jpg')}}" class="img-responsive center-block" alt="#">
                        <div class="iq-team-info text-center">
                            <h4 class="iq-font-white iq-tw-5"> أسامة محسن</h4>
                            <span class="team-post iq-tw-5">أخصائي تجميل</span>
                            <div class="share iq-mt-15">
                                <span> <i class="fa fa-share-alt iq-mr-10" aria-hidden="true"></i> إتصل بنا</span>
                                <nav>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-google"></i></a>
                                    <a href="#"><i class="fa fa-github"></i></a>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Meet the Team END -->


    <!-- Counter -->

    <div class="iq-ptb-70 iq-counter-box iq-bg iq-bg-fixed iq-over-black-80" style="background: url({{asset('landing/images/about/04.jpg')}});">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-3 col-xs-6 text-center">
                    <div class="counter"> <i class="ion-ios-folder-outline iq-font-white" aria-hidden="true"></i> <span class="timer iq-tw-6 iq-font-black" data-to="1540" data-speed="10000">1540</span>
                        <label class="iq-font-white">الحجوزات</label>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-6 text-center">
                    <div class="counter"> <i class="ion-ios-paper-outline iq-font-white" aria-hidden="true"></i> <span class="timer iq-tw-6 iq-font-black" data-to="2530" data-speed="10000">2530</span>
                        <label class="iq-font-white">التحميلات</label>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-6 text-center re7-mt-50">
                    <div class="counter"> <i class="ion-ios-person-outline iq-font-white" aria-hidden="true"></i> <span class="timer iq-tw-6 iq-font-black" data-to="8120" data-speed="10000">8120</span>
                        <label class="iq-font-white">الأطباء المتخصصين</label>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-3 col-xs-6 text-center re7-mt-50">
                    <div class="counter"> <i class="ion-ios-star iq-font-white" aria-hidden="true"></i> <span class="timer iq-tw-6 iq-font-black" data-to="1620" data-speed="10000">1620</span>
                        <label class="iq-font-white">تقييمات العملاء</label>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Counter END -->


    <!-- Frequently Asked Questions -->

    <section class="overview-block-ptb white-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title">
                        <h2 class="title iq-tw-6">  الأسئلة الشائعة</h2>
                        <div class="divider"></div>
                        <p>

							جميع الأسئلة الشائعة عن التطبيق وإجاباتها بشكل مفصل
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <img class="img-responsive center-block" src="{{asset('landing/images/01.png')}}" alt="">
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="iq-accordion iq-mt-50">
                        <div class="iq-ad-block ad-active"> <a href="#" class="ad-title iq-tw-6 iq-font-grey"> <span class="ad-icon"><i class="ion-ios-infinite-outline" aria-hidden="true"></i></span> كيف يمكنك تحميل التطبيق </a>

                        <div class="ad-details">
                        <div class="row">
                          <div class="col-sm-3"><img alt="#" class="img-responsive" src="{{asset('landing/images/faq-1.jpg')}}"></div>
                          <div class="col-sm-9">

                      التطبيق متوفر على جميع المتاجر الإلكترونية
                      </div>
                       </div>
                       </div>
                        </div>

                        <div class="iq-ad-block"> <a href="#" class="ad-title iq-tw-6 iq-font-grey"> <span class="ad-icon"><i class="ion-ios-time-outline" aria-hidden="true"></i></span> كيفية معرفة مواعيد الأطباء ؟</a>
                            <div class="ad-details"  >من خلال الدخول على الصفحة الشخصية للطبيب داخل التطبيق</div>
                        </div>
                        <div class="iq-ad-block"> <a href="#" class="ad-title iq-tw-6 iq-font-grey"> <span class="ad-icon"><i class="ion-ios-compose-outline" aria-hidden="true"></i></span>  كيف يمكنك الحجز لدى طبيب معين </a>
                            <div class="ad-details">
                             <div class="row">
                               <div class="col-sm-9">

                         من خلال البحث عن طبيبك الخاص و الدخول الى صفحته الشخصية
                         </div>
                          <div class="col-sm-3"><img alt="#" class="img-responsive" src="{{asset('landing/images/faq-1.jpg')}}"></div>

                       </div>
                            </div>
                        </div>
                        <div class="iq-ad-block"> <a href="#" class="ad-title iq-tw-6 iq-font-grey"> <span class="ad-icon"><i class="ion-ios-loop" aria-hidden="true"></i></span>    كيف يمكنني تحديث التطبيق ؟</a>
                            <div class="ad-details">
						من خلال الدخول على المتجر الخاص بك
                       </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Frequently Asked Questions END -->


    <!-- Frequently Asked Questions -->

    <section class="overview-block-ptb iq-bg iq-bg-fixed iq-over-black-80" style="background: url(images/about/04.jpg);" >
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-title iq-mb-40">
                        <h2 class="title iq-tw-6 iq-font-white"> تحميل التطبيق</h2>
                        <div class="divider white"></div>
                        <p class="iq-font-white">
							يمكنك تحميل التطبيق من جميع المتاجر و تحميل التطبيق يكون مجاني لكل المستخدمين و ذلك للتسهيل اعلى المرضي و تخفيف ألاامهم
                   </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <a class="button button-icon iq-mr-15" href="# "> <i class="ion-social-apple"></i> App Store</a>
                    <a class="button button-icon iq-mr-15 re4-mt-20" href="# "> <i class="ion-social-android"></i> Google Play</a>
                    <a class="button button-icon re7-mt-30 re4-mt-20" href="# "> <i class="ion-social-windows"></i> Windows Store</a>
                </div>
        </div>
        </div>
    </section>

    <!-- Frequently Asked Questions END -->


    <!-- Affordable Price -->



    <!-- Affordable Price END -->

    <!-- Latest Blog Post -->




    <!-- Latest Blog Post END -->


    <!-- Clients -->



    <!-- Clients END -->

</div>

<div class="footer" style="padding-top: 5px ; text-align: right">

    <!-- Subscribe Our Newsletter -->



    <!-- Subscribe Our Newsletter END -->

    <!-- full-contact -->

    <section id="contact-us" class="iq-full-contact white-bg">
        <div class="container-fluid">
            <div class="row no-gutter">
                <div class="col-sm-6">
                    <div class="iq-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.840108181602!2d144.95373631539215!3d-37.8172139797516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1497005461921" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-sm-6 iq-mt-20 iq-pall-40">
                    <h4 class="heading-left iq-tw-6 iq-pb-20">  تواصل معنا</h4>
                    <div class="row">
                        <div id="formmessage">Success/Error Message Goes Here</div>
                        <form class="form-horizontal" id="contactform" method="post" action="http://iqonicthemes.com/themes/appino/php/contact-form.php">
                            <div class="contact-form">
                                <div class="col-sm-6">
                                    <div class="section-field">
                                        <input id="name" type="text" placeholder=" الاسم" name="name">
                                    </div>
                                    <div class="section-field">
                                        <input type="email" placeholder=" البريد الإلكتروني" name="email">
                                    </div>
                                    <div class="section-field">
                                        <input type="text" placeholder=" الهاتف" name="phone">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="section-field textarea">
                                        <textarea class="input-message" placeholder=" الرسالة" rows="7" name="message"></textarea>
                                    </div>
                                    <input type="hidden" name="action" value="sendEmail" />
                                    <button id="submit" name="submit" type="submit" value="Send" class="button pull-right iq-mt-40"> إرسال</button>
                                </div>
                            </div>
                        </form>
                        <div id="ajaxloader" style="display:none"><img class="center-block mt-30 mb-30" src="{{asset('landing/images/ajax-loader.gif')}}" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row iq-ptb-80">







                <div class="col-sm-6 col-md-4 col-lg-4 media" >



                      <div class="iq-fancy-box-04">
                        <div class="iq-icon green-bg">
                            <i aria-hidden="true" class="ion-ios-email-outline"></i>
                        </div>
                        <div class="fancy-content">
                            <h5 class="iq-tw-6">البريد الإلكتروني</h5>
                            <span class="lead iq-tw-6">support@appino.com</span>
                            <p class="iq-mb-0">24 X 7 online support</p>
                        </div>
                    </div>





                </div>

                  <div class="col-sm-6 col-md-4 col-lg-4 media" >
                    <div class="iq-fancy-box-04">
                        <div class="iq-icon green-bg">
                            <i aria-hidden="true" class="ion-ios-telephone-outline"></i>
                        </div>
                        <div class="fancy-content">
                            <h5 class="iq-tw-6">الهاتف</h5>
                            <span class="lead iq-tw-6">+0123 456 789</span>
                            <p class="iq-mb-0">Mon-Fri 8:00am - 8:00pm</p>
                        </div>
                    </div>
                </div>
                 <div class="col-sm-6 col-md-4 col-lg-4 media"  >
                    <div class="iq-fancy-box-04">
                        <div class="iq-icon green-bg">
                            <i aria-hidden="true" class="ion-ios-location-outline"></i>
                        </div>
                        <div class="fancy-content">
                            <h5 class="iq-tw-6">العنوان</h5>
                            <span class="lead iq-tw-6">1234 North Luke Lane, South Bend,IN 360001</span>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- full-contact END -->

    <!-- Footer -->

    <footer class="iq-footer-03 white-bg iq-ptb-20">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="footer-copyright iq-pt-10">جميع الحقوق محفوظة@ <span id="copyright"> <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script></span> طبيق هيلث بلاس  </div>
					<a href="http://2grand.net/" style="padding-right: 13%;" class="logo"><img src="{{asset('landing/images/android.png')}}" /></a>
                </div>
                <div class="col-sm-5">
                    <ul class="info-share">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-google"></i></a></li>
                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                    </ul>
                </div>
            </div>

        </div>
    </footer>

    <!--  END -->

</div>

    <!-- back-to-top -->

    <div id="back-to-top">
        <a class="top" id="top" href="#top"> <i class="ion-ios-upload-outline"></i> </a>
    </div>

    <!-- back-to-top End -->


    <!-- style-customizer -->



    <!-- style-customizer END -->


    <!-- jQuery -->
    <script type="text/javascript" src="{{asset('landing/js/jquery.min.js')}}"></script>

    <!-- owl-carousel -->
    <script type="text/javascript" src="{{asset('landing/js/owl-carousel/owl.carousel.min.js')}}"></script>

    <!-- Counter -->
    <script type="text/javascript" src="{{asset('landing/js/counter/jquery.countTo.js')}}"></script>

    <!-- Jquery Appear -->
    <script type="text/javascript" src="{{asset('landing/js/jquery.appear.js')}}"></script>

    <!-- Magnific Popup -->
    <script type="text/javascript" src="{{asset('landing/js/magnific-popup/jquery.magnific-popup.min.js')}}"></script>

    <!-- Retina -->
    <script type="text/javascript" src="{{asset('landing/js/retina.min.js')}}"></script>

    <!-- wow -->
    <script type="text/javascript" src="{{asset('landing/js/wow.min.js')}}"></script>

    <!-- Skrollr -->
    <script type="text/javascript" src="{{asset('landing/js/skrollr.min.js')}}"></script>

    <!-- Countdown -->
    <script type="text/javascript" src="{{asset('landing/js/jquery.countdown.min.js')}}"></script>

    <!-- bootstrap -->
    <script type="text/javascript" src="{{asset('landing/js/bootstrap.min.js')}}"></script>

    <!-- Style Customizer -->
    <script type="text/javascript" src="{{asset('landing/js/style-customizer.js')}}"></script>

    <!-- Custom -->
    <script type="text/javascript" src="{{asset('landing/js/custom.js')}}"></script>

</body>



</html>
