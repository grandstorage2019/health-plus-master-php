@extends('atlant.blank')

@section('content')
      @php($active='Doctor')
      @permission('Doctor_create')
      <style media="screen">
        .right_border
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
        }
        .botton_border
        {
          border-bottom: 1px solid #ccc;
        }
        .days td
        {
          text-align: center;
        }
        #center
        {
           display: table;
           margin: 0 auto;
        }
        #Clicnic_images
        {
            width: 20%;
        }
        /* =================================== */



      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12"  id="myVue">

          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
              <li class="active"> @lang('page.show') </li>
              <li><a href="{{url('Doctor')}}"> @lang('page.Doctor') </a></li>
          </ul>
          <!-- END BREADCRUMB -->





      {!! Form::model($doctor= new \App\Doctor,['url'=>'Doctor','novalidate','id'=>'create_form','files'=>true]) !!}

      <div class="row">
          <div class="col-md-7  ">
      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.info')
          @endslot
          @slot('body')
            <br>

            <div class="row">
                <div class="col-md-6">
                  <br><br><br>
                  {!! Form::file('profile_image',['class'=>'form-control','onchange'=>'show_image(this,"profile_image")']) !!}
                </div>
                <div class="col-md-6">
                    <img src="{{asset('images/doctor_profile/doctor_image.jpg')}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px" id="img_profile_image" >
                </div>
            </div><!--End row-->

            <table class="table mydir">
                <tr>
                  <th> @lang('page.fname_en') </th>
                  <td> {!! Form::text('fname_en',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.fname_ar') </th>
                  <td> {!! Form::text('fname_ar',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.lname_en') </th>
                  <td> {!! Form::text('lname_en',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.lname_ar') </th>
                  <td> {!! Form::text('lname_ar',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.accapted by admin') </th>
                  <td> {!! Form::select('is_accapted_by_admin',['0'=>__('page.not accapted'),'1'=>__('page.accapted')],null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.price') </th>
                  <td> {!! Form::number('price',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.username') </th>
                  <td> {!! Form::text('username',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.email') </th>
                  <td> {!! Form::email('email',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.secretary email') </th>
                  <td> {!! Form::email('secretary_email',null,['class'=>'form-control']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.password') </th>
                  <td> {!! Form::password('password',['class'=>'form-control','id'=>'password','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.confirm password') </th>
                  <td> {!! Form::password('password_again',['class'=>'form-control','id'=>'password_again','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.phone') </th>
                  <td> {!! Form::text('phone',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.birth_day') </th>
                  <td> {!! Form::date('birth_day',null,['class'=>'form-control datepicker','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.gender') </th>
                  <td> {!! Form::select('gender',['male'=>__('page.male'),'female'=>__('page.female')],null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.level_ar') </th>
                  <td> {!! Form::text('level_ar',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.level_en') </th>
                  <td> {!! Form::text('level_en',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.title Arabic') </th>
                  <td> {!! Form::text('title_ar',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.title English') </th>
                  <td> {!! Form::text('title_en',null,['class'=>'form-control','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.about Arabic') </th>
                  <td> {!! Form::textarea('about_ar',null,['class'=>'form-control','rows'=>'4','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.about English') </th>
                  <td> {!! Form::textarea('about_en',null,['class'=>'form-control','rows'=>'4','required']) !!} </td>
                </tr>
                <tr>
                  <th> @lang('page.plan') </th>
                  <td> {!! Form::select('plan_id',$plans,null,['class'=>'form-control','required']) !!} </td>
                </tr>
            </table>

            <br>
          @endslot
         @endcomponent
            <!-------------------------------------------->
              {{-- <div class="botton_border"> </div> --}}
            <!-------------------------------------------->
        @component('componets.panel_default')
            @slot('panel_title')
                  @lang('page.Times')
            @endslot
            @slot('body')

            <br>
            <h2>@lang('page.available times')</h2>
            <select class="form-control" v-model="ddl_available_date" v-on:change="change_available_date()" required  name="available_date_type"   >
                <option value=""> @lang('page.choose plan') </option>
                <option value="is_life_time" selected  > @lang('page.life time') </option>
                <option value="specific"> @lang('page.specific date') </option>
            </select>

            <div class="row" v-show="specific_date">
                <div class="col-md-6">
                    {!! Form::label('from_date',__('page.form date')) !!}
                    {!! Form::date('from_date',null,['class'=>'form-control datepicker']) !!}
                </div>
                <div class="col-md-6">
                    {!! Form::label('to_date',__('page.to date')) !!}
                    {!! Form::date('to_date',null,['class'=>'form-control datepicker']) !!}
                </div>
            </div><!--End row-->
            <br>
            <hr>
            <h3>@lang('page.days')</h3>
            <table class="table mydirection">
              <thead>
                  <th> @lang('page.day') </th>
                  <th> @lang('page.from') </th>
                  <th> @lang('page.to') </th>
                  <th> @lang('page.type') </th>
                  <th> @lang('page.wating time') </th>
                  <th> @lang('page.num resrvation') </th>
              </thead>
              <tbody>
                  <tr>
                      <td> @lang('page.Sat') </td>
                      <td width="5%"> {!! Form::time('Sat_from_time',null,['class'=>'form-control']) !!} </td>
                      <td width="5%"> {!! Form::time('Sat_to_time',null,['class'=>'form-control']) !!} </td>
                      <td width="15%">
                          {!! Form::select('Sat_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],null,
                                             ['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td width="5%"> {!! Form::select('Sat_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!} </td>

                      <td width="5%"> {!! Form::text('Sat_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
                  <tr>
                      <td> @lang('page.Sun') </td>
                      <td> {!! Form::time('Sun_from_time',null,['class'=>'form-control']) !!} </td>
                      <td> {!! Form::time('Sun_to_time',null,['class'=>'form-control']) !!} </td>
                      <td>
                          {!! Form::select('Sun_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],
                                          null,['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td>{!! Form::select('Sun_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!}</td>
                      <td> {!! Form::text('Sun_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
                  <tr>
                      <td> @lang('page.Mon') </td>
                      <td> {!! Form::time('Mon_from_time',null,['class'=>'form-control']) !!} </td>
                      <td> {!! Form::time('Mon_to_time',null,['class'=>'form-control']) !!} </td>
                      <td>
                          {!! Form::select('Mon_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],null,
                                             ['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td>{!! Form::select('Mon_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!}</td>
                      <td> {!! Form::text('Mon_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
                  <tr>
                      <td> @lang('page.Tue') </td>
                      <td> {!! Form::time('Tue_from_time',null,['class'=>'form-control']) !!} </td>
                      <td> {!! Form::time('Tue_to_time',null,['class'=>'form-control']) !!} </td>
                      <td>
                          {!! Form::select('Tue_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],null,
                                             ['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td>{!! Form::select('Tue_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!}</td>
                      <td> {!! Form::text('Tue_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
                  <tr>
                      <td> @lang('page.Thu') </td>
                      <td> {!! Form::time('Thu_from_time',null,['class'=>'form-control']) !!} </td>
                      <td> {!! Form::time('Thu_to_time',null,['class'=>'form-control']) !!} </td>
                      <td>
                          {!! Form::select('Thu_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],null,
                                             ['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td>{!! Form::select('Thu_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!}</td>
                      <td> {!! Form::text('Thu_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
                  <tr>
                      <td> @lang('page.Wed') </td>
                      <td> {!! Form::time('Wed_from_time',null,['class'=>'form-control']) !!} </td>
                      <td> {!! Form::time('Wed_to_time',null,['class'=>'form-control']) !!} </td>
                      <td>
                          {!! Form::select('Wed_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],null,
                                             ['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td>{!! Form::select('Wed_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!}</td>
                      <td> {!! Form::text('Wed_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
                  <tr>
                      <td> @lang('page.Fri') </td>
                      <td> {!! Form::time('Fri_from_time',null,['class'=>'form-control']) !!} </td>
                      <td> {!! Form::time('Fri_to_time',null,['class'=>'form-control']) !!} </td>
                      <td>
                          {!! Form::select('Fri_type',['specific time'=>__('page.specific time'),'first reservation'=>__('page.first reservation')],null,
                                             ['class'=>'form-control ddl_time_type']) !!}
                      </td>
                      <td>{!! Form::select('Fri_wating_time',[''=>'','00:15:00'=>'15 minutes','00:20:00'=>'20 minutes','00:30:00'=>'30 minutes','00:40:00'=>'40 minutes','00:50:00'=>'50 minutes','01:00:00'=>'1 hour'],
                                                      null,['class'=>'form-control inp_wating_time' ]) !!}</td>
                      <td> {!! Form::text('Fri_num_resrvation',null,['class'=>'form-control inp_num_resrvation','disabled']) !!} </td>
                  </tr>
              </tbody>
            </table>

          @endslot
         @endcomponent
            <!-------------------------------------------->

          </div><!--End col-md-6-->

  {{-- ===================================================right side ==================================================== --}}
          <div class="col-md-5">
      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.more info')
          @endslot
          @slot('body')

            <br><br>
            <h2> @lang('page.specialization') </h2>
            {!! Form::select('specialization_id',$specializations,null,['class'=>'form-control','required','v-model'=>'specialization','v-on:change'=>'change_specialization()']) !!}

            <h2> @lang('page.doctorservices') </h2>

            <select class="form-control" name="services[]" multiple >
                @foreach($allServices as $key => $value)
                    <option value="{{$key}}">{{$value}}</option>
                @endforeach
            </select>
            <br>
            <!-------------------------------------------->
              <div class="botton_border"> </div>
            <!-------------------------------------------->
            <h2> @lang('page.license') </h2>

            <div id=" ">

                  <h2 class="mydirection div_add_license">
                          <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_licencse_images_row()" >
                              <i class="fa fa-plus"></i>
                          </button>
                          <span> @lang('page.Clicnic image') </span>
                  </h2>
                  <table class="table">
                      <tr v-for="(image,index) in licencses" v-if="index<5" >
                          <td> {!! Form::file('license[]',['class'=>'form-control' ,'onchange'=>'show_image(this,"license_images")']) !!} </td>
                          <td> <img src="" width="150px" >  </td>
                          <td>
                                <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_licencse_images(index)">
                                    <i class="fa fa-trash-o"></i>
                                </button>
                           </td>
                      </tr>
                  </table>

                {{-- {!! Form::file('license',['class'=>'form-control','onchange'=>'show_image(this,"licencse")']) !!}
                <img src="{{asset('images/asset/Frame.png')}}" width="400px" style="max-height:400px;" id="licencse"> --}}
            </div>
            <br>
            <!-------------------------------------------->
              <div class="botton_border"> </div>
            <!-------------------------------------------->
            <h2>@lang('page.Insurance companies (multi select)')</h2>
            {!! Form::select('InsuranceCompanies[]',$InsuranceCompany,null,['class'=>'form-control ','multiple']) !!}

            <br>
            <!-------------------------------------------->
              <div class="botton_border"> </div>
            <!-------------------------------------------->

              <h1> @lang('page.Clicnic') </h1>

              <table class="table mydirection">
                  <tr>
                    <th> @lang('page.name English')  </th>
                    <td> {!! Form::text('clinic_name_en',null,['class'=>'form-control' ]) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.name Arabic')  </th>
                    <td> {!! Form::text('clinic_name_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.area')  </th>
                    <td> {!! Form::select('Clicnic_area_id',$areas,null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.block English')  </th>
                    <td> {!! Form::text('block_en',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.block Arabic')  </th>
                    <td> {!! Form::text('block_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.street name English')  </th>
                    <td> {!! Form::text('Clicnic_streetName_en',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.street name Arabic')  </th>
                    <td> {!! Form::text('Clicnic_streetName_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.number')  </th>
                    <td> {!! Form::text('Clicnic_number',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.landmark English')  </th>
                    <td> {!! Form::text('Clicnic_landmark_en',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.landmark Arabic')  </th>
                    <td> {!! Form::text('Clicnic_landmark_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>

                  <tr>
                    <th> @lang('page.apartment English')  </th>
                    <td> {!! Form::text('Clicnic_apartment_en',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.apartment Arabic')  </th>
                    <td> {!! Form::text('Clicnic_apartment_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.building number English')  </th>
                    <td> {!! Form::text('Clicnic_building_number_en',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.building number Arabic')  </th>
                    <td> {!! Form::text('Clicnic_building_number_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.floor English')  </th>
                    <td> {!! Form::text('Clicnic_floor_en',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.floor Arabic')  </th>
                    <td> {!! Form::text('Clicnic_floor_ar',null,['class'=>'form-control']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.latitude')  </th>
                    <td> {!! Form::number('Clicnic_lat',null,['class'=>'form-control','id'=>'latitude'  ]) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.longitude')  </th>
                    <td> {!! Form::number('Clicnic_lang',null,['class'=>'form-control','id'=>'longitude'   ]) !!} </td>
                  </tr>
              </table>

              <div class="panel-body panel-body-map">
                  <div id="google_ptm_map" style="width: 100%; height: 300px;"></div>
              </div>

            @endslot
            @endcomponent
          </div><!--End col-md-6-->

          {{-- <div class="col-md-5">
            @component('componets.panel_default')
                @slot('panel_title')
                      @lang('page.Map')
                @endslot
                @slot('body')
                  <div class="panel-body panel-body-map">
                      <div id="google_ptm_map" style="width: 100%; height: 300px;"></div>
                  </div>
            @endslot
            @endcomponent
          </div><!--End col-md-6--> --}}
      </div><!--End row-->
      {{-- <hr> --}}

    @component('componets.panel_default')
        @slot('panel_title')
              @lang('page.images')
        @endslot
        @slot('body')

      <!-- ----------------------------------- Clicnic_images -------------------------------->
          <div id="Clicnic_images"  style="width:50%;display:table;margin:0 auto" >

              <table class="table mydirection">
                  <tr v-for="(image,index) in Clicnic_images">
                      <td> {!! Form::file('Clicnic_images[]',['class'=>'form-control','onchange'=>'show_image(this,"Clicnic_images")']) !!} </td>
                      <td> <img src="" width="150px" >  </td>
                      <td>
                          <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_Clicnic_images()">
                              <i class="fa fa-trash-o"></i>
                          </button>
                      </td>
                  </tr>
              </table>

              <h2 class="mydirection div_add_brand">
                      <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_Clicnic_images_row()" >
                          <i class="fa fa-plus"></i>
                      </button>
                      <span> @lang('page.Clicnic image') </span>
              </h2>

          </div><!--End Clicnic_images-->

      @endslot
      @endcomponent


      <!-------------------------------------------->
        {{-- <div class="botton_border"> </div> --}}
      <!-------------------------------------------->
      <br><br>
      {!! Form::submit(__('page.add'),['class'=>'btn btn-success ','style'=>'width:100%']) !!}

      <br><br>
    {!! Form::close() !!}
    </div><!--End panel-body-->
   </div>


 @else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission

@endsection


@section('script')
  <!-- START THIS PAGE PLUGINS-->


  <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>
      {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-timepicker.min.js')}}"></script> --}}
  <script type="text/javascript" src="{{asset('js/Vue.js')}}"></script>


  <script>
     var get_SubSpecialization_api = '{{url('Doctor/get_SubSpecialization_api')}}';

     var get_lat = '{{$doctor->get_ClicnicAddress->lat??0}}';
     var get_lang = '{{$doctor->get_ClicnicAddress->lang??0}}';



  </script>

  <script src="{{asset('js/doctor_create.js')}}"> </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGmN1GmN4mJz32R_E0oX9qGUc8hlEGT8o&callback=initMap" async defer></script>

@endsection
