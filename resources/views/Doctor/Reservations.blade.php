@extends('atlant.blank')

@section('content')
      @php($active='Doctor')
      @permission('Doctor_edit')

      <style media="screen">
        .right_border
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
        }
        .botton_border
        {
          border-bottom: 1px solid #ccc;
        }
        .days td
        {
          text-align: center;
        }
        #center
        {
           display: table;
           margin: 0 auto;
        }
        #Clicnic_images
        {
            width: 20%;
        }
        /* =================================== */



      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12" id="myVue">

          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
              <li class="active"> @lang('page.reservations') </li>
              <li><a href="{{url('Doctor')}}"> @lang('page.Doctor') </a></li>
          </ul>
          <!-- END BREADCRUMB -->




      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.upcoming reservations for doctor')  <b> {{$doctor->get_doc_name()}} </b>

          @endslot
          @slot('body')

            <table class="table mydir">
                <thead>
                    <th> @lang('page.Patient id') </th>
                    <th> @lang('page.Patient name') </th>
                    <th> @lang('page.Patient email') </th>
                    <th> @lang('page.Patient phone') </th>
                    <th> @lang('page.Time') </th>
                    <th> @lang('page.more') </th>
                </thead>
                <tbody>
                  @foreach ($Reservations as $Reservation)
                      <tr>
                        <td> {{$Reservation->get_Patient->id??''}} </td>
                        <td> {{$Reservation->get_Patient->name??''}} </td>
                        <td> {{$Reservation->get_Patient->email??''}} </td>
                        <td> {{$Reservation->get_Patient->phone??''}} </td>
                        <td> {{$Reservation->date}} {{$Reservation->time}} </td>
                        <td>
                            <a href="{{url('Doctor/reservations/cancle/'.$Reservation->id)}}" class="btn btn-danger btn-rounded" data-toggle="tooltip" title="@lang('page.cancle the reservation')" >
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                      </tr>
                  @endforeach
                </tbody>
            </table>

          @endslot
      @endcomponent





 @else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission


@endsection


@section('script')

   <script>

   </script>
  <script src="{!!asset('js/doctor_edit.js')!!}"> </script>


@endsection
