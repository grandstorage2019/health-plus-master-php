

@extends('atlant.blank')

@section('content')
      @php($active='Doctor')
      @permission('Doctor_index')

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Doctor') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Doctor') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
      @permission('Doctor_create')
      <a class="btn btn-primary btn-rounded" href="{{url('Doctor/create')}}"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </a>
      @endpermission
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th> @lang('page.id') </th>
            <th> @lang('page.name') </th>
            <th> @lang('page.accapted by admin') </th>
            <th> @lang('page.email') </th>
            <th> @lang('page.gender') </th>
            <th> @lang('page.specialization') </th>
            <th> @lang('page.area') </th>
            <th> @lang('page.phone') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($doctors as $doctor)
                  <tr>
                        <td width="3%"> {{$doctor->id}} </td>
                        <td>
                          @if ($doctor->profile_image)
                              <img src="{{asset('images/doctor_profile/'.$doctor->profile_image)}}" class="img-thumbnail mydir" width="60px" style="max-height:70px;margin:1px 15px">
                          @else
                              <img src="{{asset('images/doctor_profile/doctor_image.jpg')}}" class="img-thumbnail mydir" width="60px" style="max-height:70px;margin:1px 15px">
                          @endif
                          {{$doctor->get_doc_name()}}
                        </td>
                        <td>
                            @if ($doctor->is_accapted_by_admin)
                                <span class="badge badge-success">@lang('page.yes')</span>
                            @else
                                <span class="badge badge-danger">@lang('page.no')</span>
                            @endif
                        </td>
                        <td> {{$doctor->email}} </td>
                        <td> {{$doctor->gender}} </td>
                        <td>
                            @if ($doctor->get_specialization_mini)
                                {{( \Session::get('lang') == 'ar' )? $doctor->get_specialization_mini->name_ar : $doctor->get_specialization_mini->name_en}}
                            @endif
                        </td>
                        <td>
                          @isset ($doctor->get_ClicnicAddress->get_area->name_en)
                              {{$doctor->get_ClicnicAddress->get_area->name_en}}
                          @endisset
                        </td>
                        <td> {{$doctor->phone}} </td>
                        <td>
                              @permission('Doctor_show')
                                <a href="{{url('Doctor/'.$doctor->id)}}" class="btn btn-primary btn-rounded"> <i class="fa fa-search"></i> </a>
                              @endpermission

                              @permission('Doctor_edit')
                                <a href="{{url('Doctor/edit/'.$doctor->id)}}" class="btn btn-warning btn-rounded" >
                                    <i class="fa fa-pencil"></i>
                                </a>
                              @endpermission

                              @permission('Doctor_delete')
                              <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('Doctor/delete/'.$doctor->id)}}','{{$doctor->fname_en}}')" >
                                  <i class="glyphicon glyphicon-trash"></i>
                              </button>
                              @endpermission
                              {{-- @permission('Doctor_delete') --}}
                              <a href="{{url('Doctor/reservations/'.$doctor->id)}}" class="btn btn-info btn-rounded" >
                                  <i class="fa fa-calendar"></i>
                              </a>
                              {{-- @endpermission --}}
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$doctors->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('Doctor')}}';
    </script>
    {{-- <script src="{{asset('js/Country.js')}}"> </script> --}}


@endsection
