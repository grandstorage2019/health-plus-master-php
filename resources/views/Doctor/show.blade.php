@extends('atlant.blank')

@section('content')
      @php($active='Doctor')
      @permission('Doctor_show')

      <style media="screen">
        .right_border
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
        }
        .botton_border
        {
          border-bottom: 1px solid #ccc;
        }
        .days td
        {
          text-align: center;
        }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
              <li class="active"> @lang('page.show') </li>
              <li><a href="{{url('Doctor')}}"> @lang('page.Doctor') </a></li>
          </ul>
          <!-- END BREADCRUMB -->


      <div class="row">
          <div class="col-md-6  ">
      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.Info')
          @endslot
          @slot('body')

            @if ($doctor->profile_image)
                <img src="{{asset('images/doctor_profile/'.$doctor->profile_image)}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px">
            @else
                <img src="{{asset('images/doctor_profile/doctor_image.jpg')}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px">
            @endif
            <br>
            <span>@lang('page.price'): {{$doctor->price}}</span>
            <br><br>
            <span>
                <span class="mydir" style="margin-right:15px"> @lang('page.starts'):   </span>
                 @for ($i = 0; $i < (int)$doctor->get_starts(); $i++)
                   <i class="fa fa-star"></i>
                 @endfor
             </span>
             <br><br>
             <span> @lang('page.views'): {{$doctor->views}} </span>

            <table class="table mydirection">

                <tr>
                  <td> @lang('page.name Arabic') </td>
                  <td> {{$doctor->fname_ar.' '.$doctor->lname_ar}} </td>
                </tr>
                <tr>
                  <td> @lang('page.name English') </td>
                  <td> {{$doctor->fname_en.' '.$doctor->lname_en}} </td>
                </tr>
                <tr>
                  <td> @lang('page.accapted by admin') </td>
                  <td>
                        @if ($doctor->is_accapted_by_admin)
                            <span class="badge badge-success">@lang('page.yes')</span>
                        @else
                            <span class="badge badge-danger">@lang('page.no')</span>
                        @endif
                   </td>
                </tr>
                <tr>
                  <td> @lang('page.email') </td>
                  <td> {{$doctor->email}} </td>
                </tr>
                <tr>
                  <td> @lang('page.secretary email') </td>
                  <td> {{$doctor->secretary_email}} </td>
                </tr>
                <tr>
                  <td> @lang('page.username') </td>
                  <td> {{$doctor->username}} </td>
                </tr>
                <tr>
                  <td> @lang('page.gender') </td>
                  <td> {{$doctor->gender}} </td>
                </tr>
                <tr>
                  <td> @lang('page.phone') </td>
                  <td> {{$doctor->phone}} </td>
                </tr>
                <tr>
                  <td> @lang('page.birth day') </td>
                  <td>
                    @if ($doctor->birth_day)
                       {{$doctor->birth_day->toDateString()}}
                    @endif
                  </td>
                </tr>

                <tr>
                  {{-- <td> @lang('page.country') </td>
                  <td>
                      @isset($doctor->get_country)
                          @if (\Session::get('lang') == 'ar')
                              {{$doctor->get_country->name_ar}}
                          @else
                              {{$doctor->get_country->name_en}}
                          @endif
                      @endisset
                  </td> --}}
                  <tr>
                    <td> @lang('page.level_en') </td>
                    <td> {{$doctor->level_en}} </td>
                  </tr>
                  <tr>
                    <td> @lang('page.level_ar') </td>
                    <td> {{$doctor->level_ar}} </td>
                  </tr>
                  <tr>
                    <td> @lang('page.title Arabic') </td>
                    <td> {{$doctor->title_ar}} </td>
                  </tr>
                  <tr>
                    <td> @lang('page.title English') </td>
                    <td> {{$doctor->title_en}} </td>
                  </tr>
                  <tr>
                    <td> @lang('page.about Arabic') </td>
                    <td> {{$doctor->about_ar}} </td>
                  </tr>
                  <tr>
                    <td> @lang('page.about English') </td>
                    <td> {{$doctor->about_en}} </td>
                  </tr>
                </tr>
                <tr>
                  <td> @lang('page.plan') </td>
                  <td>
                      @isset($doctor->get_plan->name_en)
                          {{(\Session::get('lang') == 'ar')?$doctor->get_plan->name_ar:$doctor->get_plan->name_en}}
                      @else
                         @lang('page.No plan selected')
                      @endisset
                  </td>
                </tr>
            </table>

          @endslot
         @endcomponent


            <!-------------------------------------------->
              {{-- <div class="botton_border"> </div> --}}
            <!-------------------------------------------->
      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.Time')
          @endslot
          @slot('body')

            <h2> @lang('page.available times') </h2>
            @if ($doctor->get_available_date)
                @if ($doctor->get_available_date->is_life_time)
                    <p> @lang('page.for a life time') </p>
                @else
                  <p>
                      <span>
                          @lang('page.from date') :
                          {{isset($doctor->get_available_date->from_date)?$doctor->get_available_date->from_date->toDateString():''}}
                      </span>
                      <span>
                          @lang('page.to date') :
                          {{isset($doctor->get_available_date->to_date)?$doctor->get_available_date->to_date->toDateString():''}}
                      </span>
                  </p>
                @endif
                  <h3>@lang('page.days')</h3>
                  <table class="table mydir days">
                    <thead>
                        <th> @lang('page.day') </th>
                        <th> @lang('page.from') </th>
                        <th> @lang('page.to') </th>
                        <th> @lang('page.type') </th>
                        <th> @lang('page.wating time') </th>
                        <th> @lang('page.num resrvation') </th>
                    </thead>
                    <tbody>
                      @if ($doctor->get_available_date->Sat_type)
                        <tr> <!---Sat Day --->
                            <td>  @lang('page.Sat') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Sat_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Sat_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Sat_type)}} </td>
                            @if ($doctor->get_available_date->Sat_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Sat_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Sat_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif

                      @if ($doctor->get_available_date->Sun_type)
                        <tr> <!---Sun Day --->
                            <td>  @lang('page.Sun') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Sun_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Sun_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Sun_type)}} </td>
                            @if ($doctor->get_available_date->Sun_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Sun_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Sun_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif

                      @if ($doctor->get_available_date->Mon_type)
                        <tr> <!---Mon Day --->
                            <td>  @lang('page.Mon') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Mon_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Mon_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Mon_type)}} </td>
                            @if ($doctor->get_available_date->Mon_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Mon_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Mon_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif

                      @if ($doctor->get_available_date->Tue_type)
                        <tr> <!---Tue Day --->
                            <td>  @lang('page.Tue') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Tue_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Tue_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Tue_type)}} </td>
                            @if ($doctor->get_available_date->Tue_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Tue_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Tue_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif

                      @if ($doctor->get_available_date->Thu_type)
                        <tr> <!---Thu Day --->
                            <td>  @lang('page.Thu') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Thu_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Thu_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Thu_type)}} </td>
                            @if ($doctor->get_available_date->Thu_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Thu_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Thu_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif

                      @if ($doctor->get_available_date->Wed_type)
                        <tr> <!---Wed Day --->
                            <td>  @lang('page.Wed') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Wed_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Wed_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Wed_type)}} </td>
                            @if ($doctor->get_available_date->Wed_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Wed_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Wed_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif

                      @if ($doctor->get_available_date->Fri_type)
                        <tr> <!---Fri Day --->
                            <td>  @lang('page.Fri') </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Fri_from_time))}} </td>
                            <td> {{date('h:i',strtotime($doctor->get_available_date->Fri_to_time))}} </td>
                            <td> {{__('page.'.$doctor->get_available_date->Fri_type)}} </td>
                            @if ($doctor->get_available_date->Fri_type == 'specific time')
                              <td> {{substr($doctor->get_available_date->Fri_wating_time,0,5)}} </td>
                              <td> <i class="fa fa-times-circle"></i> </td>
                            @else
                              <td>  <i class="fa fa-times-circle"></i> </td>
                              <td> {{$doctor->get_available_date->Fri_num_resrvation}} </td>
                            @endif
                        </tr>
                      @endif
                    </tbody>
                  </table>
            @else
              <p> @lang('page.No available times') </p>
            @endif

          @endslot
         @endcomponent

          </div><!--End col-md-6-->

      {{-- =======================================Right side======================================== --}}
          <div class="col-md-6">

      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.more info')
          @endslot
          @slot('body')

              <h2> @lang('page.specialization') :
                @if ($doctor->get_specialization_mini)
                    {{( \Session::get('lang') == 'ar' )? $doctor->get_specialization_mini->name_ar : $doctor->get_specialization_mini->name_en}}
                @else
                  @lang('page.No specialization selected')
                @endif
              </h2>

            <h2> @lang('page.doctor Sub Specialization') </h2>
            @if ($doctor->get_SubSpecialization->isEmpty())
                <p> @lang('page.sub SubSpecialization') </p>
            @else
              <ul class="mydirection">
                @foreach ($doctor->get_SubSpecialization as $SubSpec)
                    <li> {{ ( \Session::get('lang') == 'ar' )?$SubSpec->text_ar: $SubSpec->text_en}} </li>
                @endforeach
              </ul>
            @endif
            <br>
            <!-------------------------------------------->
              <div class="botton_border"> </div>
            <!-------------------------------------------->
            <h2> @lang('page.license') </h2>

            @forelse ($get_Licenses as $license)
                <img src="{{$license->image}}" width="400px" style="max-height:400px;margin: 0 auto; Display:table">
            @empty
                <p> @lang('page.No license') </p>
            @endforelse

            <!-------------------------------------------->
              <div class="botton_border"> </div>
            <!-------------------------------------------->

            <h2> @lang('page.Insurance companies') </h2>
            @if ($doctor->get_Insurance_companies->isEmpty())
                <p> @lang('page.No Insurance companies') </p>
            @else
              <ul class="mydirection">
                @foreach ($doctor->get_Insurance_companies as $company)
                    <li> {{$company->name}} </li>
                @endforeach
              </ul>
            @endif

            <!-------------------------------------------->
              <div class="botton_border"> </div>
            <!-------------------------------------------->

            <h1> @lang('page.Clicnic') </h1>

            @isset($doctor->get_ClicnicAddress->clinic_name_en)
                <table class="table mydir">
                    <tr>
                      <th> @lang('page.name Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->clinic_name_en}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.name English') </th>
                      <td> {{$doctor->get_ClicnicAddress->clinic_name_ar}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.area') </th>
                      <td>
                        @isset($doctor->get_ClicnicAddress->get_area->name_en)
                            @if (\Session::get('lang') == 'ar' )
                                {{$doctor->get_ClicnicAddress->get_area->name_ar}}
                            @else
                                {{$doctor->get_ClicnicAddress->get_area->name_en}}
                            @endif
                        @endisset
                       </td>
                    </tr>
                    <tr>
                      <th> @lang('page.number') </th>
                      <td> {{$doctor->get_ClicnicAddress->clinic_number??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.block English') </th>
                      <td> {{$doctor->get_ClicnicAddress->block_en??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.block Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->block_ar??''}} </td>
                    </tr>

                    <tr>
                      <th> @lang('page.street name Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->street_name_ar??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.street name English') </th>
                      <td> {{$doctor->get_ClicnicAddress->street_name_en??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.landmark Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->landmark_en??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.landmark Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->landmark_ar??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.apartment English') </th>
                      <td> {{$doctor->get_ClicnicAddress->apartment_en??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.apartment Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->apartment_ar??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.building number Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->building_number_ar??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.building number English') </th>
                      <td> {{$doctor->get_ClicnicAddress->building_number_en??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.floor English') </th>
                      <td> {{$doctor->get_ClicnicAddress->floor_en??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.floor Arabic') </th>
                      <td> {{$doctor->get_ClicnicAddress->floor_ar??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.lat') </th>
                      <td> {{$doctor->get_ClicnicAddress->lat??''}} </td>
                    </tr>
                    <tr>
                      <th> @lang('page.lang') </th>
                      <td> {{$doctor->get_ClicnicAddress->lang??''}} </td>
                    </tr>

                </table>

                <div class="panel-body panel-body-map">
                    <div id="google_ptm_map" style="width: 100%; height: 300px;"></div>
                </div>

            @else
                <p> @lang('page.No Clicnic') </p>
            @endisset

          @endslot
         @endcomponent

          </div><!--End col-md-6-->
      </div><!--End row-->

    {{-- =============================================left side========================================= --}}
  @component('componets.panel_default')
    @slot('panel_title')
          @lang('page.Clicnic Images')
    @endslot
    @slot('body')

      @if($doctor->get_ClinicPicture->isEmpty())
          <p> @lang('page.No Clicnic Images') </p>
      @else
          @foreach ($doctor->get_ClinicPicture as $image)
              <img src="{{asset('images/Clinic/'.$image->image)}}" class="img-thumbnail" width="200px" style="max-height:200px;">
          @endforeach
      @endif
      <br>
    @endslot
   @endcomponent
      <!-------------------------------------------->
        {{-- <div class="botton_border"> </div> --}}
      <!-------------------------------------------->

      @component('componets.panel_default')
        @slot('panel_title')
              @lang('page.patient resirvation (last 15)')
        @endslot
        @slot('body')

      @if ($doctor->get_reservationed_patient->isEmpty())
        @lang('page.No patient resirvation')
      @else
      <table class="table mydir">
        <thead>
            <th> @lang('page.name') </th>
            <th> @lang('page.type') </th>
            <th> @lang('page.date') </th>
            <th> @lang('page.time') </th>
        </thead>
          @foreach ($doctor->get_reservationed_patient_limit as $reservationed)
              <tr>
                  @if (\Session::get('lang') == 'ar' )
                    <td>  {{$reservationed->name}} </td>
                    <td>  {{($reservationed->pivot->type == 'specific time' )? ' وقت محدد ' : " الاسبقية " }} </td>
                    <td>  {{$reservationed->pivot->date}}   </td>
                    <td>  {{date("H:i",strtotime($reservationed->pivot->time))}}   </td>
                  @else
                    <td>  {{$reservationed->name}} </td>
                    <td>  {{$reservationed->pivot->type}} </td>
                    <td>  {{$reservationed->pivot->date}}  </td>
                    <td>  {{date("H:i",strtotime($reservationed->pivot->time))}}  </td>
                  @endif
              </tr>
          @endforeach
      </table>
      @endif
    @endslot
   @endcomponent


    </div><!--End panel-body-->
   </div>

 @else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission

@endsection



@section('script')
  <!-- START THIS PAGE PLUGINS-->
  {{-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script> --}}
  {{-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7FUiJzJb8T0MubHLFf4VoKT5DrWhqFjE&v=3.exp&sensor=false&libraries=places"></script> --}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGmN1GmN4mJz32R_E0oX9qGUc8hlEGT8o&callback=initMap" async defer></script>

<script>
var get_lat = '{{$doctor->get_ClicnicAddress->lat??0}}';
var get_lang = '{{$doctor->get_ClicnicAddress->lang??0}}';

function initMap()
{

      var myLatlng = new google.maps.LatLng(get_lat,get_lang);
      var mapOptions = {
       zoom: 13,
       center: myLatlng
      }

      var map = new google.maps.Map(document.getElementById("google_ptm_map"), mapOptions);

      var marker = new google.maps.Marker({
         position: myLatlng,
         map: map,
         draggable:true,
      }); 

      // google.maps.event.addListener(marker, 'dragend', function(event) {
      //   var myLatLng = event.latLng;
      //    var lat = myLatLng.lat();
      //    var lng = myLatLng.lng();
      //
      //    document.getElementById("latitude").value = lat;
      //
      //    document.getElementById("longitude").value = lng;
      // });
}

</script>
@endsection
