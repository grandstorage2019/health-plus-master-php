

@extends('atlant.blank')

@section('content')
      @php($active='Dashboard')

      @permission('DashBoard')

      <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>

      <style media="screen">
          .widget-data
          {
              padding-right: 20px;
          }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12" id="myroot">


        <br>
    <!-- START WIDGETS -->
    <div class="row">
      <div class="col-md-3">

          <!-- START WIDGET MESSAGES -->
          <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Patient')}}';" style="cursor: pointer">
                <div class="widget-item-left">
                    <span class="fa fa-user"></span>
                </div>
              <div class="widget-data">
                  <div class="widget-int num-count"> {{$patient_count}} </div>
                  <div class="widget-title">  @lang('page.Patient')  </div>
                  <div class="widget-subtitle"> @lang('page.All Patient') </div>
              </div>
          </div>
          <!-- END WIDGET MESSAGES -->

      </div>
        <div class="col-md-3">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Doctor')}}';" style="cursor: pointer">
                  <div class="widget-item-left">
                      <i class="fa fa-wheelchair"></i>
                  </div>
                <div class="widget-data">
                    <div class="widget-int num-count"> {{$doctor_count}} </div>
                    <div class="widget-title"> @lang('page.Doctor') </div>
                    <div class="widget-subtitle"> @lang('page.All Doctor')  </div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
        <div class="col-md-3">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('Care')}}';" style="cursor: pointer">
                  <div class="widget-item-left">
                      <i class="fa fa-weibo"></i>
                  </div>
                <div class="widget-data">
                    <div class="widget-int num-count"> {{$care_count}} </div>
                    <div class="widget-title"> @lang('page.Care') </div>
                    <div class="widget-subtitle">  @lang('page.All Care')  </div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
        <div class="col-md-3">

            <!-- START WIDGET MESSAGES -->
            <div class="widget widget-default widget-item-icon" onclick="location.href='{{url('CareOffer')}}';" style="cursor: pointer">
                  <div class="widget-item-left">
                    <i class="fa fa-gift"></i>
                  </div>
                <div class="widget-data">
                    <div class="widget-int num-count"> {{$careOffer_count}} </div>
                    <div class="widget-title">  @lang('page.Care Offer') </div>
                    <div class="widget-subtitle">  @lang('page.All Care Offer')  </div>
                </div>
            </div>
            <!-- END WIDGET MESSAGES -->

        </div>
    </div>
    <!-- END WIDGETS -->

{{-- ---------------------------START Calender-------------------------- --}}
    <div class="row">
          <div class="col-md-2"> </div>
          <div class="col-md-8">
            @component('componets.panel_default')
               @slot('panel_title')
                   @lang('page.Reservation last 50')
               @endslot
              @slot('body')
                <div id="calendar1"> </div>
              @endslot
           @endcomponent
      </div><!--End col-md-8-->
    </div><!--End row-->
{{-- ---------------------------End Calender-------------------------- --}}


    {{-- ---------------------------patients charts-------------------------- --}}
  <div class="row">
        <div class="col-md-8">
               @component('componets.panel_default')
                  @slot('panel_title')
                      @lang('page.patients') ({{\Carbon\Carbon::now()->year}})
                  @endslot
                  @slot('body')
                        <div id="morris_patients_months"></div>
                  @endslot
               @endcomponent
        </div><!--End col-md-8-->

      <div class="col-md-4">

            @component('componets.panel_default')
               @slot('panel_title')
                   @lang('page.patients') ({{\Carbon\Carbon::now()->year}})
               @endslot
               @slot('body')
                   <div id="morris_patient_dount"style="height:255px" ></div>
                   <table class="table mydirection"  >
                       <tr>
                          <th> @lang('page.android') </th>
                          <td> {{$android_patient_count}} </td>
                       </tr>
                       <tr>
                          <th> @lang('page.Ios') </th>
                          <td> {{$ios_patient_count}}  </td>
                       </tr>
                   </table>
               @endslot
            @endcomponent

      </div><!--End col-md-4-->
  </div><!--End row-->
  {{-- ---------------------------End users charts-------------------------- --}}

    {{-- ---------------------------scand row charts-------------------------- --}}
    <div class="row">
          <div class="col-md-8">
                 @component('componets.panel_default')
                    @slot('panel_title')
                        @lang('page.Doctor') ({{\Carbon\Carbon::now()->year}})
                    @endslot
                    @slot('body')
                          <div id="morris_Doctor_months"></div>
                    @endslot
                 @endcomponent
          </div><!--End col-md-8-->

        <div class="col-md-4">

              @component('componets.panel_default')
                 @slot('panel_title')
                     @lang('page.Doctor') ({{\Carbon\Carbon::now()->year}})
                 @endslot
                 @slot('body')
                     <div id="morris_Doctor_dount"style="height:255px" ></div>
                     <table class="table mydirection"  >
                         <tr>
                            <th> @lang('page.is active') </th>
                            <td> {{$Accapted_by_admin}} </td>
                         </tr>
                         <tr>
                            <th> @lang('page.not active') </th>
                            <td> {{$Not_accapted_by_admin}}  </td>
                         </tr>
                     </table>
                 @endslot
              @endcomponent

        </div><!--End col-md-4-->
    </div><!--End row-->
  {{-- ---------------------------End scand row  charts-------------------------- --}}

    {{-- ---------------------------thired row charts-------------------------- --}}
    <div class="row">
          <div class="col-md-8">
                 @component('componets.panel_default')
                    @slot('panel_title')
                        @lang('page.care') ({{\Carbon\Carbon::now()->year}})
                    @endslot
                    @slot('body')
                          <div id="morris_care_months"></div>
                    @endslot
                 @endcomponent
          </div><!--End col-md-8-->

        <div class="col-md-4">

              @component('componets.panel_default')
                 @slot('panel_title')
                     @lang('page.Advertising') ({{\Carbon\Carbon::now()->year}})
                 @endslot
                 @slot('body')
                     <div id="morris_Advertising_dount"style="height:255px" ></div>
                     <table class="table mydirection"  >
                         <tr>
                            <th> @lang('page.care Advertising') </th>
                            <td> {{$careAdvertising_count}} </td>
                         </tr>
                         <tr>
                            <th> @lang('page.patient Advertising') </th>
                            <td> {{$patientAdvertising_count}}  </td>
                         </tr>
                     </table>
                 @endslot
              @endcomponent

        </div><!--End col-md-4-->
    </div><!--End row-->
  {{-- ---------------------------End thired row charts-------------------------- --}}

    {{-- ---------------------------thired row charts-------------------------- --}}
    <div class="row">
          <div class="col-md-8">
                 @component('componets.panel_default')
                    @slot('panel_title')
                        @lang('page.Reservation') ({{\Carbon\Carbon::now()->year}})
                    @endslot
                    @slot('body')
                          <div id="morris_Reservation_months"></div>
                    @endslot
                 @endcomponent
          </div><!--End col-md-8-->

        <div class="col-md-4">

              @component('componets.panel_default')
                 @slot('panel_title')
                     @lang('page.Reservation') ({{\Carbon\Carbon::now()->year}})
                 @endslot
                 @slot('body')
                     <div id="morris_Reservation_dount"style="height:255px" ></div>
                     <table class="table mydirection"  >
                         <tr>
                            <th> @lang('page.specific Time') </th>
                            <td> {{$Reservation_specificTime}} </td>
                         </tr>
                         <tr>
                            <th> @lang('page.first Reservation') </th>
                            <td> {{$Reservation_firstReservation}}  </td>
                         </tr>
                     </table>
                 @endslot
              @endcomponent

        </div><!--End col-md-4-->
    </div><!--End row-->
  {{-- ---------------------------End thired row charts-------------------------- --}}



      </div><!--End col-md-12 root-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission



@endsection


@section('script')

  <script type="text/javascript" src="{{asset('atlant/js/plugins/morris/raphael-min.js')}}"></script>
  <script type="text/javascript" src="{{asset('atlant/js/plugins/morris/morris.min.js')}}"></script>

  <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
   <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.4.0/fullcalendar.js"></script>

  <script>

    init_Patient_Donut();
    Patient_MorrisArea();
    init_Doctor_Donut();
    Doctor_MorrisArea();
    init_Advertising_Donut();
    Care_MorrisArea();
    Reservation_MorrisArea();
    init_Reservation_Donut();

    $(document).ready(function() {
      $('#calendar1').fullCalendar({
                          header: {
                              left: 'prev,next today',
                              center: 'title',
                              right: 'month,agendaWeek,listWeek' //agendaDay,listMonth
                           },
                           defaultView: 'agendaWeek',
                           aspectRatio: 1.8,
                           events: [
                            @foreach ($Reservations_calender as $reservation)
                               {"id":'{{$reservation->id}}',"title":"doc:{{$reservation->doctor_name}} \n pati:{{$reservation->patient_name}} ",
                               "start":"{{$reservation->date}} {{$reservation->time}}",
                               "end":"{{$reservation->date}} {{$reservation->time}}",
                               "className":"customEventsClass","url":'' },
                            @endforeach
                        ]

                    });//end $('#calendar').fullCalendar
          });//end $(document).ready

    function init_Patient_Donut()
    {
          Morris.Donut({
              element: 'morris_patient_dount',
              data: [{
                      label: ' @lang('page.android') ',
                      value: {{$android_patient_count}}
                  }, {
                      label: ' @lang('page.Ios') ',
                      value: {{$ios_patient_count}}
                  }
              ],
              colors: ['#93e3ff', '#b0dd91' ],
              formatter: function(y) {
                  return y
              }
          });
   }

   function Patient_MorrisArea()
   {
       var current_year = '{{\Carbon\Carbon::now()->year}}';
       // $('h2#report').append(' '+current_year);
       // $('.current_year').append(' '+current_year);
       @if (\Session::get('lang') == 'ar')
           var months=["يناير", "فبراير", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغستوس", "سبتمبر", "اكتوبر", "نوفمبير", "ديسمبر"];
       @else
           var months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
       @endif

       Morris.Area({
        element: 'morris_patients_months',
        data:
        [
               { month: current_year+'-1', count: 0 },
               { month: current_year+'-2', count: 0 },
               { month: current_year+'-3', count: 0 },
               { month: current_year+'-4', count: 0 },
               { month: current_year+'-5', count: 0 },
               { month: current_year+'-6', count: 0 },
               { month: current_year+'-7', count: 0 },
               { month: current_year+'-8', count: 0 },
               { month: current_year+'-9', count: 0 },
               { month: current_year+'-10', count: 0 },
               { month: current_year+'-11', count: 0 },
               { month: current_year+'-12', count: 0 },
                @foreach ($morris_Patients as $Patient)
                 {
                    month:current_year+'-{{$Patient->month}}',
                    count:'{{$Patient->count}}'
                 },
               @endforeach
        ],
       lineColors: ['#33414E', '#E0EEF9', '#ff758e'],
       xkey: 'month',
       ykeys: ['count'],
       labels: ["@lang('Patient')"],
       xLabelFormat:function(x) {
           var month=months[x.getMonth()];
           return month
       },
           dateFormat:function(x) {
               var month=months[new Date(x).getMonth()];
               return month
       },
       pointSize: 0,
       lineWidth: 0,
       resize: true,
       fillOpacity: 0.8,
       behaveLikeLine: true,
       gridLineColor: '#e0e0e0',
       hideHover: 'auto'
       });
   }

   function init_Doctor_Donut()
   {
         Morris.Donut({
             element: 'morris_Doctor_dount',
             data: [{
                     label: ' @lang('page.is active') ',
                     value: {{$Accapted_by_admin}}
                 }, {
                     label: ' @lang('page.not active') ',
                     value: {{$Not_accapted_by_admin}}
                 }
             ],
             colors: ['#008000', '#990000' ],
             formatter: function(y) {
                 return y
             }
         });
  }


function Doctor_MorrisArea()
{
    var current_year = '{{\Carbon\Carbon::now()->year}}';
    // $('h2#report').append(' '+current_year);
    // $('.current_year').append(' '+current_year);
    @if (\Session::get('lang') == 'ar')
        var months=["يناير", "فبراير", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغستوس", "سبتمبر", "اكتوبر", "نوفمبير", "ديسمبر"];
    @else
        var months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    @endif

    Morris.Area({
        element: 'morris_Doctor_months',
        data:
        [
          { month: current_year+'-1', count: 0 },
          { month: current_year+'-2', count: 0 },
          { month: current_year+'-3', count: 0 },
          { month: current_year+'-4', count: 0 },
          { month: current_year+'-5', count: 0 },
          { month: current_year+'-6', count: 0 },
          { month: current_year+'-7', count: 0 },
          { month: current_year+'-8', count: 0 },
          { month: current_year+'-9', count: 0 },
          { month: current_year+'-10', count: 0 },
          { month: current_year+'-11', count: 0 },
          { month: current_year+'-12', count: 0 },
           @foreach ($morris_Doctors as $Doctor)
            {
               month:current_year+'-{{$Doctor->month}}',
               count:'{{$Doctor->count}}'
            },
          @endforeach
    ],
    lineColors: ['#003333', '#E0EEF9', '#ff758e'],
    xkey: 'month',
    ykeys: ['count'],
    labels: ["@lang('Doctor')"],
    xLabelFormat:function(x) {
        var month=months[x.getMonth()];
        return month
    },
        dateFormat:function(x) {
            var month=months[new Date(x).getMonth()];
            return month
    },
    pointSize: 0,
    lineWidth: 0,
    resize: true,
    fillOpacity: 0.8,
    behaveLikeLine: true,
    gridLineColor: '#e0e0e0',
    hideHover: 'auto'
    });
}

function init_Advertising_Donut()
{
      Morris.Donut({
          element: 'morris_Advertising_dount',
          data: [{
                  label: ' @lang('page.care Advertising') ',
                  value: {{$careAdvertising_count}}
              }, {
                  label: ' @lang('page.patient Advertising') ',
                  value: {{$patientAdvertising_count}}
              }
          ],
          colors: ['#93e3ff', '#b0dd91' ],
          formatter: function(y) {
              return y
          }
      });
}


function Care_MorrisArea()
{
    var current_year = '{{\Carbon\Carbon::now()->year}}';
    // $('h2#report').append(' '+current_year);
    // $('.current_year').append(' '+current_year);
    @if (\Session::get('lang') == 'ar')
        var months=["يناير", "فبراير", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغستوس", "سبتمبر", "اكتوبر", "نوفمبير", "ديسمبر"];
    @else
        var months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    @endif

    Morris.Area({
        element: 'morris_care_months',
        data:
        [
          { month: current_year+'-1', count: 0 },
          { month: current_year+'-2', count: 0 },
          { month: current_year+'-3', count: 0 },
          { month: current_year+'-4', count: 0 },
          { month: current_year+'-5', count: 0 },
          { month: current_year+'-6', count: 0 },
          { month: current_year+'-7', count: 0 },
          { month: current_year+'-8', count: 0 },
          { month: current_year+'-9', count: 0 },
          { month: current_year+'-10', count: 0 },
          { month: current_year+'-11', count: 0 },
          { month: current_year+'-12', count: 0 },
           @foreach ($morris_Cares as $Care)
            {
               month:current_year+'-{{$Care->month}}',
               count:'{{$Care->count}}'
            },
          @endforeach
    ],
    lineColors: ['#191967', '#E0EEF9', '#ff758e'],
    xkey: 'month',
    ykeys: ['count'],
    labels: ["@lang('Care')"],
    xLabelFormat:function(x) {
        var month=months[x.getMonth()];
        return month
    },
        dateFormat:function(x) {
            var month=months[new Date(x).getMonth()];
            return month
    },
    pointSize: 0,
    lineWidth: 0,
    resize: true,
    fillOpacity: 0.8,
    behaveLikeLine: true,
    gridLineColor: '#e0e0e0',
    hideHover: 'auto'
    });
}


function init_Reservation_Donut()
{
      Morris.Donut({
          element: 'morris_Reservation_dount',
          data: [{
                  label: ' @lang('page.specific Time') ',
                  value: {{$Reservation_specificTime}}
              }, {
                  label: ' @lang('page.first Reservation') ',
                  value: {{$Reservation_firstReservation}}
              }
          ],
          colors: ['#93e3ff', '#b0dd91' ],
          formatter: function(y) {
              return y
          }
      });
}

function Reservation_MorrisArea()
{
    var current_year = '{{\Carbon\Carbon::now()->year}}';
    // $('h2#report').append(' '+current_year);
    // $('.current_year').append(' '+current_year);
    @if (\Session::get('lang') == 'ar')
        var months=["يناير", "فبراير", "مارس", "ابريل", "مايو", "يونيو", "يوليو", "اغستوس", "سبتمبر", "اكتوبر", "نوفمبير", "ديسمبر"];
    @else
        var months=["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    @endif

    Morris.Area({
        element: 'morris_Reservation_months',
        data:
        [
          { month: current_year+'-1', count: 0 },
          { month: current_year+'-2', count: 0 },
          { month: current_year+'-3', count: 0 },
          { month: current_year+'-4', count: 0 },
          { month: current_year+'-5', count: 0 },
          { month: current_year+'-6', count: 0 },
          { month: current_year+'-7', count: 0 },
          { month: current_year+'-8', count: 0 },
          { month: current_year+'-9', count: 0 },
          { month: current_year+'-10', count: 0 },
          { month: current_year+'-11', count: 0 },
          { month: current_year+'-12', count: 0 },
           @foreach ($morris_Reservations as $Reservation)
            {
               month:current_year+'-{{$Reservation->month}}',
               count:'{{$Reservation->count}}'
            },
          @endforeach
    ],
    lineColors: ['#660000', '#E0EEF9', '#ff758e'],
    xkey: 'month',
    ykeys: ['count'],
    labels: ["@lang('Reservation')"],
    xLabelFormat:function(x) {
        var month=months[x.getMonth()];
        return month
    },
        dateFormat:function(x) {
            var month=months[new Date(x).getMonth()];
            return month
    },
    pointSize: 0,
    lineWidth: 0,
    resize: true,
    fillOpacity: 0.8,
    behaveLikeLine: true,
    gridLineColor: '#e0e0e0',
    hideHover: 'auto'
    });
}


  </script>

@endsection
