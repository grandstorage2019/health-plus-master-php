@extends('atlant.blank')

@section('content')
      @php($active='ContactUs')
      @permission('ContactUs')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Contact Us') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Contact Us') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      <br><br>
      {{-- <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div> --}}


      <table class="table mydirection">
          <thead>
            <th> @lang('page.Doctor') </th>
            <th> @lang('page.patient') </th>
            <th> @lang('page.id') </th>
            <th> @lang('page.phone') </th>
            <th> @lang('page.email') </th>
            <th> @lang('page.message') </th>
            <th> @lang('page.created at') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($ContactUs as $Contact)
                  <tr>
                        <td>
                            @isset ($Contact->get_doctor->fname_ar)
                                {{$Contact->get_doctor->fname_ar}}
                            @endisset
                            @isset ($Contact->get_doctor->lname_ar)
                                {{$Contact->get_doctor->lname_ar}}
                            @endisset

                            @isset ($Contact->get_doctor->fname_en)
                                  {{$Contact->get_doctor->fname_en}}
                            @endisset
                            @isset ($Contact->get_doctor->lname_en)
                                {{$Contact->get_doctor->lname_en}}
                            @endisset
                        </td>
                        <td>
                          @isset ($Contact->get_Patient->name)
                              {{$Contact->get_Patient->name}}
                          @endisset
                        </td>
                        <td>
                            @if ($Contact->doctor_id)
                                doctor id : {{$Contact->doctor_id}}
                            @elseif ($Contact->patient_id)
                                patient id : {{$Contact->patient_id }}
                            @endif
                        </td>
                        <td>
                              {{isset($Contact->get_doctor->phone)?$Contact->get_doctor->phone:''}}
                              {{isset($Contact->get_Patient->phone)?$Contact->get_Patient->phone:''}}
                        </td>
                        <td>
                              {{isset($Contact->get_doctor->email)?$Contact->get_doctor->email:''}}
                              {{isset($Contact->get_Patient->email)?$Contact->get_Patient->email:''}}
                        </td>

                          <td width="40%"> {{$Contact->message}} </td>
                          <td > {{$Contact->created_at}} <br> [{{$Contact->created_at->diffForHumans()}}] </td>
                          <td>
                            <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('ContactUs/delete/'.$Contact->id)}}',`{{$Contact->message}}`)" >
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                          </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$ContactUs->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission
@endsection
@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('ContactUs')}}';
    </script>



@endsection
