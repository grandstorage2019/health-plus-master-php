

<!-- START PAGE SIDEBAR -->
<div class="page-sidebar page-sidebar-fixed scroll">
    <!-- START X-NAVIGATION -->
    <ul class="x-navigation">
        <li class="xn-logo">
            {{-- <a href="index.html">ATLANT</a> --}}
            <a href="{{url('magazines')}}"> اسرار </a>
            <a href="#" class="x-navigation-control"></a>
        </li>
        <li class="xn-profile">
              {{-- <a href="#" class="profile-mini">
                  <img src="{{asset('atlant/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
              </a>
              <div class="profile">
                  <div class="profile-image">
                      <img src="{{asset('atlant/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                  </div>
                  <div class="profile-data">
                      <div class="profile-data-name">John Doe</div>
                      <div class="profile-data-title">Web Developer/Designer</div>
                  </div>
                  <div class="profile-controls">
                      <a href="pages-profile.html" class="profile-control-left"><span class="fa fa-info"></span></a>
                      <a href="pages-messages.html" class="profile-control-right"><span class="fa fa-envelope"></span></a>
                  </div>
              </div> --}}
        </li>
        @if (!isset($active))
          @php($active = '')
        @endif
          <li class="xn-title">Navigation</li>
          @permission('DashBoard')
          <li class="{{($active == 'Dashboard')?'active':''}}" >
              <a href="{{url('DashBoard')}}" >  <span class="xn-text"> @lang('page.Dashboard')</span> <span class="fa fa-home mydir"></span>  </a>
          </li>
          @endpermission

            <li class="xn-openable {{($active == 'Country'||$active == 'Governorate'||$active == 'Area')?'active':''}}">
              <a href="#"><span class="xn-text">@lang('page.Location')</span> <i class="fa fa-location-arrow mydir"></i>    </a>
              <ul>
                    @permission('Country')
                    <li class="{{($active == 'Country')?'active':''}}" >
                        <a href="{{url('Country')}}" >  <span class="xn-text">
                          @lang('page.Country')</span> <i class="fa fa-location-arrow mydir"></i>  </a>
                    </li>
                    @endpermission

                    @permission('Governorate')
                    <li class="{{($active == 'Governorate')?'active':''}}" >
                        <a href="{{url('Governorate')}}"><span class="xn-text">
                          @lang('page.Governorate') </span> <i class="fa fa-location-arrow mydir"></i> </a>
                    </li>
                    @endpermission

                    @permission('Area')
                    <li class="{{($active == 'Area')?'active':''}}" >
                        <a href="{{url('Area')}}"><span class="xn-text">
                          @lang('page.Area') </span> <i class="fa fa-location-arrow mydir"></i> </a>
                    </li>
                    @endpermission
              </ul>
          </li>




          <li class="xn-openable {{($active == 'DocSpecialization'||$active == 'CareSpecialization'||$active=='DocSubSpecialization')?'active':''}}">
            <a href="#"><span class="xn-text">@lang('page.Specialization')</span> <i class="fa fa-briefcase mydir"></i>    </a>
            <ul>
              @permission('DocSpecialization')
              <li class="{{($active == 'DocSpecialization')?'active':''}}" >
                  <a href="{{url('DocSpecialization')}}" >  <span class="xn-text"> @lang('page.Doc Specialization')</span>
                      <i class="fa fa-briefcase mydir"></i>
                  </a>
              </li>
              @endpermission

              @permission('DocSubSpecialization')
              <li class="{{($active == 'DocSubSpecialization')?'active':''}}" >
                  <a href="{{url('DocSubSpecialization')}}" >  <span class="xn-text"> @lang('page.Doc Sub Specialization')</span>
                      <i class="fa fa-briefcase mydir"></i>
                  </a>
              </li>
              @endpermission

              @permission('CareSpecialization')
              <li class="{{($active == 'CareSpecialization')?'active':''}}" >
                  <a href="{{url('CareSpecialization')}}" >  <span class="xn-text"> @lang('page.Care Specialization')</span>
                      <i class="fa fa-briefcase mydir"></i>
                  </a>
              </li>
              @endpermission

            </ul>
        </li>

          <li class="xn-openable {{($active == 'PatientAdvertising'||$active == 'CareAdvertising')?'active':''}}">
            <a href="#"><span class="xn-text">@lang('page.Advertising')</span> <i class="glyphicon glyphicon-pushpin mydir"></i></span>    </a>
            <ul>
                @permission('PatientAdvertising')
                <li class="{{($active == 'PatientAdvertising')?'active':''}}" >
                      <a href="{{url('PatientAdvertising')}}" >
                          <span class="xn-text"> @lang('page.Patient Advertising')</span> <i class="glyphicon glyphicon-pushpin mydir"></i> </span>
                      </a>
                </li>
                @endpermission

                @permission('CareAdvertising')
                <li class="{{($active == 'CareAdvertising')?'active':''}}" >
                      <a href="{{url('CareAdvertising')}}" >
                          <span class="xn-text"> @lang('page.Care Advertising')</span> <i class="glyphicon glyphicon-pushpin mydir"></i> </span>
                      </a>
                </li>
                @endpermission
            </ul>
        </li>

        @permission('InsuranceCompany')
        <li class="{{($active == 'InsuranceCompany')?'active':''}}" >
            <a href="{{url('InsuranceCompany')}}" >  <span class="xn-text"> @lang('page.Insurance Company')</span> <i class="fa fa-hospital-o mydir"></i> </a>
        </li>
        @endpermission

        @permission('Plan')
        <li class="{{($active == 'Plan')?'active':''}}" >
            <a href="{{url('Plan')}}" >  <span class="xn-text"> @lang('page.Plan')</span> <i class="glyphicon glyphicon-align-center mydir"></i>  </a>
        </li>
        @endpermission

        @permission('Patient')
        <li class="{{($active == 'Patient')?'active':''}}" >
            <a href="{{url('Patient')}}" >  <span class="xn-text"> @lang('page.Patient')</span> <i class="fa fa-wheelchair mydir"></i>  </a>
        </li>
        @endpermission
        @permission('healthReservations_index')
        <li class="{{($active == 'health_Reservation')?'active':''}}" >
            <a href="{{route('health_Reservation')}}" >  <span class="xn-text"> @lang('page.health_Reservation')</span> <i class="fa fa-calendar mydir"></i>  </a>
        </li>
        @endpermission
        @permission('CanclePatient_index')
        <li class="{{($active == 'CanclePatient')?'active':''}}" >
            <a href="{{url('CanclePatient')}}" >  <span class="xn-text"> @lang('page.CanclePatient')</span> <i class="fa fa-times mydir"></i>  </a>
        </li>
        @endpermission
        @permission('careReservation_index')
        <li class="{{($active == 'care_Reservation')?'active':''}}" >
            <a href="{{route('care_Reservation')}}" >  <span class="xn-text"> @lang('page.care_Reservation')</span> <i class="fa fa-calendar mydir"></i>  </a>
        </li>
        @endpermission
        @permission('CancleCare_index')
        <li class="{{($active == 'CancleCare')?'active':''}}" >
            <a href="{{url('CancleCare')}}" >  <span class="xn-text"> @lang('page.CancleCare')</span> <i class="fa fa-times mydir"></i>  </a>
        </li>
        @endpermission
        {{-- <li class="xn-openable {{($active == 'health_Reservation'||$active == 'care_Reservation')?'active':''}}">
          <a href="#"><span class="xn-text">@lang('page.Reservation')</span> <i class="fa fa-calendar mydir"></i>   </a>
          <ul>

              {{-- @permission('Doctor_index') --}}
              {{-- <li class="{{($active == 'health_Reservation')?'active':''}}" >
                  <a href="{{route('health_Reservation')}}" >  <span class="xn-text"> @lang('page.health_Reservation')</span> <i class="fa fa-calendar mydir"></i>  </a>
              </li> --}}
              {{-- @endpermission --}}

              {{-- @permission('DoctorServices') --}}
              {{-- <li class="{{($active == 'care_Reservation')?'active':''}}" >
                  <a href="{{url('care_Reservation')}}" >  <span class="xn-text"> @lang('page.care_Reservation')</span> <i class="fa fa-calendar mydir"></i>  </a>
              </li> --}}
              {{-- @endpermission  --}}

          {{-- </ul>
      </li> --}}

        <li class="xn-openable {{($active == 'Doctor'||$active == 'DoctorServices')?'active':''}}">
          <a href="#"><span class="xn-text">@lang('page.Doctor')</span> <i class="fa fa-user-md mydir"></i>   </a>
          <ul>

              @permission('Doctor_index')
              <li class="{{($active == 'Doctor')?'active':''}}" >
                  <a href="{{url('Doctor')}}" >  <span class="xn-text"> @lang('page.Doctor')</span> <i class="fa fa-user-md mydir"></i>  </a>
              </li>
              @endpermission
              @permission('DoctorServices_index')
              <li class="{{($active == 'DoctorServices')?'active':''}}" >
                  <a href="{{url('DoctorServices')}}" >  <span class="xn-text"> @lang('page.DoctorServices')</span> <i class="fa fa-user-md mydir"></i>  </a>
              </li>
              @endpermission


          </ul>
      </li>

    <!--====================================================Care========================================================-->
        <li class="xn-openable {{($active == 'Care'||$active == 'CareSubServices'||$active=='CareOffer')?'active':''}}">
          <a href="#"><span class="xn-text">@lang('page.Care')</span> <i class="fa fa-weibo mydir"></i>   </a>
          <ul>
            @permission('Care')
            <li class="{{($active == 'Care')?'active':''}}" >
                <a href="{{url('Care')}}" >  <span class="xn-text"> @lang('page.Care')</span>
                      <i class="fa fa-weibo mydir"></i>
                </a>
            </li>
            @endpermission

            @permission('CareSubServices')
            <li class="{{($active == 'CareSubServices')?'active':''}}" >
                <a href="{{url('CareSubServices')}}" >  <span class="xn-text"> @lang('page.Care sub services') </span>
                    <i class="fa fa-weibo mydir"></i>
                </a>
            </li>
            @endpermission

            @permission('CareOffer')
            <li class="{{($active == 'CareOffer')?'active':''}}" >
                <a href="{{url('CareOffer')}}" >  <span class="xn-text"> @lang('page.Care Offer') </span>
                    <i class="fa fa-weibo mydir"></i>
                </a>
            </li>
            @endpermission

          </ul>
      </li>
<!--===================================================End care=========================================================-->

    <!--====================================================Notification========================================================-->
        <li class="xn-openable {{($active == 'NotificationCare'||$active == 'NotificationPatient')?'active':''}}">
          <a href="#"><span class="xn-text">@lang('page.Notification')</span> <i class="fa fa-globe mydir"></i>    </a>
          <ul>
            @permission('NotificationCare')
            <li class="{{($active == 'NotificationCare')?'active':''}}" >
                <a href="{{url('NotificationCare')}}" >  <span class="xn-text"> @lang('page.Notification Care')</span>
                    <i class="fa fa-globe mydir"></i>
                </a>
            </li>
            @endpermission

            @permission('NotificationPatient')
            <li class="{{($active == 'NotificationPatient')?'active':''}}" >
                <a href="{{url('NotificationPatient')}}" >  <span class="xn-text"> @lang('page.Notification Patient')</span>
                    <i class="fa fa-globe mydir"></i>
                </a>
            </li>
            @endpermission

          </ul>
      </li>
<!--===================================================End NotificationNotification=========================================================-->

    <!--====================================================admins Role========================================================-->
        @permission('mainSuperAdmin')
        <li class="xn-openable {{($active == 'Admin'||$active == 'Role')?'active':''}}">
          <a href="#"><span class="xn-text">@lang('page.Admin')</span> <i class="fa fa-gavel mydir"></i>   </a>
          <ul>
              <li class="{{($active == 'Admin')?'active':''}}" >
                  <a href="{{url('Admin')}}" >  <span class="xn-text"> @lang('page.Admin')</span>
                      <i class="fa fa-gavel mydir"></i>
                  </a>
              </li>
              <li class="{{($active == 'Role')?'active':''}}" >
                  <a href="{{url('Role')}}" >  <span class="xn-text"> @lang('page.Role')</span>
                      <i class="fa fa-gavel mydir"></i>
                  </a>
              </li>

          </ul>
      </li>
      @endpermission
<!--===================================================End NotificationNotification=========================================================-->

    <!--====================================================More========================================================-->

        <li class="xn-openable {{($active == 'AboutUs'||$active == 'ContactUs')?'active':''}}">
          <a href="#"><span class="xn-text">@lang('page.More')</span> <i class="fa fa-dot-circle-o mydir"></i>   </a>
          <ul>
              @permission('AboutUs')
              <li class="{{($active == 'AboutUs')?'active':''}}" >
                  <a href="{{url('AboutUs')}}" >  <span class="xn-text"> @lang('page.About Us')</span>
                      <i class="fa fa-dot-circle-o mydir"></i>
                  </a>
              </li>
              @endpermission

              @permission('ContactUs')
              <li class="{{($active == 'ContactUs')?'active':''}}" >
                  <a href="{{url('ContactUs')}}" >  <span class="xn-text"> @lang('page.Contact Us')</span>
                      <i class="fa fa-dot-circle-o mydir"></i>
                  </a>
              </li>
              @endpermission
          </ul>
      </li>

<!--===================================================End more=========================================================-->
    @permission('Setting')
    <li class="{{($active == 'Setting')?'active':''}}" >
        <a href="{{url('Setting')}}" >  <span class="xn-text"> @lang('page.Setting')</span>
            <i class="fa fa-cog mydir"></i>
        </a>
    </li>
    @endpermission

    </ul>
    <!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->
