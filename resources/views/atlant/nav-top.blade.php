<!-- START X-NAVIGATION VERTICAL -->
<ul class="x-navigation x-navigation-horizontal x-navigation-panel">
    <!-- POWER OFF -->
    <li class="xn-icon-button last">
        <a href="#"><span class="fa fa-power-off"></span></a>
        <ul class="animated zoomIn">
            {{-- <li><a href="pages-lock-screen.html">Lock Screen <span class="fa fa-lock"></span></a></li> --}}
            <li><a href="#" class="mb-control" data-box="#mb-signout"> تسجيل الدخول <span class="fa fa-sign-out"></span></a></li>

        </ul>
    </li>
    <!-- END POWER OFF -->
    <!-- SEARCH -->
    {{-- <li class="xn-search">
        <form role="form">
            <input type="text" name="search" placeholder="Search..."/>
        </form>
    </li> --}}
    <!-- END SEARCH -->
    <!-- TOGGLE NAVIGATION -->
    {{-- <li class="xn-icon-button pull-right">
        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
    </li> --}}
    <!-- END TOGGLE NAVIGATION -->
    <!-- MESSAGES -->
    @php( $count_unAccapted_doctors = adminNotification::get_unAccapted_doctors_count() )
     <li class="xn-icon-button pull-right">
        <a href="#"><span class="fa fa-comments"></span></a>
        <div class="informer informer-danger">{{$count_unAccapted_doctors }}</div>
        <div class="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="fa fa-comments"></span>  @lang('page.last 15 un accapted doctor') </h3>
                <div class="pull-left">
                    <span class="label label-danger">{{$count_unAccapted_doctors}} @lang('page.un accapted doctor') </span>
                </div>
            </div>
            <div class="panel-body list-group list-group-contacts scroll" style="height: 200px;">


                @foreach (adminNotification::get_unAccapted_doctors() as $doctor)
                    <a href="{{url('Doctor/edit/'.$doctor->id)}}" class="list-group-item">
                        <div class="list-group-status status-online"></div>
                        @if ($doctor->profile_image)
                            <img src="{{asset('images/doctor_profile/'.$doctor->profile_image)}}" class="pull-right" width="60px" style="max-height:70px;margin:1px 15px">
                        @else
                            <img src="{{asset('images/doctor_profile/doctor_image.jpg')}}" class="pull-right" width="60px" style="max-height:70px;margin:1px 15px">
                        @endif
                        <span class="contacts-title"> .
                          @if (\Session::get('lang') == 'ar' )
                              {{$doctor->fname_ar}} {{$doctor->lname_ar}}
                          @else
                              {{$doctor->fname_en}} {{$doctor->lname_en}}
                          @endif
                        </span>
                        <p> @lang('page.is not accapted yet') </p>
                    </a>
                @endforeach

            </div>
            <div class="panel-footer text-center">
                <a href="{{url('Doctor')}}"> @lang('page.Show all doctors') </a>
            </div>
        </div>
    </li>
    <!-- END MESSAGES -->
    <!-- TASKS -->

    <!-- END TASKS -->
    <!-- LANG BAR -->
    <li class="xn-icon-button pull-right">
        <a href="#"><span class="flag flag-gb"></span></a>
        <ul class="xn-drop-left xn-drop-white animated zoomIn">
            <li><a href="{{url('setLang/en')}}"> @lang('page.English') <span class="flag flag-gb"></span></a></li>
            <li><a href="{{url('setLang/ar')}}"> @lang('page.Arabic') <span class="flag flag-de"></span></a></li>
        </ul>
    </li>
    <!-- END LANG BAR -->
</ul>
<!-- END X-NAVIGATION VERTICAL -->
