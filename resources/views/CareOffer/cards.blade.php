
<style media="screen">
.my_card .more a,
.my_card .more button {
    width: 32%
  }
</style>

      {{-- <a class="btn btn-primary btn-rounded" id="btn_create" href="{{url('Item/create')}}"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </a>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') "  >
      </div>
      <div class="col-md-6">
         <a href="{{url('Item/list')}}" class="btn btn-primary btn-condensed"> <i class="fa fa-list"></i>  </a>
      </div> --}}
      <br><br>
<div class="cards_wraper">


@foreach ($offers as $offer)

    <div class="my_card advertising">

          <div class="title">
              @if ( app::getLocale() == 'ar' )
                  {{$offer->get_care->name_ar}}
              @else
                  {{$offer->get_care->name_en}}
              @endif
          </div><!--end title-->

          <div class="category">
            <span> @lang('page.phone'): </span>
               {{$offer->get_care->phone}}
          </div><!--end category-->

          <img src="{{asset('images/health_cares/'.$offer->image)}}" class="offer_img_card">

          <div class="price"> <span>  </span>{{$offer->link}} </div>

          <div class="more">
                {{-- <a href="{{url('Item/'.$offer->id)}}" class="btn btn-info">
                    <i class="fa fa-search"></i> --}}
                </a>
                <button class="btn btn-primary" onclick="edit_model('{{$offer->id}}','{{$offer->link}}','{{$offer->image}}','{{$offer->care_id}}')">
                    <i class="fa fa-pencil"></i>
                </button>
                @if ($offer->status)
                    <a href="{{url('CareOffer/showORhide/'.$offer->id)}}" class="btn btn-success" data-toggle="tooltip" title="@lang('page.click for deActivate')"> <i class="fa fa-eye"></i> </a>
                @else
                    <a href="{{url('CareOffer/showORhide/'.$offer->id)}}" class="btn btn-danger" data-toggle="tooltip" title="@lang('page.click for Activate')"> <i class="fa fa-eye-slash"></i> </a>
                @endif
                <button type="button" class="btn btn-danger" onclick="showDeleteMessage('{{url('CareOffer/delete/'.$offer->id)}}','{{$offer->name_en}}')" >
                    <i class="glyphicon glyphicon-trash"></i>
                </button>
          </div><!--end more-->

  </div><!--end card-->
@endforeach
