@extends('atlant.blank')

@section('content')
      <link rel="stylesheet" href="{{asset('css/my_css.css')}}">
      @php($active='CareOffer')
      @permission('CareOffer')

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Care Offer') </h2>
</div>

      <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      @include('CareOffer.cards')

      <div class="row">
            {{-- <div class="col-md-8 col-md-offset-5"> {{$PatientAdvertising->links()}} </div> --}}
      </div>

    </div><!--End panel-body-->
   </div>


{{-- ------------------- create ------------------------ --}}

    @component('componets.modal')
        @slot('id')
          create_model
        @endslot
        @slot('header')
          @lang('page.create new')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\CareOffer,['url'=>'CareOffer','class'=>"mydirection",'id'=>'create_form','files'=>true]) !!}
        @endslot
        @slot('body')

              <div class="form-group">
                {!! Form::label('image','الصورة:') !!}
                {!! Form::file('image',['class'=>'form-control','onchange'=>"change_image(this)",'required']) !!}
              </div>
              <img src="" id="create_img_temp" width="200px">
              <br>

              {!! Form::label('care_id', __('page.care') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::select('care_id',$cares,null,['class'=>'form-control select2','required']) !!}
              </div>


              {!! Form::label('link', __('page.link') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-link"></span></span>
                {!! Form::text('link',null,['class'=>'form-control','required']) !!}
              </div>
              <br>


        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.Add'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- edit ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          edit_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\CareOffer,['url'=>'CareOffer/update','class'=>"mydirection",'id'=>'edit_model','files'=>true]) !!}
        @endslot
        @slot('body')

              {!! Form::hidden('id',null,['id'=>'edit_id']) !!}
              <div class="form-group">
                {!! Form::label('image','الصورة:') !!}
                {!! Form::file('image',['class'=>'form-control','onchange'=>"change_image(this)"]) !!}
              </div>
              <img src="" id="edit_img_temp" width="200px">
              <br>

              {!! Form::label('care_id', __('page.care') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::select('care_id',$cares,null,['class'=>'form-control select2','id'=>'edit_care_id','required']) !!}
              </div>

              {!! Form::label('link', __('page.link') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-link"></span></span>
                {!! Form::text('link',null,['class'=>'form-control','id'=>'edit_link','required']) !!}
              </div>
              <br>


        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.update'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

  @else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission


@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('CareOffer')}}';
        var CareOffer_image_path = '{{asset('images/health_cares')}}';
    </script>
    <script src="{{asset('js/CareOffer.js')}}"> </script>


@endsection
