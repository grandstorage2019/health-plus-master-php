@extends('atlant.blank')

@section('content')
      @php($active='Governorate')
      @permission('Governorate')

      <style media="screen">
        .inp_error
        {
          border: 1px solid red;
        }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Governorate') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default" id="myVue">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Governorate') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
      {!! Form::model($gov = new \App\Governorate,['url'=>'Governorate','id'=>'create_form','v-on:submit'=>'do_submit($event)']) !!}
           <div class="row">
              <div class="col-md-6 mydirection ">
                  {!! Form::label('country_id',__('page.country')) !!}
                  {!! Form::select('country_id',$countries,null,['class'=>'form-control select2', 'id'=>'ddl_country_id' ,'v-model'=>'ddl_country_id','v-on:change'=>'change_country()']) !!}
              </div>
           </div><!--End row-->

           <hr>

          <div v-show="show_content">



            <div class="container brands-container">
               <div class="row">
                  <div class="col-md-8 col-md-offset-2"> 

                   <table class="table mydirection" id="table_Governorate">
                       <tr v-for= "(govern,index) in governora_list">
                           <td>
                                 {!! Form::label('name_en', __('page.name English') ) !!}
                                 {!! Form::text('name_en[]',null,['class'=>'form-control','required', 'v-model'=>"govern.name_en" ]) !!} <!-- ':value'=>"govern.name_en" -->
                           </td>
                           <td>
                                 {!! Form::label('name_ar', __('page.name Arabic') ) !!}
                                 {!! Form::text('name_ar[]',null,['class'=>'form-control','required','v-model'=>"govern.name_ar"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('status', __('page.active') ) !!} <br>
                                 <label class="switch">
                                     <input type="checkbox" class="switch" value="1" name="status[]" :checked="govern.status" />
                                     <span></span>
                                 </label>
                           </td>
                           <td>
                              <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_row()" v-if="!govern.id" > <i class="fa fa-trash-o"></i> </button>
                           </td>
                           <input type="hidden" name="id[]" :value="govern.id">
                       </tr>
                   </table>

                   <h2 class="mydirection div_add_brand">
                           <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_Governorate_row(index)" >
                               <i class="fa fa-plus"></i>
                           </button>
                           <span> @lang('page.Governorate') </span> <!-- class="mydir" -->
                   </h2>

                   {!! Form::submit(__('page.add'),['class'=>'btn btn-success']) !!}

          </div><!--End show_content-->


           </div><!--End container-->
         </div><!--End container-->
       </div><!--End container-->


      {!! Form::close() !!}
    </div><!--End panel-body-->
  </div><!--End my vue-->

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}
<script src="{{asset('js/Vue.js')}}"> </script>
    <script>
        var list_path = '{{asset('Governorate')}}';
        var get_governora_by_country_id = '{{asset('Governorate/get_by_country_id')}}';

    </script>

    <script src="{{asset('js/Governorate.js')}}"> </script>


@endsection
