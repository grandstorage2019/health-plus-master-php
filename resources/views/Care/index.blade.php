@extends('atlant.blank')

@section('content')
      @php($active='Care')
      @permission('Care_index')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">
 
<div class="page-title">
    <h2> @lang('page.Care') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Care') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
      @permission('Care_create')
      <a class="btn btn-primary btn-rounded" href="{{url('Care/create')}}"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </a>
      @endpermission
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th width="5%"> @lang('page.id') </th>
            <th> @lang('page.name') </th>
            <th> @lang('page.specialization') </th>
            <th> @lang('page.area') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($Cares as $care)
                  <tr>
                        <td>{{$care->id}}</td>
                        <td>
                          @if ($care->logo)
                              <img src="{{asset('images/care/'.$care->logo)}}" class="img-thumbnail mydir" width="60px" style="max-height:80px;margin:1px 15px">
                          @else
                              <img src="{{asset('images/patient_profile/patient_image.jpg')}}" class="img-thumbnail mydir" width="60px" style="max-height:80px;margin:1px 15px">
                          @endif
                            @if ( \Session::get('lang') == 'ar' )
                                {{$care->name_ar}}
                            @else
                                {{$care->name_en}}
                            @endif
                         </td>
                         <td>
                           @if ($care->get_specialization)
                              {{( \Session::get('lang') == 'ar' )?$care->get_specialization->name_ar:$care->get_specialization->name_en}}
                           @endif
                         </td>
                         <td>
                           @if ($care->get_area)
                              {{( \Session::get('lang') == 'ar' )?$care->get_area->name_ar:$care->get_area->name_en}}
                           @endif
                         </td>
                        <td>
                              @permission('Care_show')
                                <a href="{{url('Care/'.$care->id)}}" class="btn btn-primary btn-rounded"> <i class="fa fa-search"></i> </a>
                              @endpermission
                              @permission('Care_edit')
                                <a href="{{url('Care/edit/'.$care->id)}}" class="btn btn-warning btn-rounded" > <i class="fa fa-pencil"></i> </a>
                              @endpermission
                              @if ($care->status)
                                <a href="{{url('Care/showORhide/'.$care->id)}}" class="btn btn-success btn-rounded"> <i class="fa fa-eye"></i> </a>
                              @else
                                <a href="{{url('Care/showORhide/'.$care->id)}}" class="btn btn-danger btn-rounded"> <i class="fa fa-eye-slash"></i> </a>
                              @endif
                              @permission('Care_delete')
                              <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('Care/delete/'.$care->id)}}','{{$care->name_en}}')" >
                                  <i class="glyphicon glyphicon-trash"></i>
                              </button>
                              @endpermission
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$Cares->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
<br><br>
<div class="container">
    <h2> @lang('page.you dont have a permissions') </h2>
</div>
@endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('Care')}}';
    </script>
    {{-- <script src="{{asset('js/Country.js')}}"> </script> --}}


@endsection
