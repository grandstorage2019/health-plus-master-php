@extends('atlant.blank')

@section('content')
      @php($active='Care')
      @permission('Care_create')
      <style media="screen">
        .right_border
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
        }
        .botton_border
        {
          border-bottom: 1px solid #ccc;
        }
        .days td
        {
          text-align: center;
        }
        .inp_error
        {
            border: 1px solid red;
        }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12" id="myVue">

          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
              <li class="active"> @lang('page.create') </li>
              <li><a href="{{url('Care')}}"> @lang('page.Care') </a></li>
          </ul>
          <!-- END BREADCRUMB -->

<!-- START PANEL WITH STATIC CONTROLS -->
{{-- <div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.create Care') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="
                dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div> --}}



        {!! Form::model($care= new \App\Care,['url'=>'Care','novalidate','id'=>'create_form','files'=>true,'v-on:submit'=>'do_submit($event)']) !!}

      <div class="row">
          <div class="col-md-5 ">
            <br>
      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.Info')
          @endslot
          @slot('body')

            <div class="row">
                <div class="col-md-6">
                  <br><br><br>
                  {!! Form::file('logo',['class'=>'form-control','onchange'=>'show_image(this,"profile_image")','required']) !!}
                </div>
                <div class="col-md-6">
                    <img src="{{asset('images/doctor_profile/doctor_image.jpg')}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px" id="img_profile_image" >
                </div>
            </div><!--End row-->

              <table class="table mydirection">
                  <tr>
                    <th> @lang('page.name English') </th>
                    <td> {!! Form::text('name_en',null,['class'=>'form-control','required']) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.name Arabic') </th>
                    <td> {!! Form::text('name_ar',null,['class'=>'form-control','required']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.area') </th>
                    <td>  {!! Form::select('area_id',$areas,null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.specialization') </th>
                    <td>  {!! Form::select('specialization_id',$specializations,null,['class'=>'form-control','required']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.phone') </th>
                    <td> {!! Form::text('phone',null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.email') </th>
                    <td> {!! Form::text('email',null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  {{-- <tr>
                    <th> @lang('page.mobile') </th>
                    <td> {!! Form::text('mobile',null,['class'=>'form-control','required']) !!}   </td>
                  </tr> --}}
                  <tr>
                    <th> @lang('page.from time') </th>
                    <td> {!! Form::time('from_time',null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.to time') </th>
                    <td> {!! Form::time('to_time',null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.about English') </th>
                    <td> {!! Form::text('about_ar',null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.about Arabic') </th>
                    <td> {!! Form::text('about_en',null,['class'=>'form-control','required']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.location comment ar') </th>
                    <td> {!! Form::text('location_comment_ar',null,['class'=>'form-control','required']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.location comment en') </th>
                    <td> {!! Form::text('location_comment_en',null,['class'=>'form-control','required']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.facebook') </th>
                    <td> {!! Form::text('facebook',null,['class'=>'form-control']) !!}   </td>
                  </tr>
                  <tr>
                    <th> @lang('page.instagram') </th>
                    <td> {!! Form::text('instagram',null,['class'=>'form-control']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.twitter') </th>
                    <td> {!! Form::text('twitter',null,['class'=>'form-control']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.snapchat') </th>
                    <td> {!! Form::text('snapchat',null,['class'=>'form-control']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.insurance company') </th>
                    <td> {!! Form::select('insurance_company_id',$InsuranceCompany,null,['class'=>'form-control']) !!}  </td>
                  </tr>
                  <tr>
                    <th> @lang('page.latitude')  </th>
                    <td> {!! Form::number('lat',null,['class'=>'form-control','id'=>'latitude'  ]) !!} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.longitude')  </th>
                    <td> {!! Form::number('lang',null,['class'=>'form-control','id'=>'longitude'   ]) !!} </td>
                  </tr>
              </table>
              <div class="panel-body panel-body-map">
                  <div id="google_ptm_map" style="width: 100%; height: 300px;"></div>
              </div>
       @endslot
   @endcomponent
          </div><!--End col-md-5 -->

        {{-- =============================================right side====================================== --}}

          <div class="col-md-7">
      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.services')
          @endslot
          @slot('body')
              <br>

              <div id="Care_images" >

                  <table class="table mydirection add_services">
                      <tr v-for=" servic in services">
                          <td> {!! Form::file('services_image[]',['class'=>'form-control','onchange'=>'show_image(this,"Care_images")']) !!}  </td>
                          <td> <img src="" width="60px" > </td>
                          <td> {!! Form::text('services_name_ar[]',null,['class'=>'form-control','v-model'=>'servic.name_ar','placeholder'=>__('page.name Arabic')]) !!}  </td>
                          <td> {!! Form::text('services_name_en[]',null,['class'=>'form-control','v-model'=>'servic.name_en','placeholder'=>__('page.name English')]) !!}  </td>
                          <td>
                              <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_Care_images()">
                                  <i class="fa fa-trash-o"></i>
                              </button>
                          </td>
                      </tr>
                  </table>


                  <h2 class="mydirection div_add_brand">
                          <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_Care_service_row()" >
                              <i class="fa fa-plus"></i>
                          </button>
                          <span> @lang('page.Care service') </span>
                  </h2>

              </div><!--End Clicnic_images-->

        @endslot
       @endcomponent

          </div><!--End col-md-7 -->
      </div><!--End row-->

      <!--===========================================button side==================================-->

      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.images')
          @endslot
          @slot('body')

      <div id="Care_images"  style="width:50%;display:table;margin:0 auto" >

          <table class="table mydirection">
              <tr v-for="(image,index) in Care_images">
                  <td> {!! Form::file('Care_images[]',['class'=>'form-control','onchange'=>'show_image(this,"Care_images")']) !!} </td>
                  <td> <img src="" width="150px" >  </td>
                  <td>
                      <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_Care_images()">
                          <i class="fa fa-trash-o"></i>
                      </button>
                  </td>
              </tr>
          </table>

          <h2 class="mydirection div_add_brand">
                  <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_Care_images_row()" >
                      <i class="fa fa-plus"></i>
                  </button>
                  <span> @lang('page.Care image') </span>
          </h2>

      </div><!--End Clicnic_images-->
    @endslot
   @endcomponent


      <!-------------------------------------------->
        <div class="botton_border"> </div>
      <!-------------------------------------------->
      <br><br>
      {!! Form::submit(__('page.add'),['class'=>'btn btn-success ','style'=>'width:100%']) !!}

      <br><br>
    {!! Form::close() !!}


    </div><!--End panel-body-->
   </div>

 @else
<br><br>
<div class="container">
   <h2> @lang('page.you dont have a permissions') </h2>
</div>
@endpermission


@endsection


@section('script')
  <script type="text/javascript" src="{{asset('js/Vue.js')}}"></script>
  <script>

      var get_lat = '0';
      var get_lang = '0';

  </script>

  <script src="{{asset('js/care_create.js')}}"> </script>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGmN1GmN4mJz32R_E0oX9qGUc8hlEGT8o&callback=initMap" async defer></script>

@endsection
