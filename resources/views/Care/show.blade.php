@extends('atlant.blank')

@section('content')
      @php($active='Care')
      @permission('Care_show')
      <style media="screen">
        .right_border
        {
            border-left: 1px solid #ccc;
            border-right: 1px solid #ccc;
        }
        .botton_border
        {
          border-bottom: 1px solid #ccc;
        }
        .days td
        {
          text-align: center;
        }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
              <li class="active"> @lang('page.show') </li>
              <li><a href="{{url('Care')}}"> @lang('page.Care') </a></li>
          </ul>
          <!-- END BREADCRUMB -->





      <div class="row">
          <div class="col-md-5  ">

      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.Info')
          @endslot
          @slot('body')

            @if ($care->logo)
                <img src="{{asset('images/care/'.$care->logo)}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px">
            @else
                <img src="{{asset('images/patient_profile/patient_image.jpg')}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px">
            @endif
            <br><br>
            <span>
                <span class="mydir" style="margin-right:15px"> @lang('page.starts'):   </span>
                 @for ($i = 0; $i < (int)$care->get_starts(); $i++)
                   <i class="fa fa-star"></i>
                 @endfor
             </span>
             <br><br>
             <span> @lang('page.views'): {{$care->views}} </span>

              <table class="table mydirection">
                  <tr>
                    <th> @lang('page.name English') </th>
                    <td> {{$care->name_en}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.name Arabic') </th>
                    <td> {{$care->name_ar}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.area') </th>
                    <td> {{( \Session::get('lang') == 'ar' )?$care->get_area->name_ar:$care->get_area->name_en}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.specialization') </th>
                    <td> {{( \Session::get('lang') == 'ar' )?$care->get_specialization->name_ar:$care->get_specialization->name_en}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.phone') </th>
                    <td> {{$care->phone}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.email') </th>
                    <td> {{$care->email}} </td>
                  </tr>
                  {{-- <tr>
                    <th> @lang('page.mobile') </th>
                    <td> {{$care->mobile}} </td>
                  </tr> --}}
                  <tr>
                    <th> @lang('page.from time') </th>
                    <td> {{$care->from_time}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.to time') </th>
                    <td> {{$care->to_time}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.about_ar') </th>
                    <td> {{$care->about_ar}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.about_en') </th>
                    <td> {{$care->about_en}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.location comment ar') </th>
                    <td> {{$care->location_comment_ar}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.location comment en') </th>
                    <td> {{$care->location_comment_en}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.facebook') </th>
                    <td> {{$care->facebook}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.instagram') </th>
                    <td> {{$care->instagram}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.twitter') </th>
                    <td> {{$care->twitter}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.snapchat') </th>
                    <td> {{$care->snapchat}} </td>
                  </tr>
                  <tr>
                    <th> @lang('page.insurance company') </th>
                    <td> {{$care->get_insurance_company->name_en??''}} </td>
                  </tr>
              </table>

            @endslot
           @endcomponent
          </div><!--End col-md-5 -->

{{-- ====================================================right side ================================================ --}}

          <div class="col-md-7">

      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.services')
          @endslot
          @slot('body')
              <br>


                @if ($care->get_service->isEmpty())
                    <p> @lang('page.no services') </p>
                @else
                  <ul class="mydirection">
                        @foreach ($care->get_service as $servic)
                          <li>
                              @if ($servic->logo)
                                  <img src="{{asset('images/care_services/'.$servic->logo)}}" width="60px" >
                              @else
                                  <img src="{{asset('images/patient_profile/patient_image.jpg')}}" width="60px"  >
                              @endif
                                {{( \Session::get('lang') == 'ar' )?$care->name_ar:$care->name_en}}
                          </li>

                            @if ($servic->get_subService->isEmpty())
                              @lang('page.No sub services , you can add from side menu -> care -> care sub services')
                            @else

                              <table class="table" style="margin: 1px 10px">
                                <thead>
                                  <th> @lang('page.sub service name') </th>
                                  <th> @lang('page.sub service quantity') </th>
                                  <th> @lang('page.sub service price') </th>
                                </thead>
                                <tbody>
                                    @foreach ($servic->get_subService as $subService)
                                        <tr class="info">
                                          <td> {{( \Session::get('lang') == 'ar' )?$subService->name_ar:$subService->name_en}} </td>
                                          <td> {{$subService->quantity}} </td>
                                          <td> {{$subService->price}} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              </table>
                            @endif
                            <hr>
                        @endforeach
                  </ul>

                @endif

              @endslot
             @endcomponent
          </div><!--End col-md-7 -->
      </div><!--End row-->

      <!--------------------------------------------->

      @component('componets.panel_default')
          @slot('panel_title')
                @lang('page.images')
          @endslot
          @slot('body')

            @if ($care->get_pictures->isEmpty())
                <p> @lang('page.no images') </p>
            @else
              @foreach ($care->get_pictures as $picture)
                      <img src="{{asset('images/health_cares/'.$picture->image)}}" class="img-thumbnail" width="200px" style="max-height:200px;">
              @endforeach
            @endif

      @endslot
     @endcomponent



    </div><!--End panel-body-->
   </div>


 @else
<br><br>
<div class="container">
   <h2> @lang('page.you dont have a permissions') </h2>
</div>
@endpermission

@endsection


@section('script')


@endsection
