@extends('atlant.blank')

@section('content')
      @php($active='Setting')
      @permission('Setting')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Setting') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Setting') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

       {!! Form::model($Setting= new \App\Setting,['url'=>'Setting' ,'id'=>'create_form','files'=>true]) !!}

      <div class="row">
          <div class="col-md-6">
              {!! Form::label('first_page_adverting_image',__('page.first_page_adverting_image')) !!}
              {!! Form::file('first_page_adverting_image',['class'=>'form-control','onchange'=>'show_image(this)']) !!}
          </div><!--End col-md-6 -->
          <div class="col-md-6">
                <img src="{{asset('images/setting/'.$first_page_adverting_image->value)}}" style="max-height:150px" id="img_first_page_adverting_imagefirst_page_adverting_image">
          </div><!--End col-md-6 -->
      </div><!--End row-->
      <br>
      <div class="row">
          <div class="col-md-6">
              {!! Form::label('first_page_adverting_link',__('page.first_page_adverting_link')) !!}
              {!! Form::text('first_page_adverting_link',$first_page_adverting_link->value,['class'=>'form-control' ,'required']) !!}
          </div><!--End col-md-6 -->
          <div class="col-md-6"> </div><!--End col-md-6 -->
      </div><!--End row-->
      <br>

      <div class="row">
          <div class="col-md-6">
              {!! Form::label('employee_email',__('page.employee email')) !!}
              {!! Form::text('employee_email',$employee_email->value,['class'=>'form-control','required']) !!}
          </div><!--End col-md-6 -->
          <div class="col-md-6">
                <img src="" style="max-height:150px">
          </div><!--End col-md-6 -->
      </div><!--End row-->

      <br><br>
      {!! Form::submit(__('page.add'),['class'=>'btn btn-success ','style'=>'width:100%']) !!}

      <br><br>
    {!! Form::close() !!}

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->






  @else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission

@endsection


@section('script')
<script>

  $('#create_form').validate();
//----------------------------------------
function show_image(input)
{
       if (input.files && input.files[0])
       {
           var reader = new FileReader();

           reader.onload = function (e) {

               $('#img_first_page_adverting_imagefirst_page_adverting_image').prop('src', e.target.result);

           }
           reader.readAsDataURL(input.files[0]);
       }
}

</script>
@endsection
