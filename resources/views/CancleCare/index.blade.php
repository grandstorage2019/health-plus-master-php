

@extends('atlant.blank')

@section('content')
      @php($active='CancleCare')
      {{-- @permission('Doctor_index') --}}

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.CancleCare') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.CancleCare') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
     <br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
              <th> @lang('page.patient_id') </th>
              <th> @lang('page.Patient name') </th>
              <th> @lang('page.Patient email') </th>
              <th> @lang('page.care_sub_service_id') </th>
              <th> @lang('page.Care name') </th>
              <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($CancleCare as $Cancle)
                  <tr>
                        <td> {{$Cancle->patient_id??''}} </td>
                        <td> {{$Cancle->Patient->name??''}} </td>
                        <td> {{$Cancle->Patient->email??''}} </td>
                        <td> {{$Cancle->care_sub_service_id??''}} </td>
                        <td> {{$Cancle->CareSubService->name_en??''}} </td>
                        <td>
                            <button type="button" class="btn btn-danger btn-rounded"
                                    onclick="showDeleteMessage('{{url('CancleCare/delete/'.$Cancle->id)}}','')" >
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$CancleCare->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


{{-- @else
  <br><br>
  <div class="container">
      <h2> @lang('page.you dont have a permissions') </h2>
  </div>
@endpermission --}}

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('CancleCare')}}';
    </script>
    {{-- <script src="{{asset('js/Country.js')}}"> </script> --}}


@endsection
