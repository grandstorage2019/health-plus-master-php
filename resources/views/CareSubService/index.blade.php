@extends('atlant.blank')

@section('content')
      @php($active='CareSubServices')
      @permission('CareSubServices')
      <style media="screen">
        .inp_error
        {
          border: 1px solid red;
        }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Care Sub Services') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default" id="myVue">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Care Sub Services') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
      {!! Form::model($gov = new \App\Area,['url'=>'CareSubServices','id'=>'create_form','v-on:submit'=>'do_submit($event)','files'=>true]) !!}
           <div class="row">
              <div class="col-md-6 mydirection ">
                  {!! Form::label('care_id',__('page.care')) !!}
                  {!! Form::select('care_id',$cares,null,['class'=>'form-control select2', 'id'=>'ddl_care_id' ,'v-model'=>'ddl_care_id','v-on:change'=>'change_care()']) !!}
              </div>
              <div class="col-md-6 mydirection ">
                  {!! Form::label('service_id',__('page.care services')) !!}
                  {!! Form::select('service_id',[],null,['class'=>'form-control select2', 'id'=>'ddl_services_id','v-model'=>'ddl_services_id','required' ]) !!}
           </div><!--End row-->

           <hr>

          <div v-show="show_content">



            <div class="container brands-container">
               <div class="row">
                  <div class=" ">

                   <table class="table mydirection" id="table_Governorate">
                       <tr v-for= "(service,index) in sub_services_list">
                           <td>
                              <img :src="service.image" width="60px">
                           </td>
                           <td>
                                 {!! Form::label('images', __('page.image') ) !!}
                                 {!! Form::file('images[]',['class'=>'form-control','onchange'=>'show_image(this)'  ]) !!}
                           </td>
                           <td>
                                 {!! Form::label('name_en', __('page.name English') ) !!}
                                 {!! Form::text('name_en[]',null,['class'=>'form-control','required', 'v-model'=>"service.name_en" ]) !!} <!-- ':value'=>"govern.name_en" -->
                           </td>
                           <td>
                                 {!! Form::label('name_ar', __('page.name Arabic') ) !!}
                                 {!! Form::text('name_ar[]',null,['class'=>'form-control','required','v-model'=>"service.name_ar"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('quantity', __('page.quantity') ) !!}
                                 {!! Form::number('quantity[]',null,['class'=>'form-control','required','v-model'=>"service.quantity"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('unit_en', __('page.unit_en') ) !!}
                                 {!! Form::text('unit_en[]',null,['class'=>'form-control','required','v-model'=>"service.unit_en"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('unit_ar', __('page.unit_ar') ) !!}
                                 {!! Form::text('unit_ar[]',null,['class'=>'form-control','required','v-model'=>"service.unit_ar"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('price', __('page.price') ) !!}
                                 {!! Form::number('price[]',null,['class'=>'form-control','required','v-model'=>"service.price"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('currency', __('page.currency') ) !!}
                                 {!! Form::text('currency[]',null,['class'=>'form-control','required','v-model'=>"service.currency"]) !!}
                           </td>
                           <td>
                                 {!! Form::label('status', __('page.active') ) !!} <br>
                                 <label class="switch">
                                     <input type="checkbox" class="switch" value="1" name="status[]" :checked="service.status" />
                                     <span></span>
                                 </label>
                           </td>
                           <td> <br>
                              <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_row()" v-if="!service.id" >
                                  <i class="fa fa-trash-o"></i>
                              </button>
                           </td>
                           <input type="hidden" name="ids[]" :value="service.id">
                           <input type="hidden" name="count[]" value="c">
                       </tr>
                   </table>

                   <h2 class="mydirection div_add_brand">
                           <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_SubService_row()" >
                               <i class="fa fa-plus"></i>
                           </button>
                           <span> @lang('page.Care sub services') </span> <!-- class="mydir" -->
                   </h2>

                   {!! Form::submit(__('page.add'),['class'=>'btn btn-success']) !!}

          </div><!--End show_content-->


           </div><!--End container-->
         </div><!--End container-->
       </div><!--End show_content-->

      {!! Form::close() !!}
    </div><!--End panel-body-->
  </div><!--End my vue-->

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->

@else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission


@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

<script src="{{asset('js/Vue.js')}}"> </script>
    <script>
        var list_path = '{{asset('CareSubServices')}}';
        var get_services_by_id = '{{asset('CareSubServices/get_services')}}';
        var get_SubService_by_id = '{{asset('CareSubServices/get_CareSubService')}}';
    </script>

    <script src="{{asset('js/CareSubServices.js')}}"> </script>


@endsection
