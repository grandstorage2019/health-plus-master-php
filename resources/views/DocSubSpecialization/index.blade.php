@extends('atlant.blank')

@section('content')
      @php($active='DocSubSpecialization')
      @permission('DocSubSpecialization')
      <style media="screen">
        .select2
        {
          width: 100%;
        }
        ul li
        {
            list-style: none;
        }
      </style>

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Doctor sub Specialization') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Doctor sub Specialization') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      {{-- <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div> --}}

     <br><br>
     <div class="container">
         <ul>
           @foreach ($get_DocSpecialization as $specialization)
               <li>
                   <h3>
                       <i class="fa fa-plus-square"></i>
                       {{(\Session::get('lang') == 'ar')?$specialization->name_ar:$specialization->name_en}}
                   </h3>
                   <ul>
                       @foreach ($specialization->subSpecialization as $subSpecialization)
                         <li>
                             <div class="row">

                                <div class="col-md-4">
                                    <p>
                                        <i class="fa fa-minus"></i>
                                        {{(\Session::get('lang') == 'ar')?$subSpecialization->text_ar:$subSpecialization->text_en}}
                                    </p>
                                </div><!--End col-md-4-->

                                <div class="col-md-8">
                                    <button onclick="edit_model('{{$subSpecialization->id}}','{{$subSpecialization->text_ar}}','{{$subSpecialization->text_en}}','{{$subSpecialization->specialization_id}}')" class="btn btn-warning btn-rounded" >
                                        <i class="fa fa-pencil"></i>
                                    </button>
                                    @if ($subSpecialization->status)
                                      <a href="{{url('DocSubSpecialization/showORhide/'.$subSpecialization->id)}}" class="btn btn-success btn-rounded"> <i class="fa fa-eye"></i> </a>
                                    @else
                                      <a href="{{url('DocSubSpecialization/showORhide/'.$subSpecialization->id)}}" class="btn btn-danger btn-rounded"> <i class="fa fa-eye-slash"></i> </a>
                                    @endif
                                </div><!--End col-md-8-->

                             </div><!--End row-->
                             <br>
                         </li>
                       @endforeach
                   </ul>
                   <hr>
               </li>

           @endforeach
         </ul>
     </div><!--End container-->



      {{-- <table class="table mydirection">
          <thead>
            <th> @lang('page.name Arabic') </th>
            <th> @lang('page.name English') </th>
            <th> @lang('page.specialization') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($DocSubSpecialization as $specialization)
                  <tr>
                        <td> {{$specialization->text_ar}} </td>
                        <td> {{$specialization->text_en}} </td>
                        <td> {{(\Session::get('lang') == 'ar')?$specialization->get_specialization->name_ar:$specialization->get_specialization->name_en}} </td>
                        <td>
                              <button onclick="edit_model('{{$specialization->id}}','{{$specialization->text_ar}}','{{$specialization->text_en}}','{{$specialization->specialization_id}}')" class="btn btn-warning btn-rounded" >
                                  <i class="fa fa-pencil"></i>
                              </button>
                              @if ($specialization->status)
                                <a href="{{url('DocSubSpecialization/showORhide/'.$specialization->id)}}" class="btn btn-success btn-rounded"> <i class="fa fa-eye"></i> </a>
                              @else
                                <a href="{{url('DocSubSpecialization/showORhide/'.$specialization->id)}}" class="btn btn-danger btn-rounded"> <i class="fa fa-eye-slash"></i> </a>
                              @endif
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$DocSubSpecialization->links()}} </div>
      </div> --}}

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




{{-- ------------------- create ------------------------ --}}

    @component('componets.modal')
        @slot('id')
          create_model
        @endslot
        @slot('header')
          @lang('page.create new')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\DocSubSpecialization,['url'=>'DocSubSpecialization','class'=>"mydirection",'id'=>'create_form']) !!}
        @endslot
        @slot('body')

              {!! Form::label('specialization_id', __('page.specialization') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::select('specialization_id',$DocSpecialization,null,['class'=>'form-control select2','required']) !!}
              </div>

              {!! Form::label('text_en', __('page.name English') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('text_en',null,['class'=>'form-control','required']) !!}
              </div>

              {!! Form::label('text_ar', __('page.name Arabic') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                {!! Form::text('text_ar',null,['class'=>'form-control','required']) !!}
              </div>

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.Add'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- edit ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          edit_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\DocSubSpecialization,['url'=>'DocSubSpecialization/update','class'=>"mydirection",'id'=>'edit_model']) !!}
        @endslot
        @slot('body')

              {!! Form::label('specialization_id', __('page.specialization') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::select('specialization_id',$DocSpecialization,null,['class'=>'form-control select2','id'=>'edit_specialization','required']) !!}
              </div>

              {!! Form::label('text_en', __('page.name English') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                {!! Form::text('text_en',null,['class'=>'form-control','id'=>'edit_name_en','required']) !!}
              </div>

              {!! Form::label('text_ar', __('page.name Arabic') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('text_ar',null,['class'=>'form-control','id'=>'edit_name_ar','required']) !!}
              </div>
              {!! Form::hidden('id',null,['id'=>'edit_id']) !!}

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.update'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent



  @else
    <br><br>
    <div class="container">
        <h2> @lang('page.you dont have a permissions') </h2>
    </div>
  @endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('DocSubSpecialization')}}';
    </script>
    <script src="{{asset('js/DocSubSpecialization.js')}}"> </script>


@endsection
