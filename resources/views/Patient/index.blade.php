@extends('atlant.blank')

@section('content')
      @php($active='Patient')
      @permission('Patient')

      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Patient') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Patient') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">


      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th> @lang('page.id') </th>
            <th> @lang('page.name') </th>
            <th> @lang('page.os') </th>
            <th> @lang('page.email') </th>
            <th> @lang('page.gender') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($patients as $patient)
                  <tr>
                        <td width="3%"> {{$patient->id}} </td>
                        <td>
                          @if ($patient->image)
                              <img src="{{asset('images/patient_profile/'.$patient->image)}}" class="img-thumbnail mydir" width="60px" style="max-height:80px;margin:1px 15px">
                          @else
                              <img src="{{asset('images/patient_profile/patient_image.jpg')}}" class="img-thumbnail mydir" width="60px" style="max-height:80px;margin:1px 15px">
                          @endif
                           {{$patient->name}}
                         </td>
                        <td>
                          @if ($patient->os == 'android')
                              <span class="badge badge-danger">{{$patient->os}}</span>
                          @else
                              <span class="badge badge-info">{{$patient->os}}</span>
                          @endif
                        </td>
                        <td> {{$patient->email}} </td>
                        <td> {{$patient->gender}} </td>
                        <td>
                              {{-- <button onclick="edit_model('{{$patient->id}}','{{$patient->name_ar}}','{{$patient->name_en}}')" class="btn btn-warning btn-rounded" >
                                  <i class="fa fa-pencil"></i>
                              </button> --}}
                                <a href="{{url('Patient/'.$patient->id)}}" class="btn btn-primary btn-rounded"> <i class="fa fa-search"></i> </a>
                                @if ($patient->status)
                                  <a href="{{url('Patient/showORhide/'.$patient->id)}}" class="btn btn-success btn-rounded"> <i class="fa fa-eye"></i> </a>
                                @else
                                  <a href="{{url('Patient/showORhide/'.$patient->id)}}" class="btn btn-danger btn-rounded"> <i class="fa fa-eye-slash"></i> </a>
                                @endif
                                {{-- <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('Doctor/delete/'.$patient->id)}}','{{$patient->name}}')" >
                                    <i class="glyphicon glyphicon-trash"></i>
                                </button> --}}
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$patients->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('Patient')}}';
    </script>
    {{-- <script src="{{asset('js/Country.js')}}"> </script> --}}


@endsection
