@extends('atlant.blank')

@section('content')
      @php($active='Patient')
      @permission('Patient')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

          <!-- START BREADCRUMB -->
          <ul class="breadcrumb">
              <li class="active"> @lang('page.show') </li>
              <li><a href="{{url('Patient')}}"> @lang('page.Patients') </a></li>
          </ul>
          <!-- END BREADCRUMB -->

<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Show Patient') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      @if ($patient->image)
          <img src="{{asset('images/patient_profile/'.$patient->image)}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px">
      @else
          <img src="{{asset('images/patient_profile/patient_image.jpg')}}" class="img-thumbnail mydir" width="150px" style="max-height:200px;margin:1px 15px">
      @endif

      <table class="table mydirection">

          <tr>
            <td> @lang('page.name') </td>
            <td> {{$patient->name}} </td>
          </tr>
          <tr>
            <td> @lang('page.os') </td>
            <td>
                  @if ($patient->os == 'android')
                      <span class="badge badge-danger">{{$patient->os}}</span>
                  @else
                      <span class="badge badge-info">{{$patient->os}}</span>
                  @endif
             </td>
          </tr>
          <tr>
            <td> @lang('page.email') </td>
            <td> {{$patient->email}} </td>
          </tr>
          <tr>
            <td> @lang('page.username') </td>
            <td> {{$patient->username}} </td>
          </tr>
          <tr>
            <td> @lang('page.gender') </td>
            <td> {{$patient->gender}} </td>
          </tr>
          <tr>
            <td> @lang('page.phone') </td>
            <td> {{$patient->phone}} </td>
          </tr>
          <tr>
            <td> @lang('page.birth day') </td>
            <td> {{$patient->birth_day}} </td>
          </tr>
          <tr>
            <td> @lang('page.phone') </td>
            <td> {{$patient->phone}} </td>
          </tr>
          <tr>
            <td> @lang('page.model') </td>
            <td> {{$patient->model}} </td>
          </tr>
          <tr>
            <td> @lang('page.country') </td>
            <td>
                @isset($patient->get_country)
                    @if (\Session::get('lang') == 'ar')
                        {{$patient->get_country->name_ar}}
                    @else
                        {{$patient->get_country->name_en}}
                    @endif
                @endisset
            </td>
          </tr>

      </table>
      <hr>
      <h2> @lang('page.doctor resirvation (last 15) ') </h2>

    @if ($patient->get_reservationed_doctor->isEmpty())
        @lang('page.No doctor resirvation')
    @else
      <table class="table mydir">
        <thead>
            <th> @lang('page.name') </th>
            <th> @lang('page.specialization') </th>
            <th> @lang('page.type') </th>
            <th> @lang('page.date') </th>
            <th> @lang('page.time') </th>
        </thead>
          @foreach ($patient->get_reservationed_doctor_limit as $reservationed)
              <tr>
                  @if (\Session::get('lang') == 'ar' )
                    <td>  {{$reservationed->fname_ar}} {{$reservationed->lname_ar}}</td>
                    <td>  {{$reservationed->get_specialization_mini->name_ar}} </td>
                    <td>  {{($reservationed->pivot->type == 'specific time' )? ' وقت محدد ' : " الاسبقية " }} </td>
                    <td>  {{$reservationed->pivot->date}}   </td>
                    <td>  {{date("H:i",strtotime($reservationed->pivot->time))}}   </td>
                  @else
                    <td>  {{$reservationed->fname_en}} {{$reservationed->fname_en}}</td>
                    <td>  {{$reservationed->get_specialization_mini->name_en}} </td>
                    <td>  {{$reservationed->pivot->type}} </td>
                    <td>  {{$reservationed->pivot->date}}  </td>
                    <td>  {{date("H:i",strtotime($reservationed->pivot->time))}}  </td>
                  @endif
              </tr>
          @endforeach
      </table>
    @endif


    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->


@else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission

@endsection


@section('script')


@endsection
