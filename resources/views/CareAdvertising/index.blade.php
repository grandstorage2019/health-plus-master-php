@extends('atlant.blank')

@section('content')
      @php($active='CareAdvertising')
      @permission('CareAdvertising')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Care Advertising') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Care Advertising') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th> @lang('page.image') </th>
            <th> @lang('page.title Arabic') </th>
            <th> @lang('page.title English') </th>
            <th> @lang('page.link') </th>
            <th> @lang('page.is promited') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($CareAdvertising as $advertising)
                  <tr>
                        <td> <img src="{{asset('images/care_adverting/'.$advertising->image)}}" width="80px" > </td>
                        <td> {{$advertising->title_en}} </td>
                        <td> {{$advertising->title_ar}} </td>
                        <td> <a href="{{$advertising->link}}" target="_blank">{{$advertising->link}}</a> </td>
                        <td>
                          @if ($advertising->is_promoted)
                              <i class="fa fa-check"></i>
                          @else
                              <i class="fa fa-times"></i>
                          @endif
                        </td>
                        <td>
                              <button onclick="edit_model('{{$advertising->id}}','{{$advertising->title_en}}','{{$advertising->title_ar}}','{{$advertising->link}}','{{$advertising->is_promoted}}','{{$advertising->image}}')" class="btn btn-warning btn-rounded" >
                                  <i class="fa fa-pencil"></i>
                              </button>
                              <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('CareAdvertising/delete/'.$advertising->id)}}','{{$advertising->title_en}}')" >
                                  <i class="glyphicon glyphicon-trash"></i>
                              </button>
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$CareAdvertising->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




{{-- ------------------- create ------------------------ --}}

    @component('componets.modal')
        @slot('id')
          create_model
        @endslot
        @slot('header')
          @lang('page.create new')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\CareAdvertising,['url'=>'CareAdvertising','class'=>"mydirection",'id'=>'create_form','files'=>true]) !!}
        @endslot
        @slot('body')

              <div class="form-group">
                {!! Form::label('image','الصورة:') !!}
                {!! Form::file('image',['class'=>'form-control','onchange'=>"change_image(this)",'required']) !!}
              </div>
              <img src="" id="create_img_temp" width="200px">
              <br>

              {!! Form::label('title_en', __('page.title English') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('title_en',null,['class'=>'form-control','required']) !!}
              </div>

              {!! Form::label('title_ar', __('page.title Arabic') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                {!! Form::text('title_ar',null,['class'=>'form-control','required']) !!}
              </div>

              {!! Form::label('link', __('page.link') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-link"></span></span>
                {!! Form::text('link',null,['class'=>'form-control','required']) !!}
              </div>
              <br>
              <div class="form-group">
                    <div class="row">
                          <div class="col-md-4">
                              <label for=""> @lang('page.is promited') : </label>
                          </div>
                          <div class="col-md-8">
                                <label class="switch">
                                    <input type="checkbox" name="is_promoted"   />
                                    <span></span>
                                </label>
                          </div>
                    </div><!--End row-->
                </div>

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.Add'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- edit ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          edit_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\CareAdvertising,['url'=>'CareAdvertising/update','class'=>"mydirection",'id'=>'edit_model','files'=>true]) !!}
        @endslot
        @slot('body')

              {!! Form::hidden('id',null,['id'=>'edit_id']) !!}
              <div class="form-group">
                {!! Form::label('image','الصورة:') !!}
                {!! Form::file('image',['class'=>'form-control','onchange'=>"change_image(this)"]) !!}
              </div>
              <img src="" id="edit_img_temp" width="200px">
              <br>

              {!! Form::label('title_en', __('page.title English') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('title_en',null,['class'=>'form-control','id'=>'edit_title_en','required']) !!}
              </div>

              {!! Form::label('title_ar', __('page.title Arabic') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                {!! Form::text('title_ar',null,['class'=>'form-control','id'=>'edit_title_ar','required']) !!}
              </div>

              {!! Form::label('link', __('page.link') ) !!}
              <div class="input-group">
                <span class="input-group-addon"><span class="fa fa-link"></span></span>
                {!! Form::text('link',null,['class'=>'form-control','id'=>'edit_link','required']) !!}
              </div>
              <br>
              <div class="form-group">
                    <div class="row">
                          <div class="col-md-4">
                              <label for=""> @lang('page.is promited') : </label>
                          </div>
                          <div class="col-md-8">
                                <label class="switch">
                                    <input type="checkbox" name="is_promoted" id="edit_is_promited"  />
                                    <span></span>
                                </label>
                          </div>
                    </div><!--End row-->
                </div>

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.update'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

  @else
   <br><br>
   <div class="container">
       <h2> @lang('page.you dont have a permissions') </h2>
   </div>
 @endpermission


@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('CareAdvertising')}}';
        var Advertising_image_path = '{{asset('images/care_adverting')}}';
    </script>
    <script src="{{asset('js/PatientAdvertising.js')}}"> </script>


@endsection
