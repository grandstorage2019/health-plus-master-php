@extends('atlant.blank')

@section('content')

      @if ($type == 'health_plus')
            @php($active='health_Reservation' )
      @elseif ($type == 'health_care')
            @php($active='care_Reservation' )
      @endif

      {{-- @permission('Plan') --}}
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.'.$type.' reservation') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.'.$type.' reservation') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      {{-- <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div> --}}

      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>

      @if ($type == 'health_plus')

        <table class="table mydirection">
            <thead>
              <th> @lang('page.reservation_id') </th>
              <th> @lang('page.doctor_id') </th>
              <th> @lang('page.doctor_name') </th>
              <th> @lang('page.patient_id') </th>
              <th> @lang('page.patient_name') </th>
              <th> @lang('page.time') </th>
              @permission('healthReservations_cancle')
              <th> @lang('page.more') </th>
              @endpermission
            </thead>
            <tbody>
              @foreach ($Reservations as $Reservation)
                    <tr>
                          <td> {{$Reservation->reservation_id}} </td>
                          <td> {{$Reservation->doctor_id}} </td>
                          <td> {{$Reservation->doctor_name}} </td>
                          <td> {{$Reservation->patient_id}} </td>
                          <td> {{$Reservation->patient_name}} </td>
                          <td>
                              {{$Reservation->date}} {{$Reservation->time}} <br>
                              [{{\Carbon\Carbon::parse($Reservation->date.' '.$Reservation->time)->diffForHumans()}}]
                          </td>
                        @permission('healthReservations_cancle')

                        <td>
                              <a href="{{url('Doctor/reservations/cancle/'.$Reservation->reservation_id)}}" class="btn btn-danger btn-rounded" data-toggle="tooltip" title="@lang('page.cancle the reservation')" >
                                  <i class="fa fa-times"></i>
                              </a>
                          </td>
                        @endpermission
                    </tr>
                @endforeach
            </tbody>

        </table>

      @elseif ($type == 'health_care')

        <table class="table mydirection">
            <thead>
              <th> @lang('page.reservation_id') </th>
              <th> @lang('page.patient_id') </th>
              <th> @lang('page.patient_name') </th>
              <th> @lang('page.patient_phone') </th>
              <th> @lang('page.patient_email') </th>
              <th> @lang('page.care_resvation_id') </th>
              <th> @lang('page.care_sub_services_id') </th>
              <th> @lang('page.care') </th>
              <th> @lang('page.more') </th>
            </thead>
            <tbody>
              @foreach ($Reservations as $Reservation)
                    <tr>
                          <td> {{$Reservation->care_resvation_id}} </td>
                          <td> {{$Reservation->patient_id}} </td>
                          <td> {{$Reservation->patient_name}} </td>
                          <td> {{$Reservation->patient_phone}} </td>
                          <td> {{$Reservation->patient_email}} </td>
                          <td> {{$Reservation->care_resvation_id}} </td>
                          <td> {{$Reservation->care_sub_services_id}} </td>
                          <td> {{$Reservation->care_services_name}} -> {{$Reservation->care_sub_services_name}} </td>

                          <td>
                              <a href="{{url('Reservation/care_cancle/'.$Reservation->care_resvation_id)}}" class="btn btn-danger btn-rounded" data-toggle="tooltip" title="@lang('page.cancle the reservation')" >
                                  <i class="fa fa-times"></i>
                              </a>
                          </td>
                    </tr>
                @endforeach
            </tbody>

        </table>
      @endif



      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$Reservations->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->



  {{-- @else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission --}}

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
          @if ($type == 'health_plus')
              var list_path = '{{asset('Reservation/health')}}';
          @elseif ($type == 'health_care')
              var list_path = '{{asset('Reservation/care')}}';
          @endif
    </script>
    <script src="{{asset('js/Plan.js')}}"> </script>


@endsection
