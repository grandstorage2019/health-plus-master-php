@extends('atlant.blank')

@section('content')
      @php($active='NotificationPatient')
      @permission('NotificationPatient')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Notification Patient') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Notification Care') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th> @lang('page.name') </th>
            <th> @lang('page.details') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($Notifications as $Notification)
                  <tr>
                        <td> <img src="{{asset('images/notification_care/'.$Notification->image)}}" width="200px" class="thumbnail">   </td>
                        <td> {{$Notification->message}} </td>
                        <td>
                            <a  href="{{url('NotificationPatient/pushAgin/'.$Notification->id)}}" class="btn btn-success btn-rounded" data-toggle="tooltip" title="@lang('page.send notification agin')" >
                                <i class="fa fa-bullhorn"></i>
                            </a>
                            <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('NotificationPatient/delete/'.$Notification->id)}}','{{$Notification->message}}')" >
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$Notifications->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




{{-- ------------------- create ------------------------ --}}

    @component('componets.modal')
        @slot('id')
          create_model
        @endslot
        @slot('header')
          @lang('page.create new')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\NotificationPatient,['url'=>'NotificationPatient','class'=>"mydirection",'id'=>'create_form','files'=>true]) !!}
        @endslot
        @slot('body')

          <div class="form-group">
            {!! Form::label('image',__('page.image')) !!}
            {!! Form::file('image',['class'=>'form-control','onchange'=>"change_image(this)",'required']) !!}
          </div>
          <img src="" id="create_img_temp" width="200px">
          <br>

          {!! Form::label('message', __('page.message') ) !!}
              {!! Form::textarea('message',null,['class'=>'form-control','required']) !!}


        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.Add'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

  @else
    <br><br>
    <div class="container">
        <h2> @lang('page.you dont have a permissions') </h2>
    </div>
  @endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('NotificationPatient')}}';
        var notification_image_path = '{{asset('images/notification_care')}}';
    </script>
    <script src="{{asset('js/NotificationCare.js')}}"> </script>
    {{-- <script src="{{asset('js/Plan.js')}}"> </script> --}}


@endsection
