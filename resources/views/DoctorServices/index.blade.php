@extends('atlant.blank')

@section('content')
      @php($active='DoctorServices')
      <style media="screen">
        .inp_error
        {
          border: 1px solid red;
        }
      </style>
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Doctor Services') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Doctor Services') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body" id="myVue">

      <div class="row">
         <div class="col-md-10 col-md-offset-2">
{!! Form::model($gov = new \App\Area,['url'=>'DoctorServices','id'=>'create_form','v-on:submit'=>'do_submit($event)' ]) !!}

      <table class="table mydirection" id="my_table">
          <tr v-for= "(list,index) in list_items">
              <td>
                    {!! Form::label('name_en', __('page.name English') ) !!}
                    {!! Form::text('name_en[]',null,['class'=>'form-control','required', 'v-model'=>"list.name_en" ]) !!} <!-- ':value'=>"govern.name_en" -->
              </td>
              <td>
                    {!! Form::label('name_ar', __('page.name Arabic') ) !!}
                    {!! Form::text('name_ar[]',null,['class'=>'form-control','required','v-model'=>"list.name_ar"]) !!}
              </td>
              @permission('DoctorServices_activate')

              <td>
                    {!! Form::label('status', __('page.active') ) !!} <br>
                    <label class="switch">
                        <input type="checkbox" class="switch" value="1" name="status[]" :checked="list.status" />
                        <span></span>
                    </label>
              </td>
              @endpermission
              <td> <br>
                 <button type="button" class="btn btn-danger btn-condensed del" v-on:click="del_row()" v-if="!list.id" >
                     <i class="fa fa-trash-o"></i>
                 </button>
              </td>
              <input type="hidden" name="ids[]" v-if="list.id" :value="list.id">
              <input type="hidden" name="count[]" value="c">
          </tr>
      </table>
             @permission('DoctorServices_create')
      <h2 class="mydirection div_add_brand">
              <button type="button" class="btn btn-primary btn-condensed" v-on:click="add_row()" >
                  <i class="fa fa-plus"></i>
              </button>
              <span> @lang('page.Add Doctor Services') </span> <!-- class="mydir" -->
      </h2>
      {!! Form::submit(__('page.add'),['class'=>'btn btn-success']) !!}
            @endpermission
    {!! Form::close() !!}
      </div><!--End col-md-10 col-md-offset-2-->
    </div><!--End row-->

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->



   {{--<br><br>
   <div class="container">
       <h2> @lang('page.you dont have a permissions') </h2>
   </div>--}}

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}
<script src="{{asset('js/Vue.js')}}"> </script>
    <script>
        var get_list = JSON.parse(`{!!$DocServices!!}`);
    </script>
    <script src="{{asset('js/DoctorServices.js')}}"> </script>


@endsection
