
@extends('atlant.blank')

@section('content')
      @php($active='Role')
      <style>
        tr td:first-child
        {
            font-weight: bold;
        }
      </style>
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li class="active"> @lang('page.create') </li>
            <li><a href="{{url('Role')}}"> @lang('page.Role') </a></li>
        </ul>
        <!-- END BREADCRUMB -->
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Role') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">
      {!! Form::model($Role= new \App\Role,['url'=>'Role', 'id'=>'create_form' ]) !!}

      <div class="col-md-6">
          {!! Form::label('name',__('page.name')) !!}
          {!! Form::text('name',null,['class'=>'form-control','required']) !!}

            {!! Form::label('comment',__('page.comment')) !!}
          {!! Form::textarea('comment',null,['class'=>'form-control','rows'=>'3','required']) !!}
          <br>
      </div><!--End col-md-6-->

      <table class="table table-bordered mydirection">

          <thead>
              <th width="15%"> @lang('page.place') </th>
              <th> @lang('page.permissions')
                <span class="pull-atherWay">
                    <button type="button" class="btn btn-success btn-condensed" id="btn_all"> <i class="fa fa-check-square-o"></i> </button>
                </span>
              </th>
          </thead>
          <tbody>

          <tr>  <!-- ============ DashBoard ===============  -->
            <td> @lang('page.Dashboard') </td>
            <td>
                {!! Form::label('DashBoard',__('page.Dashboard')) !!} <br>
                <label class="switch">
                    <input type="checkbox" class="switch" value="1" name="permissions[]"  />
                    <span></span>
                </label>
            </td>
          </tr>


          <tr>  <!-- ============ location ===============  -->
              <td> @lang('page.Location') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('DashBoard',__('page.Country')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="2" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('Governorate',__('page.Governorate')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="3" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-xs-4">
                          {!! Form::label('Area',__('page.Area')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="4" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Specialization ===============  -->
              <td> @lang('page.Specialization') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('DashBoard',__('page.Doctor Specialization')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="5" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('DocSubSpecialization',__('page.Doctor sub Specialization')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="6" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-xs-4">
                          {!! Form::label('CareSpecialization',__('page.Care Specialization')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="7" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Advertising ===============  -->
              <td> @lang('page.Advertising') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('PatientAdvertising',__('page.Patient Advertising')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="8" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('CareAdvertising',__('page.Care Advertising')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="9" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ InsuranceCompany ===============  -->
              <td> @lang('page.Insurance Company') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('InsuranceCompany',__('page.Insurance Company')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="10" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Plan ===============  -->
              <td> @lang('page.Plan') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('Plan',__('page.Plan')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="11" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Patient ===============  -->
              <td> @lang('page.Patient') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('Plan',__('page.Patient')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="12" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Patient ===============  -->
              <td> @lang('page.Doctor') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('Doctor_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="13" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Doctor_create',__('page.create')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="14" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Doctor_view',__('page.view')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="15" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Doctor_edit',__('page.edit')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="16" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Doctor_delete',__('page.delete')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="28" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>


          <tr>  <!-- ============ Health Reservation ===============  -->
              <td> @lang('page.health_Reservation') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('health_Reservation_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="30" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>

                      <div class="col-md-2">
                          {!! Form::label('health_Reservation_delete',__('page.delete')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="31" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Cancled Patient ===============  -->
              <td> @lang('page.CanclePatient') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('CanclePatient_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="34" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>

                      <div class="col-md-2">
                          {!! Form::label('CanclePatient_delete',__('page.cancle')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="35" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>


          <tr>  <!-- ============ Care Reservation ===============  -->
              <td> @lang('page.care_Reservation') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('care_Reservation_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="32" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>

                      <div class="col-md-2">
                          {!! Form::label('care_Reservation_delete',__('page.delete')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="33" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>


          <tr>  <!-- ============ Cancle Care Reservation ===============  -->
              <td> @lang('page.CancleCare') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('CancleCare_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="36" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>

                      <div class="col-md-2">
                          {!! Form::label('CancleCare_delete',__('page.cancle')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="37" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>


          <tr>  <!-- ============ Doctor Service ===============  -->
              <td> @lang('page.DoctorServices') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('DoctorServices_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="38" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>

                      <div class="col-md-2">
                          {!! Form::label('DoctorServices_create',__('page.create')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="39" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('DoctorServices_activate',__('page.active')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="40" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>


          <tr>  <!-- ============ Care ===============  -->
              <td> @lang('page.Care') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-2">
                          {!! Form::label('Care_index',__('page.main page')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="17" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Care_index',__('page.create')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="19" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Care_view',__('page.view')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="18" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Care_edit',__('page.edit')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="16" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-2">
                          {!! Form::label('Care_delete',__('page.delete')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="29" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Care ===============  -->
              <td> @lang('page.Care') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('Care_index',__('page.Care Sub Services')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="21" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('CareOffer',__('page.Care Offer')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="22" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

          <tr>  <!-- ============ Notification ===============  -->
              <td> @lang('page.Notification') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('NotificationCare',__('page.Notification Care')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="23" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('NotificationPatient',__('page.Notification Patient')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="24" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>
          <tr>  <!-- ============ More ===============  -->
              <td> @lang('page.more') </td>
              <td>
                  <div class="row ">
                      <div class="col-md-4">
                          {!! Form::label('AboutUs',__('page.AboutUs')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="25" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('ContactUs',__('page.Contact Us')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="26" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                      <div class="col-md-4">
                          {!! Form::label('Setting',__('page.Setting')) !!} <br>
                          <label class="switch">
                              <input type="checkbox" class="switch" value="27" name="permissions[]"  />
                              <span></span>
                          </label>
                      </div>
                  </div><!--End row-->
              </td>
          </tr>

        </tbody>
      </table>

      {!! Form::submit(__('page.add'),['class'=>'btn btn-success ','style'=>'width:100%']) !!}

      <br><br>
    {!! Form::close() !!}

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
          $('#create_form').validate();
          var checked_status = 'not_checkedAll';

          $('#btn_all').click(function(event) {
              if (checked_status == 'not_checkedAll')
              {
                  $('input[type=checkbox]').prop('checked',true);
                  $(this).removeClass('btn-success');
                  $(this).addClass('btn-danger');
                  checked_status = 'checkedAll';
              }
              else if (checked_status == 'checkedAll')
              {
                  $('input[type=checkbox]').prop('checked',false);
                  $(this).removeClass('btn-danger');
                  $(this).addClass('btn-success');
                  checked_status = 'not_checkedAll';
              }
          });
    </script>
    {{-- <script src="{{asset('js/Country.js')}}"> </script> --}}


@endsection
