@extends('atlant.blank')

@section('content')
      @php($active='Plan')
      @permission('Plan')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.Plan') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create Plan') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th> @lang('page.name English') </th>
            <th> @lang('page.details English') </th>
            <th> @lang('page.name Arabic') </th>
            <th> @lang('page.details Arabic') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($plans as $plan)
                  <tr>
                        <td> {{$plan->name_en}} </td>
                        <td> {{$plan->details_en}} </td>
                        <td> {{$plan->name_ar}} </td>
                        <td> {{$plan->details_ar}} </td>

                        <td>
                            <button onclick="show_model('{{$plan->name_en}}','{{$plan->name_ar}}','{{$plan->details_ar}}','{{$plan->details_en}}')" class="btn btn-primary btn-rounded" >
                                <i class="fa fa-search"></i>
                            </button>
                            <button onclick="edit_model('{{$plan->id}}','{{$plan->name_en}}','{{$plan->name_ar}}','{{$plan->details_ar}}','{{$plan->details_en}}')" class="btn btn-warning btn-rounded" >
                                <i class="fa fa-pencil"></i>
                            </button>
                            @if ($plan->status)
                              <a href="{{url('Plan/showORhide/'.$plan->id)}}" class="btn btn-success btn-rounded"> <i class="fa fa-eye"></i> </a>
                            @else
                              <a href="{{url('Plan/showORhide/'.$plan->id)}}" class="btn btn-danger btn-rounded"> <i class="fa fa-eye-slash"></i> </a>
                            @endif
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$plans->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




{{-- ------------------- create ------------------------ --}}

    @component('componets.modal')
        @slot('id')
          create_model
        @endslot
        @slot('header')
          @lang('page.create new')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\Plan,['url'=>'Plan','class'=>"mydirection",'id'=>'create_form']) !!}
        @endslot
        @slot('body')

          {!! Form::label('name_ar', __('page.name Arabic') ) !!}
          <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
              {!! Form::text('name_ar',null,['class'=>'form-control','required']) !!}
          </div>

          {!! Form::label('name_en', __('page.name English') ) !!}
          <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
              {!! Form::text('name_en',null,['class'=>'form-control','required']) !!}
          </div>

          {!! Form::label('details_ar', __('page.details Arabic') ) !!}
          {!! Form::textarea('details_ar',null,['class'=>'form-control','required','rows'=>'7']) !!}

          {!! Form::label('details_en', __('page.details English') ) !!}
          {!! Form::textarea('details_en',null,['class'=>'form-control','required','rows'=>'7']) !!}

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.Add'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- edit ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          edit_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\Plan,['url'=>'Plan/update','class'=>"mydirection",'id'=>'edit_form']) !!}
        @endslot
        @slot('body')
              {!! Form::hidden('id',null,['id'=>'edit_id']) !!}

              {!! Form::label('name_ar', __('page.name Arabic') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('name_ar',null,['class'=>'form-control','id'=>'edit_name_ar','required']) !!}
              </div>

              {!! Form::label('name_en', __('page.name English') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('name_en',null,['class'=>'form-control','id'=>'edit_name_en','required']) !!}
              </div>

              {!! Form::label('details_ar', __('page.details Arabic') ) !!}
              {!! Form::textarea('details_ar',null,['class'=>'form-control','id'=>'edit_details_ar','required','rows'=>'7']) !!}

              {!! Form::label('details_en', __('page.details English') ) !!}
              {!! Form::textarea('details_en',null,['class'=>'form-control','id'=>'edit_details_en','required','rows'=>'7']) !!}

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.update'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- show ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          show_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('body')
               <table class="table">
                   <tr>
                       <th> @lang('page.name Arabic') </th>
                       <td id="show_name_ar">  </td>
                   </tr>
                   <tr>
                       <th> @lang('page.name English') </th>
                       <td id="show_name_en">  </td>
                   </tr>
                   <tr>
                       <th> @lang('page.details Arabic') </th>
                       <td id="show_details_ar">  </td>
                   </tr>
                   <tr>
                       <th> @lang('page.details English') </th>
                       <td id="show_details_en">  </td>
                   </tr>
               </table>
        @endslot

    @endcomponent


  @else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('Plan')}}';
    </script>
    <script src="{{asset('js/Plan.js')}}"> </script>


@endsection
