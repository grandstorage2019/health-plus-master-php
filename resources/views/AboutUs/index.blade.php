@extends('atlant.blank')

@section('content')
      @php($active='AboutUs')
      @permission('AboutUs')
      <!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">
  <div class="row">
      <div class="col-md-12">

<div class="page-title">
    <h2> @lang('page.About Us') </h2>
</div>
<!-- START PANEL WITH STATIC CONTROLS -->
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"> @lang('page.Create About Us') </h3>
        <ul class="panel-controls">
            <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                    <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span> Refresh</a></li>
                </ul>
            </li>
            <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
        </ul>
    </div>
    <div class="panel-body">

      <button class="btn btn-primary btn-rounded" id="btn_create"> @lang('page.add new')<i class="fa fa-plus mydir"></i> </button>
      <br><br>
      <div class="col-md-6 mydirection">
            <input type="text" class="form-control mydirection" id="inp_search" value="{{$val??''}}" placeholder=" @lang('page.search') " dir="rtl">
      </div>


      <table class="table mydirection">
          <thead>
            <th> @lang('page.title English') </th>
            <th> @lang('page.title Arabic') </th>
            <th> @lang('page.details English') </th>
            <th> @lang('page.details Arabic') </th>
            <th> @lang('page.more') </th>
          </thead>
          <tbody>
            @foreach ($AboutUs as $About)
                  <tr>
                        <td> {{$About->title_en}} </td>
                        <td> {{$About->title_ar}} </td>
                        <td> {{$About->details_en}} </td>
                        <td> {{$About->details_ar}} </td>
                        <td width="10%">
                            <button onclick="edit_model(`{{$About->id}}`,`{{$About->title_en}}`,`{{$About->title_ar}}`,`{{$About->details_ar}}`,`{{$About->details_en}}`)" class="btn btn-warning btn-rounded" >
                                <i class="fa fa-pencil"></i>
                            </button>
                            <button type="button" class="btn btn-danger btn-rounded" onclick="showDeleteMessage('{{url('AboutUs/delete/'.$About->id)}}',`{{$About->title_en}}`)" >
                                <i class="glyphicon glyphicon-trash"></i>
                            </button>
                        </td>
                  </tr>
              @endforeach
          </tbody>

      </table>

      <div class="row">
            <div class="col-md-8 col-md-offset-5"> {{$AboutUs->links()}} </div>
      </div>

    </div><!--End panel-body-->
   </div>

 </div><!--End col-md-12-->
</div><!--End row -->
</div><!--End page-content-wrap-->
<!-- END PANEL WITH STATIC CONTROLS -->




{{-- ------------------- create ------------------------ --}}

    @component('componets.modal')
        @slot('id')
          create_model
        @endslot
        @slot('header')
          @lang('page.create new')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\AboutUs,['url'=>'AboutUs','class'=>"mydirection",'id'=>'create_form']) !!}
        @endslot
        @slot('body')

          {!! Form::label('title_en', __('page.title English') ) !!}
          <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
              {!! Form::text('title_en',null,['class'=>'form-control','required']) !!}
          </div>

          {!! Form::label('title_ar', __('page.title Arabic') ) !!}
          <div class="input-group">
              <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
              {!! Form::text('title_ar',null,['class'=>'form-control','required']) !!}
          </div>

          {!! Form::label('details_en', __('page.details English') ) !!}
          {!! Form::textarea('details_en',null,['class'=>'form-control','required','rows'=>'7']) !!}

          {!! Form::label('details_ar', __('page.details Arabic') ) !!}
          {!! Form::textarea('details_ar',null,['class'=>'form-control','required','rows'=>'7']) !!}

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.Add'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- edit ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          edit_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('form_header')
            {!! Form::model( new \App\AboutUs,['url'=>'AboutUs/update','class'=>"mydirection",'id'=>'edit_form']) !!}
        @endslot
        @slot('body')
              {!! Form::hidden('id',null,['id'=>'edit_id']) !!}

              {!! Form::label('title_en', __('page.title English') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('title_en',null,['class'=>'form-control','id'=>'edit_name_en','required']) !!}
              </div>

              {!! Form::label('title_ar', __('page.title Arabic') ) !!}
              <div class="input-group">
                  <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                  {!! Form::text('title_ar',null,['class'=>'form-control','id'=>'edit_name_ar','required']) !!}
              </div>

              {!! Form::label('details_en', __('page.details English') ) !!}
              {!! Form::textarea('details_en',null,['class'=>'form-control','id'=>'edit_details_en','required','rows'=>'7']) !!}

              {!! Form::label('details_ar', __('page.details Arabic') ) !!}
              {!! Form::textarea('details_ar',null,['class'=>'form-control','id'=>'edit_details_ar','required','rows'=>'7']) !!}

        @endslot
        @slot('submit_input')
          {!! Form::submit(__('page.update'),['class'=>'btn btn-success']) !!}
        @endslot
    @endcomponent

{{-- ------------------- show ------------------------ --}}
    @component('componets.modal')
        @slot('id')
          show_model
        @endslot
        @slot('header')
          @lang('page.edit')
        @endslot
        @slot('body')
               <table class="table">
                   <tr>
                       <th> @lang('page.name Arabic') </th>
                       <td id="show_name_ar">  </td>
                   </tr>
                   <tr>
                       <th> @lang('page.name English') </th>
                       <td id="show_name_en">  </td>
                   </tr>
                   <tr>
                       <th> @lang('page.details Arabic') </th>
                       <td id="show_details_ar">  </td>
                   </tr>
                   <tr>
                       <th> @lang('page.details English') </th>
                       <td id="show_details_en">  </td>
                   </tr>
               </table>
        @endslot

    @endcomponent


  @else
 <br><br>
 <div class="container">
     <h2> @lang('page.you dont have a permissions') </h2>
 </div>
@endpermission

@endsection


@section('script')
    {{-- <script type="text/javascript" src="{{asset('atlant/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script> --}}

    <script>
        var list_path = '{{asset('AboutUs')}}';
    </script>
    <script src="{{asset('js/AboutUs.js')}}"> </script>


@endsection
