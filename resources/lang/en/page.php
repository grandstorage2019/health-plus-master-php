<?php

return [

  'Country' => 'Country',
  //-------------------------------old--------------------------------------
  'Category' => 'Category',
  'category' => 'category',
  'users' => 'users',
  'Users' => 'Users',
  'User' => 'User',
  'Dashboard' => 'Dashboard',
  'English' => 'English',
  'Arabic' => 'Arabic',
  'add new' => 'Add new',
  'search' => 'search',
  'Create Category' => 'Create Category',
  'name Arabic' => 'name Arabic',
  'name English' => 'name English',
  'image' => 'image',
  'images' => 'images',
  'Add' => 'Add',
  'more' => 'more',
  'More' => 'more',
  'create' => 'create',
  'Edit' => 'Edit',
  'show' => 'show',
  'edit' => 'edit',
  'update' => 'update',

  'search' => 'search',
  'Edit Country' => 'Edit Country',
  'Add image' => 'Add image',
  'name' => 'name',
  'path' => 'path',
  'path' => 'path',
  'are you sure ?' => 'are you sure ?',
  'you want to delete ' => 'you want to delete ',
  'yes ,deleted' => 'yes ,Deleted',
  'cancle' => 'cancle',

  'Add' => 'Add',
  'from date' => 'from date',
  'form date' => 'form date',
  'to date' => 'to date',

  'push notification' => 'push notification',
  //-----user----
  'model' => 'model',
  'os' => 'os',
  'app version' => 'app version',
  'phone' => 'phone',
  'created date' => 'created date',

  'lat' => 'lat',
  'lang' => 'lang',

  'type' => 'type',

  'create new' => 'create new',

  'Popular Questions' => 'Popular Questions',
  'question' => 'question',
  'published' => 'published',
  'answer' => 'answer',
  'answer Arabic' => 'answer Arabic',
  'question Arabic' => 'question Arabic',
  'answer English' => 'answer English',
  'question English' => 'question English',
  'Chat' => 'Chat',
  //-------------------
  'All User' => 'All User',
  'All Item' => 'All Item',
  'All Recipt' => 'All Recipt',
  'All Reporet' => 'All Reporet',

  'android' => 'android',
  'Ios' => 'Ios',
  'wrong' => 'wrong',
  'Messages' => 'Messages',
  'messages' => 'messages',
  'Admin' => 'Admin',
  'Your message...' => 'Your message...',
  'Send' => 'Send',
  'Yes' => 'Yes',
  'yes' => 'Yes',
  'No' => 'No',
  'no' => 'No',
  'Log out' => 'Log out',
  'Are you sure you want to log out?' => 'Are you sure you want to log out?',
  'Press No if youwant to continue work. Press Yes to logout current user' => 'Press No if youwant to continue work. Press Yes to logout current user',

  'submit' => 'submit',

  //=======================================The very New================================================================
  'view' => 'view',
  'starts' => ' starts ',
  'views' => 'views ',
  'number' => ' number ',
  'street name' => 'street name  ',
  'area' => ' area ',
  'specific time' => 'specific time',
  'first reservation' => 'first reservation',
  'Doc Sub Specialization' => 'Doc Sub Specialization',
  'specialization' => ' pecialization ',
  'Specialization' => 'Specialization',
  'active' => 'active',


  'you dont have a permissions' => 'you dont have a permissions',
  'About Us' => 'About Us',
  'Create About Us' => 'Create About Us',
  'create Admin' => 'create Admin',
  'username' => 'username',
  'password' => 'password',
  'password_again' => 'password_again',
  'email' => 'email',
  'role' => 'role',
  'Role' => 'Role',
  'Roles' => 'Roles',
  'title English' => 'title English',
  'title Arabic' => 'title Arabic',
  'details English' => 'details English',
  'details Arabic' => 'details Arabic',
  'AboutUs' => 'AboutUs',
  'Area' => 'Area',
  'Create Area' => 'Create Area',
  'country' => 'country',
  'governorate' => 'governorate',
  'Location' => 'Location',
  'Country' => 'Country',
  'last 15 un accapted doctor' => 'last 15 un accapted doctor',
  'un accapted doctor' => 'un accapted doctor',
  'is not accapted yet' => 'is not accapted yet',
  'Show all doctors' => 'Show all doctors',
  'Care' => 'Care',
  'care' => 'Care',
  'Info' => 'Info',
  'specialization' => 'specialization',
  'Specialization' => 'Specialization',
  'from time' => 'from time',
  'to time' => 'to time',
  'about English' => 'about English',
  'about Arabic' => 'about Arabic',
  'location comment ar' => 'location comment Arabic',
  'location comment en' => 'location comment English',
  'facebook' => 'facebook',
  'instagram' => 'instagram',
  'twitter' => 'twitter',
  'snapchat' => 'snapchat',
  'services' => 'services',
  'Care service' => 'Care service',
  'care services' => 'care services',
  'images' => 'images',
  'Care image' => 'Care image',
  'id' => 'id',
  'Care Advertising' => 'Care Advertising',
  'Create Care Advertising' => 'Create Care Advertising',
  'Create Care' => 'Create Care',
  'Advertising' => 'Advertising' ,
  'link' => 'link',
  'is promited' => 'is promited',
  'Country' => 'Country',
  'Create Country' => 'Create Country',
  'Patient' => 'Patient',
  'All Patient' => 'All Patient',
  'Doctor' => 'Doctor',
  'All Doctor' => 'All Doctor',
  'All Care' => 'All Care',
  'Care Offer' => 'Care Offer',
  'All Care Offer' => 'All Care Offer',
  'patients' => 'patients',
  'patients' => 'patients',
  'android' => 'android',
  'Ios' => 'Ios',
  'is active' => 'is active',
  'not active' => 'not active',
  'Advertising' => 'Advertising',
  'care Advertising' => 'care Advertising',
  'patient Advertising' => 'patient Advertising',
  'Patient Advertising' => 'Patient Advertising',
  'Reservation' => 'Reservation',
  'specific Time' => 'specific Time',
  'first Reservation' => 'first Reservation',
  'Doctor Specialization' => 'Doctor Specialization',
  'Create Doctor Specialization' => 'Create Doctor Specialization',
  'Doctor sub Specialization' => 'Doctor sub Specialization',
  'doctor Sub Specialization' => 'Doctor sub Specialization',
  'Create Doctor Sub Specialization' => 'Create Doctor Sub Specialization',
  'fname_en' => 'fname_en',
  'fname_ar' => 'fname_ar',
  'lname_en' => 'lname_en',
  'lname_ar' => 'lname_ar',
  'accapted by admin' => 'accapted by admin',
  'price' => 'price',
  'confirm password' => 'confirm password',
  'birth_day' => 'birth_day',
  'gender' => 'gender',
  'plan' => 'plan',
  'Plan' => 'Plan',
  'Times' => 'Times',
  'Time' => 'Times',
  'available times' => 'available times',
  'choose plan' => 'choose plan',
  'life time' => 'life time',
  'specific date' => 'specific date',
  'day' => 'day',
  'from' => 'from',
  'to' => 'to',
  'type' => 'type',
  'wating time' => 'wating time',
  'num resrvation' => 'num resrvation',
  'first reservation' => 'first reservation',
  'specific time' => 'specific time',
  'Sat' => 'Sat',
  'Sun' => 'Sun',
  'Mon' => 'Mon',
  'Tue' => 'Tue',
  'Thu' => 'Thu',
  'Wed' => 'Wed',
  'Fri' => 'Fri',
  'license' => 'license',
  'doctorservices' => 'doctor Services (multi select)',
  'Insurance companies (multi select)' => 'Insurance companies (multi select)',
  'Insurance companies' => 'Insurance companies',
  'Clicnic' => 'Clicnic',
  'No Clicnic' => 'No Clicnic',
  'street name English' => 'street name English',
  'street name Arabic' => 'street name Arabic',
  'apartment English' => 'street apartment English',
  'apartment Arabic' => 'street apartment Arabic',
  'building number English' => 'building number English',
  'building number Arabic' => 'building number Arabic',
  'floor English' => 'floor English',
  'floor Arabic' => 'floor Arabic',
  'landmark English' => 'landmark English',
  'landmark Arabic' => 'landmark Arabic',
  'Clicnic image' => 'Clicnic image',
  'Clicnic images' => 'Clicnic images',
  'Clicnic Images' => 'Clicnic Images',
  'Create Doctor' => 'Create Doctor',
  'No Clicnic Images' => 'No Clicnic Images',
  'accapted by admin' => 'accapted by admin',
  'No patient resirvation' => 'No patient resirvation',
  'Governorate' => 'Governorate',
  'Create Governorate' => 'Create Governorate',
  'Insurance Company' => 'Insurance Company',
  'Create Insurance Company' => 'Create Insurance Company',
  'Notification Care' => 'Notification Care',
  'Create Notification Care' => 'Create Notification Care',
  'details' => 'details',
  'message' => 'message',
  'Notification Patient' => 'Notification Patient',
  'Create Notification Care' => 'Create Notification Care',
  'image' => 'image',
  'Patient' => 'Patient',
  'Create Patient' => 'Create Patient',
  'Create Patient Advertising' => 'Create Patient Advertising',
  'Create Plan' => 'Create Plan',
  'Create Role' => 'Create Role',
  'permissions' => 'permissions',
  'place' => 'place',
  'comment' => 'comment',
  'Care Specialization' => 'Care Specialization',
  'main page' => 'main page',
  'Setting' => 'Setting',
  'Create Setting' => 'Create Setting',
  'first_page_adverting_image' => 'first_page_adverting_image',
  'employee email' => 'employee email',
  'Notification' => 'Notification',
  'Contact Us' => 'Contact Us',
  'Create Contact Us' => 'Create Contact Us',
  'Doc Specialization' => 'Doc Specialization ',
  'Care sub services' => 'Care sub services ',
  'Care Sub Services' => 'Care sub services ',
  'Create Care Sub Services' => 'Care sub services ',
  'Create Care Specialization' => 'Create Care Specialization',
  'add' => 'add',
  'not accapted' => 'not accapted',
  'accapted' => 'accapted',
  'delete' => 'delete',
  'quantity' => 'quantity',
  'sub service name' => 'sub service name',
  'sub service quantity' => 'sub service quantity',
  'sub service price' => 'sub service price',
  'No sub services , you can add from side menu -> care -> care sub services'  => 'No sub services , you can add from side menu -> care -> care sub services' ,
  'first_page_adverting_link' => 'first page adverting link',
  'edit Role' => 'edit Role',
  'password_again' => 'password again',
  'username' => 'user name',
  'Plans' => 'Plans',
  'days' => 'days',
  'male' =>  'male',
  'female' =>  'female',
  'more info' => 'more info' ,
  'for a life time' => 'for a life time',
  'patient resirvation (last 15)' => 'patient resirvation (last 15)',
  'secretary email' => 'secretary email',
  'DoctorServices' => 'Doctor Services',
  'Create Doctor Services' => 'Create Doctor Services',
  'Doctor Services' => 'Doctor Services',
  'Add Doctor Services' => 'Add Doctor Services',
  'currency' => 'currency',
  'No Insurance companies' => 'No Insurance companies' ,
  'upcoming reservations for doctor' =>  'Upcoming Reservations for doctor' ,
  'cancle the reservation' => 'cancle the reservation' ,
  'Patient name' => 'Patient name' ,
  'Patient email' => 'Patient email' ,
  'Patient phone' => 'Patient phone' ,
  'reservations' => 'reservations' ,
  'Patient id' => 'Patient id' ,
  'Reservation last 50' => 'health + Reservation last 50' ,
  'health_plus reservation' => 'Reservations health plus' ,
  'reservation_id' => 'reservation id' ,
  'doctor_id' => 'doctor id' ,
  'doctor_name' => 'doctor name' ,
  'patient_id' => 'patient id' ,
  'patient_name' => 'patient name' ,
  'time' => 'time' ,
  'health_Reservation' => 'health Reservation' ,
  'level_ar' => 'level Arabic' ,
  'level_en' => 'level English' ,
  'created at' => 'created at' ,
  'latitude' => 'latitude' ,
  'longitude' => 'longitude' ,
  'CanclePatient' => 'Cancled Patient' ,
  'Doctor name' => 'Doctor name' ,
  'patient_phone' => 'patient phone' ,
  'patient_email' => 'patient email' ,
  'care_resvation_id' => 'care resvation id' ,
  'care_sub_services_id' => 'care sub services id' ,
  'health_care reservation' => 'health care reservation' ,
  'care_Reservation' => 'care Reservation' ,
  'CancleCare' => 'Cancle Care' ,
  'Care_id' => 'Care id',
  'Care name' => ' Care name',
  'unit' => 'unit',
  'unit_en' => 'unit English',
  'unit_ar' => 'unit Arabic',
];
