<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocSpecialization extends Model
{
    protected $table = 'specialization';
    protected $fillable = [
        'name_ar', 'name_en', 'status'
    ];

    public function subSpecialization()
    {
        return $this->hasMany(DocSubSpecialization::class,'specialization_id');
    }

}
