<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareOffer extends Model
{
    protected $table = 'care_offers';
    protected $fillable = [
        'image','link','status','care_id'
    ];

    public function get_care()
    {
        return $this->belongsTo(Care::class,'care_id');
    }

}
