<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancleCare extends Model
{
    protected $table = 'cancled_cares';
    const UPDATED_AT = null;
    protected $fillable = [
        'patient_id','care_sub_service_id'
    ];

    public function Patient()
    {
         return $this->belongsTo(Patient::class,'patient_id');
    }

    public function CareSubService()
    {
         return $this->belongsTo(CareSubService::class,'care_sub_service_id');
    }



}
