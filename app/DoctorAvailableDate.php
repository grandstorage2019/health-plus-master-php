<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DoctorAvailableDate extends Model
{
    protected $table = 'doctor_available_date';
    public $timestamps = false;
    protected $dates = ['from_date','to_date'];
    protected $fillable = [
        'doctor_id', 'from_date', 'to_date','is_life_time',
        'Sat_from_time','Sat_to_time','Sat_type','Sat_wating_time','Sat_num_resrvation',
        'Sun_from_time','Sun_to_time','Sun_type','Sun_wating_time','Sun_num_resrvation',
        'Mon_from_time','Mon_to_time','Mon_type','Mon_wating_time','Mon_num_resrvation',
        'Tue_from_time','Tue_to_time','Tue_type','Tue_wating_time','Tue_num_resrvation',
        'Thu_from_time','Thu_to_time','Thu_type','Thu_wating_time','Thu_num_resrvation',
        'Wed_from_time','Wed_to_time','Wed_type','Wed_wating_time','Wed_num_resrvation',
        'Fri_from_time','Fri_to_time','Fri_type','Fri_wating_time','Fri_num_resrvation'
    ];
}
