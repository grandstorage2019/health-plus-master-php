<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AboutUs extends Model
{
    public $timestamps = false;
    protected $table = 'about_us';
    protected $fillable = [
        'title_en','title_ar','details_en','details_ar' 
    ];
}
