<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doc_Reservation extends Model
{
    protected $table = 'reservations';
    protected $dates = ['date'];
    protected $fillable = [
        'doctor_id', 'patient_id', 'date','type','time'
    ];
}
