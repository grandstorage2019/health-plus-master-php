<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarePicture extends Model
{
    protected $table = 'care_pictures';
      const UPDATED_AT = null;
    protected $fillable = [
        'care_id','image'
    ];
}
