<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareSubService extends Model
{
      public $timestamps = false;
      protected $table = 'care_sub_services';
      protected $fillable = [
          'service_id','name_en','name_ar','quantity','price','image','status','currency','unit_en','unit_ar'
      ];

      public function get_service()
      {
         return $this->belongsTo(CareService::class,'service_id');
      }

      public function getImageAttribute($value){
          return asset('images/CareSubService/'.$value);
      }
}
