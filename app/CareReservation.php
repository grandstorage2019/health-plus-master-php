<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareReservation extends Model
{
      protected $table = 'care_resvation';
      const UPDATED_AT = null;
      protected $fillable = [
          'patient_id','care_sub_services_id','price'
      ];
}
