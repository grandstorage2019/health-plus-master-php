<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareSpecialization extends Model
{
    protected $table = 'care_specialization';
    protected $fillable = [
        'name_ar', 'name_en', 'status'
    ];
}
