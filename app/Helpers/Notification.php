<?php

namespace App\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doctor;

class Notification extends Controller
{
    public static function get_unAccapted_doctors()
    {
        return Doctor::where('is_accapted_by_admin',0)->limit(15)->get();
    }

    public static function get_unAccapted_doctors_count()
    {
        return Doctor::where('is_accapted_by_admin',0)->count();
    }
}
