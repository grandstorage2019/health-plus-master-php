<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientAdvertising extends Model
{
    protected $table = 'patient_advertising';
    protected $fillable = [
        'title_en','title_ar','image','link','is_promited','status'
    ];
}
