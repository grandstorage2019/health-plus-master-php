<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Patient extends Authenticatable
{
    use Notifiable;

    protected $table = 'patients';
    protected $fillable = [
        'type', 'firebase_token', 'status','model','app_version','os','name', 'email','password','phone','birth_day','gender','country_id',
        'facebook_id','google_id','registred','username','device_token','added_by_doctor_id'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    public function get_country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }

    public function get_reservationed_doctor()
    {
        return $this->belongsToMany(Doctor::class,'reservations','patient_id','doctor_id')->withPivot('date','type','time')
                          ->select('fname_en','lname_en','fname_ar','lname_ar','specialization_id') ;
    }

    public function get_reservationed_doctor_limit()
    {
        return $this->belongsToMany(Doctor::class,'reservations','patient_id','doctor_id')->withPivot('date','type','time')
                          ->select('fname_en','lname_en','fname_ar','lname_ar','specialization_id')->limit(15) ;
    }


}
