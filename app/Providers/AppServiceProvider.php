<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Blade::if('permission', function ($val) {

            if ( auth()->user()->super_admin ) {
                return true;
            }

            foreach (auth()->user()->get_Role->get_Permissions as $value)
            {
                if ($value->title == $val)
                {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
