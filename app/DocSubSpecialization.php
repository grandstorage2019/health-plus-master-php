<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocSubSpecialization extends Model
{
    public $timestamps = false;
    protected $table = 'sub_specialization';
    protected $fillable = [
        'text_en', 'text_ar', 'specialization_id','status'
    ];

    public function get_specialization()
    {
        return $this->belongsTo( DocSpecialization::class ,'specialization_id');
    }

}
