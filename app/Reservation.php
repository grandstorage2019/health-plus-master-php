<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'doctor_id','patient_id','date','type','time'
    ];

    public function get_Patient()
    {
       return $this->belongsTo(Patient::class,'patient_id');
    }

    public function get_Doctor()
    {
       return $this->belongsTo(Doctor::class,'doctor_id');
    }

}
