<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClinicPicture extends Model
{
      protected $table = 'clinic_picture';
      public $timestamps = false;
      protected $fillable = [
          'image', 'doctor_id'
      ];



}
