<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CanclePatient extends Model
{
    protected $table = 'cancled_patients';
    const UPDATED_AT = null;
    protected $fillable = [
        'patient_id','doctor_id'
    ];

    public function Doctor()
    {
         return $this->belongsTo(Doctor::class,'doctor_id');
    }

    public function Patient()
    {
         return $this->belongsTo(Patient::class,'patient_id');
    }

}
