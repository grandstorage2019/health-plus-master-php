<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $first_page_adverting_image = Setting::where('title','first_page_adverting_image')->first();
        $employee_email = Setting::where('title','employee_email')->first();
        $first_page_adverting_link = Setting::where('title','first_page_adverting_link')->first();
        return view('Setting.index',compact('first_page_adverting_image','employee_email','first_page_adverting_link'));
    }


    public function store(Request $request)
    {
          $data = $request->validate([
              'employee_email' => 'required',
          ]);
          if ($request->first_page_adverting_image)
          {
              $extension = $request->first_page_adverting_image->getClientOriginalExtension(); //get img extention
              $fileName  = rand(11111,99999).'.'.$extension; // renameing image
              $destinationPath = public_path('images/setting');
              $request->first_page_adverting_image->move($destinationPath, $fileName); // uploading file to given path
              Setting::where('title','first_page_adverting_image')->update([
                  'value' => $fileName
              ]);
          }
          Setting::where('title','first_page_adverting_link')->update([
            'value' => $request->first_page_adverting_link
          ]);
          Setting::where('title','employee_email')->update([
            'value' => $request->employee_email
          ]);

          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' الاعدادات اتعدلت ');   }
          else
            { \Session::flash('flash_message','Setting  has Updated');  }

          return redirect('Setting');
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
