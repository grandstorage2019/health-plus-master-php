<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocalizationController extends Controller
{
      public function __construct()
      {
         $this->middleware('auth');
      }

     public function setLang($get_lang)
     {
           \Session::put('lang',$get_lang);
           app()->setLocale($get_lang);  
           \Auth::user()->lang = $get_lang;
           \Auth::user()->save();
           return back();
     }
}
