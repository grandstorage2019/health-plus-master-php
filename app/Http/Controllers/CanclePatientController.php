<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CanclePatient;

class CanclePatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $CanclePatients = CanclePatient::latest()->paginate();
        return view('CanclePatient.index',compact('CanclePatients') );
    }

    public function search($val)
    {
        $CanclePatients = CanclePatient::where('patient_id',$val)->orWhere('doctor_id',$val)
        ->orWhereHas('Doctor',function($query)use($val){
            $query->whereRaw("CONCAT(fname_en,' ',lname_en) like ?",['%'.$val.'%'])
                    ->orWhereRaw("CONCAT(fname_en,' ',lname_en) like ?",['%'.$val.'%']);
        })->OrWhereHas('Patient',function($query)use($val){
            $query->where('name','like','%'.$val.'%')->orWhere('email','like','%'.$val.'%');
        })->latest()->paginate();

        return view('CanclePatient.index',compact('CanclePatients','val') );
    }

    public function destroy($id)
    {
        CanclePatient::destroy($id);
        return redirect('CanclePatient');
    }
}
