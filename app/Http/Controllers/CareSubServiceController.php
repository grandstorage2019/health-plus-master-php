<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareSubService;
use App\CareService;
use App\Care;

class CareSubServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        if (\Session::get('lang') == 'ar')
        {
            $cares = Care::latest()->pluck('name_ar','id');
            $cares = [ ''=>' اختار الكير ' ] + collect($cares)->toArray();
        }
        else {
          $cares = Care::latest()->pluck('name_en','id');
          $cares = [ ''=>' choose care ' ] + collect($cares)->toArray();
        }

        return view('CareSubService.index',compact('cares') );
    }

    //--api--
    public function get_services($care_id)
    {
        if(\Session::get('lang') == 'ar')
        {
           $CareService = CareService::where('care_id',$care_id)->select('name_ar as name','id')->get();
        }
        else {
          $CareService = CareService::where('care_id',$care_id)->select('name_en as name','id')->get();
        }

        $data = array(); $temp = array();

        foreach ($CareService as $col)
        {
              $temp['text'] = $col->name;
              $temp['id'] = $col->id;
              $data[]  = $temp;
        }
        return $data;
    }

    public function get_CareSubService($services_id)
    {
        $CareSubService = CareSubService::where('service_id',$services_id)
                        ->select('name_ar','name_en','id','quantity','price','status','image','currency','unit_en','unit_ar')->get();
        return $CareSubService;
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'service_id' => 'required'
        ]);
        $request->ids = array_filter($request->ids);
        CareSubService::where('service_id',$request->service_id)->whereNotIn('id',$request->ids)->delete();
        for ($i=0; $i < count($request->count) ; $i++)
        {
            if ( isset($request->name_en[$i]) )
            {
                $the_data = [
                  'name_en' => $request->name_en[$i] ,
                  'name_ar' => $request->name_ar[$i] ,
                  'quantity' => $request->quantity[$i] ,
                  'price' => $request->price[$i],
                  'currency' => $request->currency[$i],
                  'unit_en' => $request->unit_en[$i],
                  'unit_ar' => $request->unit_ar[$i]
                ];
                if ($request->status[$i] == 'on')
                {
                    $the_data['status'] = 1;
                }
                else {
                  $the_data['status'] = 0;
                }

                if (isset($request->images[$i]))
                {
                    $extension = $request->images[$i]->getClientOriginalExtension(); //get img extention
                    $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                    $destinationPath = public_path('images/CareSubService');
                    $request->images[$i]->move($destinationPath, $fileName); // uploading file to given path
                    $the_data['image'] = $fileName;
                }

                if (isset($request->ids[$i]))
                {
                    CareSubService::whereId($request->ids[$i])->update($the_data);
                }
                else
                {
                    $the_data['service_id'] = $request->service_id;
                    CareSubService::create($the_data);
                }
            }
        }
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message','صب تخصص اضاف ');   }
        else
          { \Session::flash('flash_message','ٍSub Specialization  has added');  }

        return redirect('CareSubServices');
    }


    public function showORhide($id)
    {
        $CareSubService = CareSubService::findOrFail($id);
           if( $CareSubService->status )
           {
                 $CareSubService->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' التخصص الفرعي للكير '.$CareSubService->name.' كير ');   }
                else
                  { \Session::flash('flash_message','Care Sub Service '.$CareSubService->name.' is not public any more');  }
           }
           else
           {
                $CareSubService->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$CareSubService->name.' التخصص الفرعي للكير ');   }
                else
                  { \Session::flash('flash_message','Care Sub Service'.$CareSubService->name.' now is public ');  }
           }

           return back();
    }


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
