<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocSpecialization;

class DocSpecializationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $DocSpecializations = DocSpecialization::orderBy('name_en')->paginate();
        return view('DocSpecialization.index',compact('DocSpecializations'));
    }

    public function search($val)
    {
         $DocSpecializations = DocSpecialization::where('name_en','like','%'.$val.'%')->orWhere('name_ar','like','%'.$val.'%')
                                                ->orderBy('name_en')->paginate();
         return view('DocSpecialization.index',compact('DocSpecializations','val'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required'
        ]);
        $DocSpecialization = DocSpecialization::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$DocSpecialization->name_ar.' تخصص');   }
        else
          { \Session::flash('flash_message','Specialization '.$DocSpecialization->name_en.' has added');  }

        return redirect('DocSpecialization');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required'
        ]);
        $DocSpecialization = DocSpecialization::findOrFail($request->id);
        $DocSpecialization->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتعدل '.$DocSpecialization->name_ar.' تخصص');   }
        else
          { \Session::flash('flash_message','Specialization '.$DocSpecialization->name_en.' has updated');  }

        return redirect('DocSpecialization');
    }

    public function showORhide($id)
    {
        $DocSpecialization = DocSpecialization::findOrFail($id);
           if( $DocSpecialization->status )
           {
                 $DocSpecialization->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' لن يظهر للعامة '.$DocSpecialization->name.' كير ');   }
                else
                  { \Session::flash('flash_message','Care '.$DocSpecialization->name.' is not public any more');  }
           }
           else
           {
                $DocSpecialization->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$DocSpecialization->name.' التخصص الفرعي ');   }
                else
                  { \Session::flash('flash_message','Sub Specialization '.$DocSpecialization->name.' now is public ');  }
           }

           return back();
    }

}
