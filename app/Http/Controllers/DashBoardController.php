<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use App\Doctor;
use App\Care;
use App\CareOffer;
use App\CareAdvertising;
use App\PatientAdvertising;
use App\Reservation;
use Carbon\Carbon;

class DashBoardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
//        return auth()->user();
//        return auth()->user()->get_Role->get_Permissions;
        $lang = app()->getLocale()??'en';

        $patient_count = Patient::count();
        $doctor_count = Doctor::count();
        $care_count = Care::count();
        $careOffer_count = CareOffer::count();
        $careAdvertising_count = CareAdvertising::count();
        $patientAdvertising_count = PatientAdvertising::count();

        //--------- Doctor ----------
        $Accapted_by_admin = Doctor::where('is_accapted_by_admin','1')->count();
        $Not_accapted_by_admin = Doctor::where('is_accapted_by_admin','0')->count();

        //--------- android , iOS ----------
        $android_patient_count = Patient::where('os','android')->count();
        $ios_patient_count = Patient::where('os','ios')->count();

        //--------- Reservation type ----------
        $Reservation_specificTime = \DB::table('reservations')->where('type','specific time')->count();
        $Reservation_firstReservation = \DB::table('reservations')->where('type','first reservation')->count();

        $morris_Patients = \DB::select("SELECT count(id) as count , month(`created_at`) as month FROM `patients` WHERE YEAR(`created_at`) = ".date("Y")." group BY month(`created_at`) ORDER by `created_at` ASC") ;
        $morris_Doctors = \DB::select("SELECT count(id) as count , month(`created_at`) as month FROM `doctors` WHERE YEAR(`created_at`) = ".date("Y")." group BY month(`created_at`) ORDER by `created_at` ASC") ;
        $morris_Cares = \DB::select("SELECT count(id) as count , month(`created_at`) as month FROM `care` WHERE YEAR(`created_at`) = ".date("Y")." group BY month(`created_at`) ORDER by `created_at` ASC") ;
        $morris_Reservations = \DB::select("SELECT count(id) as count , month(`created_at`) as month FROM `reservations` WHERE YEAR(`created_at`) = ".date("Y")." group BY month(`created_at`) ORDER by `created_at` ASC") ;


         $Reservations_calender = Reservation::select( \DB::raw("CONCAT(doctors.fname_".$lang.",' ',doctors.lname_".$lang.") as doctor_name"), "reservations.id",
                                           'patients.name as patient_name', "date","time")
                                  ->leftJoin('doctors','doctors.id','reservations.doctor_id')
                                  ->leftJoin('patients','patients.id','reservations.patient_id')
                                  ->groupBy('reservations.id')
                                  ->latest('reservations.date')->limit(50)->get();

        return view('Dashboard.Dashboard',compact('patient_count','doctor_count','care_count','careOffer_count','careAdvertising_count','patientAdvertising_count',

                        //---more---
                        'android_patient_count','ios_patient_count','Accapted_by_admin','Not_accapted_by_admin',
                        'Reservation_specificTime','Reservation_firstReservation',

                        'morris_Patients','morris_Doctors','morris_Cares','morris_Reservations' , 'Reservations_calender'
                  ));
    }
}
