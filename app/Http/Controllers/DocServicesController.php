<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DocServices;

class DocServicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $DocServices = DocServices::get();
        return view('DoctorServices.index',compact('DocServices'));
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required'
        ]);
        DocServices::whereNotIn('id',$request->ids)->delete();
        for ($i=0; $i < count($request->count) ; $i++)
        {
            if ( isset($request->name_en[$i]) )
            {
                $the_data = [
                  'name_en' => $request->name_en[$i] ,
                  'name_ar' => $request->name_ar[$i]
                ];
                if ($request->status[$i] == 'on')
                {
                    $the_data['status'] = 1;
                }
                else {
                  $the_data['status'] = 0;
                }

                if (isset($request->ids[$i]))
                {
                    DocServices::whereId($request->ids[$i])->update($the_data);
                }
                else {
                    DocServices::create($the_data);
                }
            }
        }
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' خدمات الطبيب اضافت ');   }
        else
          { \Session::flash('flash_message',' Doctor Services has added ');   }


        return back();
    }


}
