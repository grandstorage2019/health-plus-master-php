<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;

class PatientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $patients = Patient::orderBy('name')->paginate();
        return view('Patient.index',compact('patients'));
    }

    public function search($val)
    {
        $patients = Patient::where('name','like','%'.$val.'%')->orWhere('email','like','%'.$val.'%')->orWhere('os','like','%'.$val.'%')
                           ->orWhere('gender','like','%'.$val.'%')->orWhere('id',$val)->orderBy('name')->paginate();
        return view('Patient.index',compact('patients','val'));
    }


    // public function store(Request $request)
    // {
    //     //
    // }


    public function show($id)
    {
        $patient = Patient::findOrFail($id);
        return view('Patient.show',compact('patient'));
    }

    public function showORhide($id)
    {
        $patient = Patient::findOrFail($id);

        if( $patient->status )
        {
              $patient->update(['status' => '0']);

             if( \Session::get('lang') == 'ar' )
               { \Session::flash('flash_message',' لن يظهر للعامة '.$patient->name.' كير ');   }
             else
               { \Session::flash('flash_message','Care '.$patient->name.' is not public any more');  }
        }
        else
        {
             $patient->update(['status' => '1']);
             if( \Session::get('lang') == 'ar' )
               { \Session::flash('flash_message',' الان سيظهر للعمامة '.$patient->name.' كير ');   }
             else
               { \Session::flash('flash_message','Care '.$patient->name.' now is public ');  }
        }

        return back();
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
