<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotificationPatient;

class NotificationPatientController extends Controller
{
      public function __construct()
      {
          $this->middleware('auth');
          $this->middleware('lang');
      }

      public function index()
      {
          $Notifications = NotificationPatient::latest()->paginate();
          return view('NotificationPatient.index',compact('Notifications'));
      }

      public function search($val)
      {
          $Notifications = NotificationPatient::where('message','like','%'.$val.'%')->latest()->paginate();
          return view('NotificationPatient.index',compact('Notifications','val'));
      }

      public function store(Request $request)
      {
          $data = $request->validate([
              'image' => 'required',
              'message' => 'required'
          ]);

          $extension = $request->image->getClientOriginalExtension(); //get img extention
          $fileName  = rand(11111,99999).'.'.$extension; // renameing image
          $destinationPath = public_path('images/notification_care');
          $request->image->move($destinationPath, $fileName); // uploading file to given path
          $data['image'] = $fileName;

          $Notifications = NotificationPatient::create($data);

          $firebase_tokens = \App\Patient::whereNotNull('google_id')->pluck('google_id')->toArray();
          if ($firebase_tokens)
          {
              $image = asset('images/notification_care/'.$Notifications->image);
              $responce = \App\PushNotification::send($firebase_tokens, $Notifications->message,$image,3);
          }

          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' الاشعرار اتنشر ');   }
          else
            { \Session::flash('flash_message',' Notification pushed ');  }

          return redirect('NotificationPatient');
      }

      public function push_agin($id)
      {
          $Notifications = NotificationPatient::findOrFail($id);
          $firebase_tokens = \App\Patient::whereNotNull('google_id')->pluck('google_id')->toArray();
          if ($firebase_tokens)
          {
              $image = asset('images/notification_care/'.$Notifications->image);
              $responce = \App\PushNotification::send($firebase_tokens, $Notifications->message,$image,3);
          }
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' الاشعرار اتنشر ');   }
          else
            { \Session::flash('flash_message',' Notification pushed ');  }

          return redirect('NotificationPatient');
      }


      public function destroy($id)
      {
          $Notifications = NotificationPatient::findOrFail($id);
          $Notifications->delete();
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' الاشعرار اتنشر ');   }
          else
            { \Session::flash('flash_message',' Notification pushed ');  }

          return redirect('NotificationPatient');
      }
}
