<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotificationCare;

class NotificationCareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $Notifications = NotificationCare::latest()->paginate();
        return view('NotificationCare.index',compact('Notifications'));
    }

    public function search($val)
    {
        $Notifications = NotificationCare::where('message','like','%'.$val.'%')->latest()->paginate();
        return view('NotificationCare.index',compact('Notifications','val'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required',
            'message' => 'required'
        ]);

        $extension = $request->image->getClientOriginalExtension(); //get img extention
        $fileName  = rand(11111,99999).'.'.$extension; // renameing image
        $destinationPath = public_path('images/notification_care');
        $request->image->move($destinationPath, $fileName); // uploading file to given path
        $data['image'] = $fileName;

        $Notifications = NotificationCare::create($data);

        $firebase_tokens = \App\Patient::whereNotNull('google_id')->pluck('google_id')->toArray();
        if ($firebase_tokens)
        {
          $image = asset('images/notification_care/'.$Notifications->image);
          $responce = \App\PushNotification::send($firebase_tokens, $Notifications->message,$image,2); //return $responce;
        }
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' الاشعرار اتنشر ');   }
        else
          { \Session::flash('flash_message',' Notification pushed ');  }

        return redirect('NotificationCare');
    }

    public function push_agin($id)
    {
        $Notifications = NotificationCare::findOrFail($id);
        $firebase_tokens = \App\Patient::whereNotNull('google_id')->pluck('google_id')->toArray();
        if ($firebase_tokens)
        {
            $image = asset('images/notification_care/'.$Notifications->image);
            $responce = \App\PushNotification::send($firebase_tokens, $Notifications->message,$image,2); //return $responce;
        }

        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' الاشعار اتبعت '); }
        else
          { \Session::flash('flash_message',' Notification pushed ');  }

        return redirect('NotificationCare');
    }


    public function destroy($id)
    {
        $Notifications = NotificationCare::findOrFail($id);
        $Notifications->delete();
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' الاشعرار اتنشر ');   }
        else
          { \Session::flash('flash_message',' Notification pushed ');  }

        return redirect('NotificationCare');
    }
}
