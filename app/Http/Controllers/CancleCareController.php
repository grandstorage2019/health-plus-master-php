<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CancleCare;

class CancleCareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $CancleCare = CancleCare::latest()->paginate();
        return view('CancleCare.index',compact('CancleCare') );
    }

    public function search($val)
    {
          $CancleCare = CancleCare::where('patient_id',$val)->orWhere('care_sub_service_id',$val)
              ->orWhereHas('CareSubService',function($query)use($val){
                  $query->where('name_en','like','%'.$val.'%');
              })
              ->OrWhereHas('Patient',function($query)use($val){
                  $query->where('name','like','%'.$val.'%')->orWhere('email','like','%'.$val.'%');
              })->latest()->paginate();

          return view('CancleCare.index',compact('CancleCare','val') );
    }





    public function destroy($id)
    {
        CancleCare::destroy($id);
        return redirect('CancleCare');
    }
}
