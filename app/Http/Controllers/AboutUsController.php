<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AboutUs;

class AboutUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $AboutUs = AboutUs::paginate(15);
        return view('AboutUs.index',compact('AboutUs'));
    }

    public function search($val)
    {
        $AboutUs = AboutUs::where('title_en','like','%'.$val.'%')->orWhere('title_ar','like','%'.$val.'%')->
        orWhere('details_en','like','%'.$val.'%')->orWhere('details_ar','like','%'.$val.'%')->paginate(15);
        return view('AboutUs.index',compact('AboutUs','val'));
    }

    public function store(Request $request)
    {
          $data = $request->validate([
              'title_en' => 'required',
              'title_ar' => 'required',
              'details_en' => 'required',
              'details_ar' => 'required',
          ]);
          $AboutUs = AboutUs::create($data);
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' عنا اضافت');   }
          else
            { \Session::flash('flash_message','About Us has added');  }

          return redirect('AboutUs');
    }


    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'title_en' => 'required',
            'title_ar' => 'required',
            'details_en' => 'required',
            'details_ar' => 'required',
        ]);
        $AboutUs = AboutUs::findOrFail($request->id);
        $AboutUs->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' عنا اتعدلت');   }
        else
          { \Session::flash('flash_message','About Us has updated');  }

        return redirect('AboutUs');
    }

    public function destroy($id)
    {
         $AboutUs = AboutUs::findOrFail($id);
         $AboutUs->delete();
         if( \Session::get('lang') == 'ar' )
           { \Session::flash('flash_message',' عنا اتمسحت');   }
         else
           { \Session::flash('flash_message','About Us has deleted');  }

         return redirect('AboutUs');
    }
}
