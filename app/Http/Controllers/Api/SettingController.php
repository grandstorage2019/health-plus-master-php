<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reservation;
use App\Setting;
use App\CancleCare;

class SettingController extends Controller
{
    public function privacy_policy()
    {
        return view('privacy_policy');
    }

    //--doctor reservation--
    public function sent_reservation_mails($reservation_id)
    {
        $reservation = Reservation::
                       select('reservations.date as reservation_date','reservations.type','reservations.time',
                                'patients.name as patient_name','patients.email as patient_email', 'patients.phone as patient_phone','patients.id as patient_id',
                                \DB::raw("CONCAT(doctors.fname_en ,' ',doctors.lname_en) as doctor_name"), 'doctors.id as doctor_id',
                                'doctors.email as doctor_email','doctors.secretary_email as secretary_email','doctors.phone as doctor_phone',
                                'specialization.name_en as specialization_en' )
                      ->leftJoin('patients','patients.id','reservations.patient_id')
                      ->leftJoin('doctors','doctors.id','reservations.doctor_id')
                      ->leftJoin('specialization','specialization.id','doctors.specialization_id')
                      ->where('reservations.id',$reservation_id)
                      ->groupBy('reservations.id')
                      ->first();

        if ($reservation)
        {
              \Mail::to($reservation->doctor_email)->send(new \App\Mail\ReservationDoctorMailMail($reservation));
              if ($reservation->secretary_email)
              {
                  \Mail::to($reservation->secretary_email)->send(new \App\Mail\ReservationDoctorMailMail($reservation));
              }

              $employee_email = Setting::where('title','employee_email')->first();    //return $employee_email->value;
              if ($employee_email->value)
              {
              \Mail::to($employee_email->value)->send(new \App\Mail\ReservationAdminMailMail($reservation));
              }
        }//End if($reservation)
        else {
        //   return response()->json([
        //     'status' => 'faild'
        //   ]);
        }
        //
        //
        //   return response()->json([
        //     'status' => 'success'
        //   ]);

    }

    //--care reservation--
    public function sent_care_reservation_mails($reservation_id)
    {
        $reservation = \DB::table('care_resvation')
                    ->select('care.id as care_id','care.email as care_email','care.name_en as care_name','patients.id as patient_id','patients.email as patient_email',
                              'patients.name as patient_name','care.phone as care_phone','patients.phone as patient_phone','care_resvation.id as reservation_id')
                    ->join('care_sub_services','care_sub_services.id','care_resvation.care_sub_services_id')
                    ->join('care_services','care_services.id','care_sub_services.service_id')
                    ->join('care','care.id','care_services.care_id')
                    ->join('patients','patients.id','care_resvation.patient_id')
                    ->where('care_resvation.id',$reservation_id)
                    ->groupBy('care_resvation.id')
                    ->first();
     // return  $reservation->care_email;
     //      dd( $reservation );
         $employee_email = Setting::where('title','employee_email')->first();    //return $employee_email->value;
         if ($employee_email->value)
         {
            \Mail::to($employee_email->value)->send(new \App\Mail\CareResrvationMail($reservation));
         }

         \Mail::to($reservation->care_email)->send(new \App\Mail\CareResrvationMail($reservation));
         \Mail::to($reservation->patient_email)->send(new \App\Mail\CareResrvationMail($reservation));
    }

    //--care reservation--
    public function sent_cancle_care_reservation_mails($reservation_id)
    {
        $reservation = \DB::table('care_resvation')
                      ->select('care_sub_services.name_en as care_sub_services_name','care_sub_services.id as care_sub_service_id',
                               'care_sub_services.quantity','care_sub_services.price','care_sub_services.currency','care_sub_services.unit_en',

                        'patients.name as patient_name','patients.id as patient_id','patients.email as patient_email','patients.phone as patient_phone',
                        'care_services.name_en as care_services_name',
                        'care.name_en as care_name','care.email as care_email' )
                        ->join('care_sub_services','care_sub_services.id','care_resvation.care_sub_services_id')
                        ->join('care_services','care_services.id','care_sub_services.service_id')
                        ->join('care','care.id','care_services.care_id')
                        ->join('patients','patients.id','care_resvation.patient_id')
                ->where('care_resvation.id',$reservation_id)
                ->groupBy('care_resvation.id')
                ->first();
          CancleCare::create([
              'patient_id' => $reservation->patient_id,
              'care_sub_service_id' =>  $reservation->care_sub_service_id ,
          ]);
     // return  $reservation->care_email;
          // dd( $reservation );
         $employee_email = Setting::where('title','employee_email')->first();    //return $employee_email->value;
         if ($employee_email->value)
         {
            \Mail::to($employee_email->value)->send(new \App\Mail\CancleCareReservationMail($reservation));
         }

         \Mail::to($reservation->care_email)->send(new \App\Mail\CancleCareReservationMail($reservation));
         \Mail::to($reservation->patient_email)->send(new \App\Mail\CancleCareReservationMail($reservation));

         \DB::table('care_resvation')->where('care_resvation.id',$reservation_id)->delete();
    }

    //cancle doctor reservations
    public function cancle_reservations($reservation_id)
    {
          $reservation = \App\Reservation::findOrFail($reservation_id);
          // $reservation->delete();

          $Patient_email = $reservation->get_Patient->email;
          $Doctor_email = $reservation->get_Doctor->email;

          $employee_email = \App\Setting::where('title','employee_email')->first();    //return $employee_email->value;
          if ($employee_email->value)
          {
              \Mail::to($employee_email->value)->send(new \App\Mail\CancleReservation($reservation));
          }
          \Mail::to($Patient_email)->send(new \App\Mail\CancleReservation($reservation));
          \Mail::to($Doctor_email)->send(new \App\Mail\CancleReservation($reservation));

          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message','  الحجز اتمسح ');   }
          else
            { \Session::flash('flash_message','reservation  has delete ');  }
    }

}
