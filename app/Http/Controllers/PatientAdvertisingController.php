<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PatientAdvertising;

class PatientAdvertisingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $PatientAdvertising = PatientAdvertising::latest()->paginate();
        return view('PatientAdvertising.index',compact('PatientAdvertising'));
    }

    public function search($val)
    {
        $PatientAdvertising = PatientAdvertising::where('title_en','like','%'.$val.'%')->orWhere('title_ar','like','%'.$val.'%')->
                                                  orWhere('link','like','%'.$val.'%')->latest()->paginate();
        return view('PatientAdvertising.index',compact('PatientAdvertising','val'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'title_en' => 'required',
            'title_ar' => 'required',
            'image' => 'required',
            'link' => 'required'
        ]);
              $extension = $request->image->getClientOriginalExtension(); //get img extention
              $fileName  = rand(11111,99999).'.'.$extension; // renameing image
              $destinationPath = public_path('images/doctor_adverting');
              $request->image->move($destinationPath, $fileName); // uploading file to given path
              $data['image'] = $fileName;
        if ( $request->has('is_promited') )
        {
            $data['is_promited'] = 1;
        }
        $PatientAdvertising = PatientAdvertising::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$PatientAdvertising->name_ar.' اعلان ');   }
        else
          { \Session::flash('flash_message',' Advertising '.$PatientAdvertising->name_en.' has added');  }

        return redirect('PatientAdvertising');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'title_en' => 'required',
            'title_ar' => 'required',
            'link' => 'required'
        ]);
        $PatientAdvertising = PatientAdvertising::findOrFail($request->id);
        if ( $request->has('is_promited') )
        {
            $data['is_promited'] = 1;
        }
        else {
          $data['is_promited'] = 0;
        }
        if (isset($request->image))
        {
            $extension = $request->image->getClientOriginalExtension(); //get img extention
            $fileName  = rand(11111,99999).'.'.$extension; // renameing image
            $destinationPath = public_path('images/doctor_adverting');
            $request->image->move($destinationPath, $fileName); // uploading file to given path
            $data['image'] = $fileName;
        }
        $PatientAdvertising->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتعدل '.$PatientAdvertising->name_ar.' اعلان ');   }
        else
          { \Session::flash('flash_message',' Advertising '.$PatientAdvertising->name_en.' has updated');  }

        return redirect('PatientAdvertising');
    }

    public function show($id)
    {
        //
    }


 
    public function destroy($id)
    {
        $PatientAdvertising = PatientAdvertising::findOrFail($id);
        $PatientAdvertising->delete();
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتمسح '.$PatientAdvertising->name_ar.' اعلان ');   }
        else
          { \Session::flash('flash_message',' Advertising '.$PatientAdvertising->name_en.' has deleted ');  }

        return redirect('PatientAdvertising');
    }
}
