<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareOffer;

class CareOfferController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $offers = CareOffer::latest()->paginate();
        if (\Session::get('lang') == 'ar')
        {
            $cares = \App\Care::latest()->pluck('name_en','id');
            $cares = [ ''=>' اختار الكير ' ] + collect($cares)->toArray();
        }
        else {
          $cares = \App\Care::latest()->pluck('name_ar','id');
          $cares = [ ''=>' choose Care ' ] + collect($cares)->toArray();
        }
        return view('CareOffer.index',compact('offers','cares'));
    }

    public function search($val)
    {
        $offers = CareOffer::where('link','like','%'.$val.'%')
                          ->orWhereHas('get_care',function($query) use($val){
                              $query->where('name_ar','like','%'.$val.'%')->orWhere('name_en','like','%'.$val.'%')->orWhere('phone','like','%'.$val.'%');
                          })->latest()->paginate();
        if (\Session::get('lang') == 'ar')
        {
            $cares = \App\Care::latest()->pluck('name_en','id');
            $cares = [ ''=>' اختار الكير ' ] + collect($cares)->toArray();
        }
        else {
          $cares = \App\Care::latest()->pluck('name_ar','id');
          $cares = [ ''=>' choose Care ' ] + collect($cares)->toArray();
        }
        return view('CareOffer.index',compact('offers','cares','val'));
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required',
            'link' => 'required',
            'care_id' => 'required',
        ]);
        $extension = $request->image->getClientOriginalExtension(); //get img extention
        $fileName  = rand(11111,99999).'.'.$extension; // renameing image
        $destinationPath = public_path('images/health_cares');
        $request->image->move($destinationPath, $fileName); // uploading file to given path
        $data['image'] = $fileName;
        $offers = CareOffer::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' عرض الكير اضاف ');   }
        else
          { \Session::flash('flash_message','Care offer has added');  }

        return redirect('CareOffer');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'image' => '',
            'link' => 'required',
            'care_id' => 'required',
        ]);
        if ($request->image)
        {
            $extension = $request->image->getClientOriginalExtension(); //get img extention
            $fileName  = rand(11111,99999).'.'.$extension; // renameing image
            $destinationPath = public_path('images/health_cares');
            $request->image->move($destinationPath, $fileName); // uploading file to given path
            $data['image'] = $fileName;
        }
        $offers = CareOffer::findOrFail($request->id);
        $offers->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' عرض الكير اتعدل');   }
        else
          { \Session::flash('flash_message','Care offer has updated');  }

        return redirect('CareOffer');
    }

    public function showORhide($id)
    {
        $offers = CareOffer::findOrFail($id);
        if ($offers->status)
        {
            $offers->update([ 'status' => 0 ]);
            if( \Session::get('lang') == 'ar' )
              { \Session::flash('flash_message','عرض الكير لنيظهر للعلن ');  }
            else
              { \Session::flash('flash_message','Care offer is not active ');  }
        }
        else {
          $offers->update([ 'status' => 1 ]);
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message','عرض الكير سيظهر للعلن ');  }
          else
            { \Session::flash('flash_message','Care offer is active ');  }
        }
        return redirect('CareOffer');
    }


    public function destroy($id)
    {
        $offers = CareOffer::findOrFail($id);
        $offers->delete();
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' عرض الكير اتمسحت');  }
        else
          { \Session::flash('flash_message','Care offer has deleted ');  }

        return redirect('CareOffer');
    }
}
