<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Area;
use App\Country;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
       $countries = '';
        if (\Session::get('lang') == 'ar')
        {
            $countries = Country::orderBy('name_en')->pluck('name_ar','id');
            $countries = [ ''=>' اختار محافظة ' ] + collect($countries)->toArray();
        }
        else {
            $countries = Country::orderBy('name_en')->pluck('name_en','id');
            $countries = [ ''=>' choose governorate ' ] + collect($countries)->toArray();
        }
        return view('Area.index',compact('countries'));
    }

    //----api--
    public function get_governora_by_country_id_pluck($country_id)
    {
        if (\Session::get('lang') == 'ar')
        {
           $governorates = \App\Governorate::where('country_id',$country_id)->orderBy('name_en')->get();
        }
        else {
          $governorates = \App\Governorate::where('country_id',$country_id)->orderBy('name_en')->get();
        }

        $data = array(); $temp = array();

          if( \Session::get('lang') == 'ar' )
          {
                foreach ($governorates as $col)
                {
                      $temp['text'] = $col->name_ar;
                      $temp['id'] = $col->id;
                      $data[]  = $temp;
                }
          }
          else
          {
                foreach ($governorates as $col)
                {
                      $temp['text'] = $col->name_en;
                      $temp['id'] = $col->id;
                      $data[]  = $temp;
                }
          }
          if( $governorates->isEmpty() )
            { return null; }
          else
          { return response()->json($data) ; }
    }

    //--api----
    public function get_area_by_id($governorate_id)
    {
        $Area = Area::where('governorate_id',$governorate_id)->orderBy('name_en')->get();
        return $Area;
    }

    public function store(Request $request)
    { 
        for ($i=0; $i < count($request->name_en) ; $i++)
        {
              $insert = [
                'governorate_id' => $request->governorate_id,
                'name_en' => $request->name_en[$i],
                'name_ar' => $request->name_ar[$i],
              ];
            if ($request->status[$i]=='on')
            {
                $insert['status'] = 1;
            }
            else {
              $insert['status'] = 0;
            }
            if ( isset($request->id[$i]) )
            {
                $Area = Area::whereId($request->id[$i])->update($insert);
            }
            else
            {
                $Area = Area::create($insert);
            }
        }//End for
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' المناطق اتعدلت ');   }
        else
          { \Session::flash('flash_message','Areas has updated');  }

        return redirect('Area');
    }

}
