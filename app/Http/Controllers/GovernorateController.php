<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Governorate;
use App\Country;

class GovernorateController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }
    public function index()
    {
        $countries = '';
        if (\Session::get('lang') == 'ar')
        {
            $countries = Country::orderBy('name_en')->pluck('name_ar','id');
            $countries = [ ''=>' اختار مدينة ' ] + collect($countries)->toArray();
        }
        else {
            $countries = Country::orderBy('name_en')->pluck('name_en','id');
            $countries = [ ''=>' choose country ' ] + collect($countries)->toArray();
        }
        return view('Governorate.index',compact('countries'));
    }

    //----api--
    public function get_governora_by_country_id($country_id)
    {
        $governorates = Governorate::where('country_id',$country_id)->orderBy('name_en')->get();
        return $governorates;
    }



    public function store(Request $request)
    {
         // $governorates = Governorate::where('country_id',$request->country_id)->whereNotIn('id',$request->id)->delete();
         for ($i=0; $i < count($request->name_en) ; $i++)
         {
              $insert = [
                   'name_ar' => $request->name_ar[$i],
                   'name_en' => $request->name_en[$i],
                   'country_id' => $request->country_id,
              ];
               if ($request->status[$i]=='on')
               {
                   $insert['status'] = 1;
               }
               else {
                 $insert['status'] = 0;
               }
              if (isset($request->id[$i]))
              {
                 $governorate = Governorate::whereId($request->id[$i])->update($insert);
              }
              else {
                $governorate = Governorate::create($insert);
              }
         }
         if( \Session::get('lang') == 'ar' )
           { \Session::flash('flash_message',' المحافظات اتعدلت ');   }
         else
           { \Session::flash('flash_message','governorates has updated');  }

         return redirect('Governorate');
    }

    public function get_by_country_id($country_id)
    {
        $governorate = Governorate::where('country_id',$country_id)->orderBy('name_en')->get();
        return $governorate;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
