<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareSpecialization;

class CareSpecializationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $CareSpecializations = CareSpecialization::latest()->paginate();
        return view('CareSpecialization.index',compact('CareSpecializations'));
    }

    public function search($val)
    {
         $CareSpecializations = CareSpecialization::where('name_en','like','%'.$val.'%')->orWhere('name_ar','like','%'.$val.'%')
                                                ->orderBy('name_en')->paginate();
         return view('CareSpecialization.index',compact('CareSpecializations','val'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required'
        ]);
        $CareSpecialization = CareSpecialization::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$CareSpecialization->name_ar.' تخصص');   }
        else
          { \Session::flash('flash_message','Specialization '.$CareSpecialization->name_en.' has added');  }

        return redirect('CareSpecialization');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required'
        ]);
        $CareSpecialization = CareSpecialization::findOrFail($request->id);
        $CareSpecialization->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتعدل '.$CareSpecialization->name_ar.' تخصص');   }
        else
          { \Session::flash('flash_message','Specialization '.$CareSpecialization->name_en.' has updated');  }

        return redirect('CareSpecialization');
    }

    public function showORhide($id)
    {
        $CareSpecialization = CareSpecialization::findOrFail($id);
           if( $CareSpecialization->status )
           {
                 $CareSpecialization->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' التخصص الفرعي للكير '.$CareSpecialization->name.' كير ');   }
                else
                  { \Session::flash('flash_message','Care Sub Service '.$CareSpecialization->name.' is not public any more');  }
           }
           else
           {
                $CareSpecialization->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$CareSpecialization->name.' التخصص الفرعي للكير ');   }
                else
                  { \Session::flash('flash_message','Care Sub Service'.$CareSpecialization->name.' now is public ');  }
           }

           return back();
    }

}
