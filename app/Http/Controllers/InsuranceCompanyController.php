<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InsuranceCompany;

class InsuranceCompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $InsuranceCompany = InsuranceCompany::orderBy('name_en')->paginate();
        return view('InsuranceCompany.index',compact('InsuranceCompany'));
    }

    public function search($val)
    {
        $InsuranceCompany = InsuranceCompany::where('name_en','like','%'.$val.'%')->orWhere('name_ar','like','%'.$val.'%')
                                            ->orderBy('name_en')->paginate();
        return view('InsuranceCompany.index',compact('InsuranceCompany','val'));
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
        ]);
        $InsuranceCompany = InsuranceCompany::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$InsuranceCompany->name.' شركة التامينات ');   }
        else
          { \Session::flash('flash_message','Insurance Company '.$InsuranceCompany->name.' has added');  }

        return redirect('InsuranceCompany');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'id' => 'required',
            'name_en' => 'required',
            'name_ar' => 'required',
        ]);
        $InsuranceCompany = InsuranceCompany::findOrFail($request->id);
        $InsuranceCompany->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتعدل '.$InsuranceCompany->name_ar.' شركة التامينات ');   }
        else
          { \Session::flash('flash_message','Insurance Company '.$InsuranceCompany->name_en.' has updated');  }

        return redirect('InsuranceCompany');
    }


    public function showORhide($id)
    {
        $InsuranceCompany = InsuranceCompany::findOrFail($id);
           if( $InsuranceCompany->status )
           {
                 $InsuranceCompany->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' لن تظهر اللي العلن '.$InsuranceCompany->name.' شركة تامين ');   }
                else
                  { \Session::flash('flash_message',' Insurance Company '.$InsuranceCompany->name.' is not public any more');  }
           }
           else
           {
                $InsuranceCompany->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$InsuranceCompany->name.' التخصص الفرعي للكير ');   }
                else
                  { \Session::flash('flash_message',' Insurance Company '.$InsuranceCompany->name.' now is public ');  }
           }

           return back();
    }

}
