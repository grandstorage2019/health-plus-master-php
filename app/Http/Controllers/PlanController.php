<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plan;

class PlanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $plans = Plan::latest()->paginate();
        return view('Plan.index',compact('plans'));
    }

    public function search($val)
    {
        $plans = Plan::where('name_ar','like','%'.$val.'%')->orWhere('name_en','like','%'.$val.'%')->orWhere('details_en','like','%'.$val.'%')
                     ->orWhere('details_ar','like','%'.$val.'%')->latest()->paginate();
        return view('Plan.index',compact('plans','val'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'details_en' => 'required',
            'details_ar' => 'required'
        ]);
        $plans = Plan::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$plans->name_ar.' الباقة ');  }
        else
          { \Session::flash('flash_message','Plan '.$plans->name_en.' has added');  }

        return redirect('Plan');
    }

    public function update(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'details_en' => 'required',
            'details_ar' => 'required'
        ]);
        $plans = Plan::findOrFail($request->id);
        $plans->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$plans->name_ar.' الباقة ');  }
        else
          { \Session::flash('flash_message','Plan '.$plans->name_en.' has added');  }

        return redirect('Plan');
    }

    public function showORhide($id)
    {
        $Plan = Plan::findOrFail($id);
           if( $Plan->status )
           {
                 $Plan->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' لن تظهر للعامة '.$Plan->name.' باقة ');   }
                else
                  { \Session::flash('flash_message',' plan '.$Plan->name.' is not public any more');  }
           }
           else
           {
                $Plan->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$Plan->name.' باقة ');   }
                else
                  { \Session::flash('flash_message','plan '.$Plan->name.' now is public ');  }
           }

           return back();
    }

}
