<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CareAdvertising;

class CareAdvertisingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $CareAdvertising = CareAdvertising::latest()->paginate();
        return view('CareAdvertising.index',compact('CareAdvertising') );
    }

    public function search($val)
    {
         $CareAdvertising = CareAdvertising::where('title_en','like','%'.$val.'%')->orWhere('title_ar','like','%'.$val.'%')
                                                ->orderBy('title_en')->paginate();
         return view('CareAdvertising.index',compact('CareAdvertising','val'));
    }


    public function store(Request $request)
    {
          $data = $request->validate([
              'title_en' => 'required',
              'title_ar' => 'required',
              'image' => 'required',
              'link' => 'required'
          ]);
                $extension = $request->image->getClientOriginalExtension(); //get img extention
                $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                $destinationPath = public_path('images/care_adverting');
                $request->image->move($destinationPath, $fileName); // uploading file to given path
                $data['image'] = $fileName;
          if ( $request->has('is_promoted') )
          {
              $data['is_promoted'] = 1;
          }
          $data['details_en'] = $request->title_en ;
          $data['details_ar'] = $request->title_ar ;
          $CareAdvertising = CareAdvertising::create($data);
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' اضاف '.$CareAdvertising->name_ar.' اعلان ');   }
          else
            { \Session::flash('flash_message',' Advertising '.$CareAdvertising->name_en.' has added');  }

          return redirect('CareAdvertising');
    }

    public function update(Request $request)
    {
          $data = $request->validate([
              'id' => 'required',
              'title_en' => 'required',
              'title_ar' => 'required',
              'link' => 'required'
          ]);
          $CareAdvertising = CareAdvertising::findOrFail($request->id);
          $data['details_en'] = $request->title_en ;
          $data['details_ar'] = $request->title_ar ;
          if (isset($request->image))
          {
              $extension = $request->image->getClientOriginalExtension(); //get img extention
              $fileName  = rand(11111,99999).'.'.$extension; // renameing image
              $destinationPath = public_path('images/care_adverting');
              $request->image->move($destinationPath, $fileName); // uploading file to given path
              $data['image'] = $fileName;
          }
          if ( $request->has('is_promoted') )
          {
              $data['is_promoted'] = 1;
          }
          else {
            $data['is_promoted'] = 0;
          }
              // dd($data);
          $CareAdvertising->update($data);
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' اتعدل '.$CareAdvertising->name_ar.' اعلان ');  }
          else
            { \Session::flash('flash_message',' Advertising '.$CareAdvertising->name_en.' has updated');  }

          return redirect('CareAdvertising');
    }


    public function destroy($id)
    {
        $CareAdvertising = CareAdvertising::findOrFail($id);
        $CareAdvertising->delete();
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتمسح '.$CareAdvertising->name_ar.' اعلان ');  }
        else
          { \Session::flash('flash_message',' Advertising '.$CareAdvertising->name_en.' has deleted ');  }

        return redirect('CareAdvertising');
    }
}
