<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Care;
use App\CarePicture;
use App\CareService;

class CareController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $Cares = Care::latest()->paginate();
        return view('Care.index',compact('Cares'));
    }
    public function search($val)
    {
        $Cares = Care::where('name_en','like','%'.$val.'%')->orWhere('name_ar','like','%'.$val.'%')->orWhere('phone','like','%'.$val.'%')
          ->orWhere('about_en','like','%'.$val.'%')->orWhere('about_ar','like','%'.$val.'%')->latest()->paginate();
        return view('Care.index',compact('Cares','val'));
    }

    public function create()
    {
          $lang = \Session::get('lang') ;
          $InsuranceCompany = \App\InsuranceCompany::latest()->pluck('name_'.$lang.' as name','id');
          if (\Session::get('lang') == 'ar' )
          {
              $specializations = \App\CareSpecialization::latest()->pluck('name_ar','id');
              $areas = \App\Area::latest()->pluck('name_ar','id');
              //-----------
              $specializations = [ ''=>' اختار التخصص ' ] + collect($specializations)->toArray();
              $areas = [ ''=>' اختار منطقة ' ] + collect($areas)->toArray();
          }
          else
          {
              $specializations = \App\CareSpecialization::latest()->pluck('name_en','id');
              $areas = \App\Area::latest()->pluck('name_en','id');
              //-----------
              $specializations = [ ''=>' choose specialization ' ] + collect($specializations)->toArray();
              $areas = [ ''=>' choose area ' ] + collect($areas)->toArray();
          }
          return view('Care.create',compact('specializations','areas','InsuranceCompany'));
    }

    public function store(Request $request)
    {
          $data = $request->validate([
              'name_en' => 'required',
              'name_ar' => 'required',
              'area_id' => 'required',
              'specialization_id' => 'required',
              'phone' => 'required',
              'email' => 'required',
              // 'mobile' => 'required',
              'from_time' => 'required',
              'to_time' => 'required',
              'about_ar' => 'required',
              'about_en' => 'required',
              'location_comment_ar' => 'required',
              'location_comment_en' => 'required',
              'facebook' => '',
              'instagram' => '',
              'twitter' => '',
              'snapchat' => '',
              'insurance_company_id' => '',
              'lat' => '',
              'lang' => '',
          ]);
          if ($request->logo)
          {
              $extension = $request->logo->getClientOriginalExtension(); //get img extention
              $fileName  = rand(11111,99999).'.'.$extension; // renameing image
              $destinationPath = public_path('images/care');
              $request->logo->move($destinationPath, $fileName); // uploading file to given path
              $data['logo'] = $fileName;
          }
          $Cares = Care::create($data);
          for ($i=0; $i < count($request->services_name_en); $i++)
          {
              $extension = $request->services_image[$i]->getClientOriginalExtension(); //get img extention
              $fileName  = rand(11111,99999).'.'.$extension; // renameing image
              $destinationPath = public_path('images/care_services');
              $request->services_image[$i]->move($destinationPath, $fileName); // uploading file to given path
              $data['logo'] = $fileName;

              \App\CareService::create([
                  'care_id' => $Cares->id ,
                  'logo' => $fileName ,
                  'name_en' => $request->services_name_en[$i] ,
                  'name_ar' => $request->services_name_ar[$i]
              ]);
          }

          for ($i=0; $i < count($request->Care_images) ; $i++)
          {
               if (isset($request->Care_images[$i]))
               {
                   $extension = $request->Care_images[$i]->getClientOriginalExtension(); //get img extention
                   $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                   $destinationPath = public_path('images/health_cares');
                   $request->Care_images[$i]->move($destinationPath, $fileName); // uploading file to given path
                   $data['logo'] = $fileName;

                   \App\CarePicture::create([
                        'care_id' => $Cares->id ,
                        'image' => $fileName
                   ]);
               }
          }
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message','  كير اضاف ');   }
          else
            { \Session::flash('flash_message','Care  has added');  }

          return redirect('Care');
    }


    public function show($id)
    {
        $care = Care::findOrFail($id);
        return view('Care.show',compact('care'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $care = Care::findOrFail($id);
          $lang = \Session::get('lang') ;
          $InsuranceCompany = \App\InsuranceCompany::latest()->pluck('name_'.$lang.' as name','id');
        if (\Session::get('lang') == 'ar' )
        {
            $specializations = \App\CareSpecialization::latest()->pluck('name_ar','id');
            $areas = \App\Area::latest()->pluck('name_ar','id');
        }
        else
        {
            $specializations = \App\CareSpecialization::latest()->pluck('name_en','id');
            $areas = \App\Area::latest()->pluck('name_en','id');
        }
        $care_services = $care->get_service;
        $care_pictures = $care->get_pictures;
        return view('Care.edit',compact('specializations','areas','care','care_services','care_pictures','InsuranceCompany'));

    }


    public function update(Request $request, $id)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'area_id' => 'required',
            'specialization_id' => 'required',
            'phone' => 'required',
            'email' => 'required',
            // 'mobile' => 'required',
            'from_time' => 'required',
            'to_time' => 'required',
            'about_ar' => 'required',
            'about_en' => 'required',
            'location_comment_ar' => 'required',
            'location_comment_en' => 'required',
            'facebook' => '',
            'instagram' => '',
            'twitter' => '',
            'snapchat' => '',
            'insurance_company_id' => '',
            'lat' => '',
            'lang' => '',
        ]);
        $Cares = Care::findOrFail($id);
        if ($request->logo)
        {
            $extension = $request->logo->getClientOriginalExtension(); //get img extention
            $fileName  = rand(11111,99999).'.'.$extension; // renameing image
            $destinationPath = public_path('images/care');
            $request->logo->move($destinationPath, $fileName); // uploading file to given path
            $data['logo'] = $fileName;
        }
        $Cares->update($data);
        // $request->services_id = array_filter($request->services_id);
        // $request->images_id = array_filter($request->images_id);
        for ($i=0; $i < count($request->services_count); $i++)
        {
            $the_data = [
              'care_id' => $Cares->id ,
              'name_en' => $request->services_name_en[$i] ,
              'name_ar' => $request->services_name_ar[$i]
            ];
            if (isset($request->services_image[$i]))
            {
                $extension = $request->services_image[$i]->getClientOriginalExtension(); //get img extention
                $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                $destinationPath = public_path('images/care_services');
                $request->services_image[$i]->move($destinationPath, $fileName); // uploading file to given path
                $the_data['logo'] = $fileName;
            }
            if ( $request->services_name_en[$i])
            {
                if (isset($request->services_id[$i]))
                {
                  \App\CareService::whereId($request->services_id[$i])->update($the_data);
                }
                else {
                    \App\CareService::create($the_data);
                }
            }

        }

        if ($request->images_id) {
          \App\CarePicture::where('care_id',$Cares->id)->whereNotIn('id',$request->images_id)->delete();
        }
        else {
          \App\CarePicture::where('care_id',$Cares->id)->delete();
        }

        for ($i=0; $i < count($request->images_count) ; $i++)
        {
             if (isset($request->Care_images[$i]))
             {
                 $extension = $request->Care_images[$i]->getClientOriginalExtension(); //get img extention
                 $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                 $destinationPath = public_path('images/health_cares');
                 $request->Care_images[$i]->move($destinationPath, $fileName); // uploading file to given path
                 $the_data = [
                   'care_id' => $Cares->id ,
                   'image' => $fileName,
                 ];
                 if (isset($request->images_id[$i]))
                 {
                    \App\CarePicture::whereId($request->images_id[$i])->update($the_data);
                 }
                 else
                 {
                   \App\CarePicture::create($the_data);
                 }
             }
        }
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' كير اتعدل ');   }
        else
          { \Session::flash('flash_message','Care  has updated');  }

        return redirect('Care');
    }

    public function showORhide($id)
    {
        $Care = Care::findOrFail($id);
           if( $Care->status )
           {
                 $Care->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' لن يظهر للعامة '.$Care->name.' كير ');   }
                else
                  { \Session::flash('flash_message','Care '.$Care->name.' is not public any more');  }
           }
           else
           {
                $Care->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$Care->name.' كير ');   }
                else
                  { \Session::flash('flash_message','Care '.$Care->name.' now is public ');  }
           }

           return back();
    }

    public function destroy($id)
    {
        $Care = Care::findOrFail($id);
              $services_ids = array();
              foreach ($Care->get_service as $service)
              {   array_push($services_ids,$service->id );   }
        $Care->get_pictures()->delete();
        \App\CareSubService::whereIn('service_id', $services_ids )->delete();
        $Care->get_service()->delete();
        $Care->delete();
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتمسح '.$Care->name.' كير ');   }
        else
          { \Session::flash('flash_message','Care '.$Care->name.' is deleted ');  }
       return back();
    }


}
