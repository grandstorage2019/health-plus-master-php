<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use App\Doctor;
use App\DocLicense;
use Illuminate\Support\Facades\DB;

class DoctorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        $doctors = Doctor::orderBy('fname_en')->paginate();
        return view('Doctor.index',compact('doctors'));
    }

    public function search($val)
    {
        $doctors = Doctor::whereRaw("CONCAT(fname_en ,' ',lname_en) LIKE ? ",['%'.$val.'%'])
                        ->orWhereRaw("CONCAT(fname_ar ,' ',lname_ar) LIKE ? ",['%'.$val.'%'])
                        ->orWhere('email','like','%'.$val.'%')->orWhere('username','like','%'.$val.'%')
                        ->orWhere('phone','like','%'.$val.'%')->orWhere('birth_day','like','%'.$val.'%')
                        ->orWhere('title_en','like','%'.$val.'%')->orWhere('title_ar','like','%'.$val.'%')
                        ->orWhere('price','like','%'.$val.'%')->orWhere('id',$val)->orderBy('fname_en')->paginate();
        return view('Doctor.index',compact('doctors','val'));
    }

    public function create()
    {
        $lang = \Session::get('lang') ;
        $InsuranceCompany = \App\InsuranceCompany::latest()->pluck('name_'.$lang.' as name','id');
        if (\Session::get('lang') == 'ar' )
        {
            $plans = \App\Plan::latest()->pluck('name_ar','id');
            $specializations = \App\DocSpecialization::latest()->pluck('name_ar','id');
            $areas = \App\Area::latest()->pluck('name_ar','id');
            //-----------
            $plans = [ ''=>' اختار الباقة' ] + collect($plans)->toArray();
            $specializations = [ ''=>' اختار التخصص ' ] + collect($specializations)->toArray();
            $areas = [ ''=>' اختار منطقة ' ] + collect($areas)->toArray();
            $allServices=Service::all()->pluck('name_ar','id');

        }
        else
        {
            $plans = \App\Plan::latest()->pluck('name_en','id');
            $specializations = \App\DocSpecialization::latest()->pluck('name_en','id');
            $areas = \App\Area::latest()->pluck('name_en','id');
            //-----------
            $plans = [ ''=>' choose plan ' ] + collect($plans)->toArray();
            $specializations = [ ''=>' choose specialization ' ] + collect($specializations)->toArray();
            $areas = [ ''=>' choose area ' ] + collect($areas)->toArray();
            $allServices=Service::all()->pluck('name_en','id');
        }

        return view('Doctor.create',compact('plans','specializations','InsuranceCompany','areas','allServices'));
    }


    public function store(Request $request)
    {
        $data = $request->validate([
            'fname_en' => 'required',
            'fname_ar' => 'required',
            'lname_en' => 'required',
            'lname_ar' => 'required',
            'price' => 'required',
            'username' => 'required|unique:doctors',
            'email' => 'required|unique:doctors',
            'secretary_email' => '',
            'password' => 'required',
            'phone' => 'required',
            'birth_day' => 'required',
            'gender' => 'required',
            'title_ar' => 'required',
            'title_en' => 'required',
            'about_ar' => 'required',
            'about_en' => 'required',
            'is_accapted_by_admin' => 'required',
            'plan_id' => '',
            'specialization_id' => '',
            'level_ar' => '',
            'level_en' => ''
        ]);
        if ($request->profile_image)
        {
            $extension = $request->profile_image->getClientOriginalExtension(); //get img extention
            $fileName  = rand(11111,99999).'.'.$extension; // renameing image
            $destinationPath = public_path('images/doctor_profile');
            $request->profile_image->move($destinationPath, $fileName); // uploading file to given path
            $data['profile_image'] = $fileName;
        }

        $data['password'] = md5($request->password);
        $doctors = Doctor::create($data);
        $doctors->get_Service()->attach($request->services);
        $doctors->get_InsuranceCompany()->attach($request->InsuranceCompanies);

        \App\ClicnicAddress::create([
            'doctor_id' => $doctors->id ,
            'clinic_name_en' => $request->clinic_name_en ,
            'clinic_name_ar' => $request->clinic_name_ar ,
            'clinic_number' => $request->Clicnic_number ,
            'area_id' => $request->Clicnic_area_id ,
            'street_name_ar' => $request->Clicnic_streetName_ar ,
            'street_name_en' => $request->Clicnic_streetName_en ,
            'building_number_ar' => $request->Clicnic_building_number_ar ,
            'building_number_en' => $request->Clicnic_building_number_en ,
            'floor_ar' => $request->Clicnic_floor_ar ,
            'floor_en' => $request->Clicnic_floor_en ,
            'apartment_en' => $request->Clicnic_apartment_en ,
            'apartment_ar' => $request->Clicnic_apartment_ar,
            'landmark_en' => $request->Clicnic_landmark_en ,
            'landmark_ar' => $request->Clicnic_landmark_ar,
            'block_en' => $request->block_en,
            'block_ar' => $request->block_ar,
            'lat' => $request->Clicnic_lat,
            'lang' => $request->Clicnic_lang,
        ]);

        for ($i=0; $i < count($request->license) ; $i++)
        {
             if (isset($request->license[$i]))
             {
                  $fileName  = rand(11111,99999).'.'.$request->license[$i]->getClientOriginalExtension(); // renameing image
                  $destinationPath = public_path('images/license');
                  $request->license[$i]->move($destinationPath, $fileName); // uploading file to given path
                  DocLicense::create([
                      'doctor_id' => $doctors->id ,
                      'image'=> $fileName
                  ]);
             }
        }

        for ($i=0; $i < count($request->Clicnic_images) ; $i++)
        {
            if (isset($request->Clicnic_images[$i]))
            {
                $extension = $request->Clicnic_images[$i]->getClientOriginalExtension(); //get img extention
                $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                $destinationPath = public_path('images/Clinic');
                $request->Clicnic_images[$i]->move($destinationPath, $fileName); // uploading file to given path

                \App\ClinicPicture::create([
                    'doctor_id' => $doctors->id ,
                    'image' => $fileName
                ]);
            }
        }
        //-------available_date
        $available_date = array();
        if ( $request->available_date_type == 'specific' )
        {
            $available_date['from_date'] = $request->from_date;
            $available_date['to_date'] = $request->to_date;
        }
        else {
           $available_date['is_life_time'] = 1;
        }

        if ($request->Sat_from_time && $request->Sat_to_time)
        {
             $available_date['Sat_from_time'] = $request->Sat_from_time;
             $available_date['Sat_to_time'] = $request->Sat_to_time;
             $available_date['Sat_type'] = $request->Sat_type;
             $available_date['Sat_wating_time'] = $request->Sat_wating_time;
             $available_date['Sat_num_resrvation'] = $request->Sat_num_resrvation;
        }
        if ($request->Sun_from_time && $request->Sun_to_time)
        {
             $available_date['Sun_from_time'] = $request->Sun_from_time;
             $available_date['Sun_to_time'] = $request->Sun_to_time;
             $available_date['Sun_type'] = $request->Sun_type;
             $available_date['Sun_wating_time'] = $request->Sun_wating_time;
             $available_date['Sun_num_resrvation'] = $request->Sun_num_resrvation;
        }
        if ($request->Mon_from_time && $request->Mon_to_time)
        {
             $available_date['Mon_from_time'] = $request->Mon_from_time;
             $available_date['Mon_to_time'] = $request->Mon_to_time;
             $available_date['Mon_type'] = $request->Mon_type;
             $available_date['Mon_wating_time'] = $request->Mon_wating_time;
             $available_date['Mon_num_resrvation'] = $request->Mon_num_resrvation;
        }
        if ($request->Tue_from_time && $request->Tue_to_time)
        {
             $available_date['Tue_from_time'] = $request->Tue_from_time;
             $available_date['Tue_to_time'] = $request->Tue_to_time;
             $available_date['Tue_type'] = $request->Tue_type;
             $available_date['Tue_wating_time'] = $request->Tue_wating_time;
             $available_date['Tue_num_resrvation'] = $request->Tue_num_resrvation;
        }
        if ($request->Thu_from_time && $request->Thu_to_time)
        {
             $available_date['Thu_from_time'] = $request->Thu_from_time;
             $available_date['Thu_to_time'] = $request->Thu_to_time;
             $available_date['Thu_type'] = $request->Thu_type;
             $available_date['Thu_wating_time'] = $request->Thu_wating_time;
             $available_date['Thu_num_resrvation'] = $request->Thu_num_resrvation;
        }
        if ($request->Wed_from_time && $request->Wed_to_time)
        {
             $available_date['Wed_from_time'] = $request->Wed_from_time;
             $available_date['Wed_to_time'] = $request->Wed_to_time;
             $available_date['Wed_type'] = $request->Wed_type;
             $available_date['Wed_wating_time'] = $request->Wed_wating_time;
             $available_date['Wed_num_resrvation'] = $request->Wed_num_resrvation;
        }
        if ($request->Fri_from_time && $request->Fri_to_time)
        {
             $available_date['Fri_from_time'] = $request->Fri_from_time;
             $available_date['Fri_to_time'] = $request->Fri_to_time;
             $available_date['Fri_type'] = $request->Fri_type;
             $available_date['Fri_wating_time'] = $request->Fri_wating_time;
             $available_date['Fri_num_resrvation'] = $request->Fri_num_resrvation;
        }
        $available_date['doctor_id'] = $doctors->id ;
        \App\DoctorAvailableDate::create($available_date);

        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message','  الدكتور اضاف ');   }
        else
          { \Session::flash('flash_message','Doctor  has added');  }

        // return back();
        return redirect('Doctor');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $doctor = Doctor::findOrFail($id);
        $get_Licenses = $doctor->get_Licenses;
        return view('Doctor.show',compact('doctor','get_Licenses'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $doctor = Doctor::findOrFail($id);
        $lang = \Session::get('lang') ;
        $InsuranceCompany = \App\InsuranceCompany::latest()->pluck('name_'.$lang.' as name','id');
        if (\Session::get('lang') == 'ar' )
        {
            $plans = \App\Plan::latest()->pluck('name_ar','id');
            $specializations = \App\DocSpecialization::latest()->pluck('name_ar','id');
            $areas = \App\Area::latest()->pluck('name_ar','id');
            //-----------
            $plans = [ ''=>' اختار الباقة' ] + collect($plans)->toArray();
            $specializations = [ ''=>' اختار التخصص ' ] + collect($specializations)->toArray();
            $areas = [ ''=>' اختار منطقة ' ] + collect($areas)->toArray();
            $allServices=Service::all()->pluck('name_ar','id');

        }
        else
        {
            $plans = \App\Plan::latest()->pluck('name_en','id');
            $specializations = \App\DocSpecialization::latest()->pluck('name_en','id');
            $areas = \App\Area::latest()->pluck('name_en','id');
            //-----------
            $plans = [ ''=>' choose plan ' ] + collect($plans)->toArray();
            $specializations = [ ''=>' choose specialization ' ] + collect($specializations)->toArray();
            $areas = [ ''=>' choose area ' ] + collect($areas)->toArray();
            $allServices=Service::all()->pluck('name_en','id');

        }

        $available_date = '';

        if (isset($doctor->get_available_date->is_life_time))
        {
          if( $doctor->get_available_date->is_life_time == 1 )
            { $available_date = 'is_life_time'; }
        }
        if (isset($doctor->get_available_date->from_date) && isset($doctor->get_available_date->to_date) )
        {
           if ( $doctor->get_available_date->from_date && $doctor->get_available_date->to_date )
            { $available_date = 'specific'; }
        }

        if (\Session::get('lang') == 'ar')
        {
            $sub_specialization = \App\DocSubSpecialization::where('specialization_id',$doctor->specialization_id)->select(['text_ar as name','id'])->get() ;
        }
        else {
          $sub_specialization = \App\DocSubSpecialization::where('specialization_id',$doctor->specialization_id)->select(['text_en as name','id'])->get() ;
        }
        $sub_specialization = json_encode($sub_specialization);
        $doctor->get_Service();
        $oneDoctorServicesId=DB::table('doctor_services')->where('doctor_id',$doctor->id)->pluck('service_id')->toArray();
        $doctor_selected_insurance_companies_ids = $doctor->get_insurance_companies_ids();

        $days_array = [ 'Sat','Sun', 'Mon', 'Tue', 'Thu' , 'Wed' , 'Fri' ];
        $get_Licenses = $doctor->get_Licenses;
        return view('Doctor.edit',compact('doctor','specializations','InsuranceCompany','areas','plans', 'available_date', 'get_Licenses',
                                'sub_specialization','oneDoctorServicesId','doctor_selected_insurance_companies_ids','days_array','allServices'));
    }

    public function update(Request $request, $id)
    {
          $data = $request->validate([
              'fname_en' => 'required',
              'fname_ar' => 'required',
              'lname_en' => 'required',
              'lname_ar' => 'required',
              'price' => 'required',
              'username' => 'required|unique:doctors,id,'.$id,
              'email' => 'required|unique:doctors,id,'.$id,
              'secretary_email' => '',
              // 'password' => '',
              'phone' => 'required',
              'birth_day' => 'required',
              'gender' => 'required',
              'title_ar' => 'required',
              'title_en' => 'required',
              'about_ar' => 'required',
              'about_en' => 'required',
              'is_accapted_by_admin' => 'required',
              'plan_id' => '',
              'specialization_id' => '',
              'level_ar' => '',
              'level_en' => ''
          ]);
          $doctors = Doctor::findOrFail($id);
          if ($request->profile_image)
          {
              $extension = $request->profile_image->getClientOriginalExtension(); //get img extention
              $fileName  = rand(11111,99999).'.'.$extension; // renameing image
              $destinationPath = public_path('images/doctor_profile');
              $request->profile_image->move($destinationPath, $fileName); // uploading file to given path
              $data['profile_image'] = $fileName;
          }
          if ($request->password)
          {
            $data['password'] = md5($request->password);
          }
          $doctors->update($data);
        $doctors->get_Service()->sync($request->services);

        $doctors->get_InsuranceCompany()->sync($request->InsuranceCompanies);

          \App\ClicnicAddress::where('doctor_id',$doctors->id)->update([
              'clinic_name_en' => $request->clinic_name_en ,
              'clinic_name_ar' => $request->clinic_name_ar ,
              'clinic_number' => $request->Clicnic_number ,
              'area_id' => $request->Clicnic_area_id ,
              'street_name_ar' => $request->Clicnic_streetName_ar ,
              'street_name_en' => $request->Clicnic_streetName_en ,
              'building_number_ar' => $request->Clicnic_building_number_ar ,
              'building_number_en' => $request->Clicnic_building_number_en ,
              'floor_ar' => $request->Clicnic_floor_ar ,
              'floor_en' => $request->Clicnic_floor_en ,
              'apartment_en' => $request->Clicnic_apartment_en ,
              'apartment_ar' => $request->Clicnic_apartment_ar ,
              'landmark_en' => $request->Clicnic_landmark_en ,
              'landmark_ar' => $request->Clicnic_landmark_ar,
              'block_en' => $request->block_en,
              'block_ar' => $request->block_ar,
              'lat' => $request->Clicnic_lat,
              'lang' => $request->Clicnic_lang,
          ]);
          if ($request->Clicnic_image_id)
          {
              $request->Clicnic_image_id =  array_filter($request->Clicnic_image_id);
              \App\ClinicPicture::where('doctor_id',$doctors->id)->whereNotIn('id',$request->Clicnic_image_id)->delete();
          }

          //-----START license-------
          if ($request->license_temp_ids) {
              DocLicense::where('doctor_id',$doctors->id)->whereNotIn('id',$request->license_temp_ids)->delete();
          }
          else {
            DocLicense::where('doctor_id',$doctors->id)->delete();
          }
          for ($i=0; $i < count($request->license_count) ; $i++)
          {
               if (isset($request->license[$i]))
               {
                    $fileName  = rand(11111,99999).'.'.$request->license[$i]->getClientOriginalExtension(); // renameing image
                    $destinationPath = public_path('images/license');
                    $request->license[$i]->move($destinationPath, $fileName); // uploading file to given path
                    if (isset($request->license_temp_ids[$i]))
                    {
                         DocLicense::whereId($request->license_temp_ids[$i])->update([ 'image'=> $fileName ]);
                    }
                    else {
                        DocLicense::create([
                            'doctor_id' => $doctors->id ,
                            'image'=> $fileName
                        ]);
                    }
               }
          }//End for--
          //-----END license-------

          for ($i=0; $i < count($request->Clicnic_image_count) ; $i++)
          {
              if (isset($request->Clicnic_images[$i]))
              {
                  $extension = $request->Clicnic_images[$i]->getClientOriginalExtension(); //get img extention
                  $fileName  = rand(11111,99999).'.'.$extension; // renameing image
                  $destinationPath = public_path('images/Clinic');
                  $request->Clicnic_images[$i]->move($destinationPath, $fileName); // uploading file to given path

                  \App\ClinicPicture::create([
                      'doctor_id' => $doctors->id ,
                      'image' => $fileName
                  ]);
              }
          }   //dd($request->Clicnic_image_count);
          //-------available_date
          $available_date = array();
          if ( $request->available_date_type == 'specific' )
          {
              $available_date['from_date'] = $request->from_date;
              $available_date['to_date'] = $request->to_date;
              $available_date['is_life_time'] = 0;
          }
          else {
             $available_date['is_life_time'] = 1;
             $available_date['from_date'] = null;
             $available_date['to_date'] = null;
          }

          if ($request->Sat_from_time && $request->Sat_to_time)
          {
               $available_date['Sat_from_time'] = $request->Sat_from_time;
               $available_date['Sat_to_time'] = $request->Sat_to_time;
               $available_date['Sat_type'] = $request->Sat_type;
               $available_date['Sat_wating_time'] = $request->Sat_wating_time;
               $available_date['Sat_num_resrvation'] = $request->Sat_num_resrvation;
          }
          else {
            $available_date['Sat_from_time'] = null;
            $available_date['Sat_to_time'] = null;
            $available_date['Sat_type'] = null;
            $available_date['Sat_wating_time'] = null;
            $available_date['Sat_num_resrvation'] = null;
          }

          if ($request->Sun_from_time && $request->Sun_to_time)
          {
               $available_date['Sun_from_time'] = $request->Sun_from_time;
               $available_date['Sun_to_time'] = $request->Sun_to_time;
               $available_date['Sun_type'] = $request->Sun_type;
               $available_date['Sun_wating_time'] = $request->Sun_wating_time;
               $available_date['Sun_num_resrvation'] = $request->Sun_num_resrvation;
          }
          else {
            $available_date['Sun_from_time'] = null;
            $available_date['Sun_to_time'] = null;
            $available_date['Sun_type'] = null;
            $available_date['Sun_wating_time'] = null;
            $available_date['Sun_num_resrvation'] = null;
          }

          if ($request->Mon_from_time && $request->Mon_to_time)
          {
               $available_date['Mon_from_time'] = $request->Mon_from_time;
               $available_date['Mon_to_time'] = $request->Mon_to_time;
               $available_date['Mon_type'] = $request->Mon_type;
               $available_date['Mon_wating_time'] = $request->Mon_wating_time;
               $available_date['Mon_num_resrvation'] = $request->Mon_num_resrvation;
          }
          else {
            $available_date['Mon_from_time'] = null;
            $available_date['Mon_to_time'] = null;
            $available_date['Mon_type'] = null;
            $available_date['Mon_wating_time'] = null;
            $available_date['Mon_num_resrvation'] = null;
          }

          if ($request->Tue_from_time && $request->Tue_to_time)
          {
               $available_date['Tue_from_time'] = $request->Tue_from_time;
               $available_date['Tue_to_time'] = $request->Tue_to_time;
               $available_date['Tue_type'] = $request->Tue_type;
               $available_date['Tue_wating_time'] = $request->Tue_wating_time;
               $available_date['Tue_num_resrvation'] = $request->Tue_num_resrvation;
          }
          else {
            $available_date['Tue_from_time'] = null;
            $available_date['Tue_to_time'] = null;
            $available_date['Tue_type'] = null;
            $available_date['Tue_wating_time'] = null;
            $available_date['Tue_num_resrvation'] = null;
          }

          if ($request->Thu_from_time && $request->Thu_to_time)
          {
               $available_date['Thu_from_time'] = $request->Thu_from_time;
               $available_date['Thu_to_time'] = $request->Thu_to_time;
               $available_date['Thu_type'] = $request->Thu_type;
               $available_date['Thu_wating_time'] = $request->Thu_wating_time;
               $available_date['Thu_num_resrvation'] = $request->Thu_num_resrvation;
          }
          else {
            $available_date['Thu_from_time'] = null;
            $available_date['Thu_to_time'] = null;
            $available_date['Thu_type'] = null;
            $available_date['Thu_wating_time'] = null;
            $available_date['Thu_num_resrvation'] = null;
          }

          if ($request->Wed_from_time && $request->Wed_to_time)
          {
               $available_date['Wed_from_time'] = $request->Wed_from_time;
               $available_date['Wed_to_time'] = $request->Wed_to_time;
               $available_date['Wed_type'] = $request->Wed_type;
               $available_date['Wed_wating_time'] = $request->Wed_wating_time;
               $available_date['Wed_num_resrvation'] = $request->Wed_num_resrvation;
          }
          else {
            $available_date['Wed_from_time'] = null;
            $available_date['Wed_to_time'] = null;
            $available_date['Wed_type'] = null;
            $available_date['Wed_wating_time'] = null;
            $available_date['Wed_num_resrvation'] = null;
          }

          if ($request->Fri_from_time && $request->Fri_to_time)
          {
               $available_date['Fri_from_time'] = $request->Fri_from_time;
               $available_date['Fri_to_time'] = $request->Fri_to_time;
               $available_date['Fri_type'] = $request->Fri_type;
               $available_date['Fri_wating_time'] = $request->Fri_wating_time;
               $available_date['Fri_num_resrvation'] = $request->Fri_num_resrvation;
          }
          else {
            $available_date['Fri_from_time'] = null;
            $available_date['Fri_to_time'] = null;
            $available_date['Fri_type'] = null;
            $available_date['Fri_wating_time'] = null;
            $available_date['Fri_num_resrvation'] = null;
          }

          \App\DoctorAvailableDate::where('doctor_id',$doctors->id)->update($available_date);

          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message','  الدكتور اتعدل ');   }
          else
            { \Session::flash('flash_message','Doctor  has updated ');  }

          // return back();
          return redirect('Doctor');
    }

    public function destroy($id)
    {
        $doctors = Doctor::findOrFail($id);
        $doctors->get_SubSpecialization()->delete();
        $doctors->get_Service()->delete();
        $doctors->get_ClicnicAddress()->delete();
        $doctors->get_ClinicPicture()->delete();
        $doctors->get_Insurance_companies()->delete();
        $doctors->get_reservationed_patient()->delete();
        $doctors->delete();
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' الدكتور اتمسح ');   }
        else
          { \Session::flash('flash_message','Doctor  has delete ');  }

        return redirect('Doctor');
    }

    //-------------------------------------------

    public function reservations_page($doctor_id)
    {
        $doctor = Doctor::findOrFail($doctor_id);
        $Reservations = \App\Reservation::where('doctor_id',$doctor_id)->wheredate('date','>=',date("Y-m-d"))->get();
        return view('Doctor.Reservations',compact('doctor','Reservations'));
    }

    public function cancle_reservations($reservation_id)
    {
          $reservation = \App\Reservation::findOrFail($reservation_id);
          $reservation->delete();

          $Patient_email = $reservation->get_Patient->email;
          $Doctor_email = $reservation->get_Doctor->email;

          $employee_email = \App\Setting::where('title','employee_email')->first();    //return $employee_email->value;
          if ($employee_email->value)
          {
              \Mail::to($employee_email->value)->send(new \App\Mail\CancleReservation($reservation));
          }
          \Mail::to($Patient_email)->send(new \App\Mail\CancleReservation($reservation));
          \Mail::to($Doctor_email)->send(new \App\Mail\CancleReservation($reservation));

          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message','  الحجز اتمسح ');   }
          else
            { \Session::flash('flash_message','reservation  has delete ');  }

          return back();
    }

}
