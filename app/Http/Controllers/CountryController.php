<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
         $countries = Country::orderBy('name_en')->orderBy('name_en')->paginate();
         return view('Country.index',compact('countries'));
    }

    public function search($val)
    {
         $countries = Country::where('name_en','like','%'.$val.'%')->orWhere('name_ar','like','%'.$val.'%')->orderBy('name_en')->orderBy('name_en')->paginate();
         return view('Country.index',compact('countries','val'));
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required'
        ]);
        $country = Country::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$country->name_ar.' بلد ');   }
        else
          { \Session::flash('flash_message','country '.$country->name_en.' has added');  }

        return redirect('Country');
    }

    public function update(Request $request)
    {
          $data = $request->validate([
              'id' => 'required',
              'name_en' => 'required',
              'name_ar' => 'required'
          ]);
          $country = Country::findOrFail($request->id);
          $country->update($data);
          if( \Session::get('lang') == 'ar' )
            { \Session::flash('flash_message',' اتعدل '.$country->name_ar.' بلد ');   }
          else
            { \Session::flash('flash_message','country '.$country->name_en.' has updateds');  }

          return redirect('Country ');
    }

    public function showORhide($id)
    {
        $country = Country::findOrFail($id);
           if( $country->status )
           {
                 $country->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' لن يظهر للعامة '.$country->name.' دولة ');   }
                else
                  { \Session::flash('flash_message','Country '.$country->name.' is not public any more');  }
           }
           else
           {
                $country->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$country->name.' دولة ');   }
                else
                  { \Session::flash('flash_message','Country '.$country->name.' now is public ');  }
           }

           return back();
    }

}
