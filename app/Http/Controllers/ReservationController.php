<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reservation;
use App\CareReservation;

class ReservationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function health_plus_index()
    {
        $lang = app()->getLocale();

        $Reservations = Reservation::select( \DB::raw("CONCAT(doctors.fname_".$lang.",' ',doctors.lname_".$lang.") as doctor_name"), "reservations.id as reservation_id",
                                          'doctors.id as doctor_id',
                                          'patients.name as patient_name', 'patients.id as patient_id' , "date","time")
                                 ->leftJoin('doctors','doctors.id','reservations.doctor_id')
                                 ->leftJoin('patients','patients.id','reservations.patient_id')
                                 ->groupBy('reservations.id')
                                 ->latest('reservations.date')->paginate();
                                 // ->latest('reservations.date')->toSql(); return $Reservations;
        $type = 'health_plus';
        return view('Reservation.index',compact('Reservations','type'));
    }

    public function health_plus_search($val)
    {
        $lang = app()->getLocale();
        $Reservations = Reservation::select( \DB::raw("CONCAT(doctors.fname_".$lang.",' ',doctors.lname_".$lang.") as doctor_name"), "reservations.id as reservation_id",
                                          'doctors.id as doctor_id',
                                          'patients.name as patient_name', 'patients.id as patient_id' , "date","time")
                                 ->leftJoin('doctors','doctors.id','reservations.doctor_id')
                                 ->leftJoin('patients','patients.id','reservations.patient_id')
                                 ->where('doctors.id',$val)->orWhere('patients.id',$val)->orWhere('reservations.id',$val)
                                 ->orWhereRaw("CONCAT(doctors.fname_".$lang.",' ',doctors.lname_".$lang.") LIKE  ?  ",['%'.$val.'%'])
                                 ->orWhere('patients.name','like','%'.$val.'%')
                                 ->groupBy('reservations.id')
                                 ->latest('reservations.date')->paginate();
        $type = 'health_plus';
        return view('Reservation.index',compact('Reservations','type','val'));
    }

    public function care_index()
    {
          $lang = app()->getLocale();
          $Reservations = CareReservation::select( 'care_resvation.id as care_resvation_id', 'patients.name as patient_name',
                                               'patients.email as patient_email', 'patients.phone as patient_phone',
                                               'care_resvation.patient_id as patient_id' , 'care_resvation.price' , 'care_resvation.care_sub_services_id',

                                               "care_sub_services.name_".$lang." as care_sub_services_name"  ,
                                               "care_services.name_".$lang." as care_services_name"
                                             )
                                   ->leftJoin('patients','patients.id','care_resvation.patient_id')
                                   ->leftJoin('care_sub_services','care_sub_services.id','care_resvation.care_sub_services_id')
                                   ->leftJoin('care_services','care_services.id','care_sub_services.service_id')
                                   ->groupBy('care_resvation.id')
                                   ->latest('care_resvation.id')->paginate();
          $type = 'health_care';
          return view('Reservation.index',compact('Reservations','type'));
    }

    public function care_search($val)
    {
          $lang = app()->getLocale();
          $Reservations = CareReservation::select( 'care_resvation.id as care_resvation_id', 'patients.name as patient_name',
                                               'patients.email as patient_email', 'patients.phone as patient_phone',
                                               'care_resvation.patient_id as patient_id' , 'care_resvation.price' , 'care_resvation.care_sub_services_id',

                                               "care_sub_services.name_".$lang." as care_sub_services_name"  ,
                                               "care_services.name_".$lang." as care_services_name"
                                             )
                                   ->leftJoin('patients','patients.id','care_resvation.patient_id')
                                   ->leftJoin('care_sub_services','care_sub_services.id','care_resvation.care_sub_services_id')
                                   ->leftJoin('care_services','care_services.id','care_sub_services.service_id')

                                   ->where('care_resvation.id',$val)->orWhere('patients.name','like','%'.$val.'%')->orWhere('patients.email','like','%'.$val.'%')
                                   ->orWhere('patients.phone',$val)->orWhere('patients.id',$val)->orWhere("care_sub_services.name_".$lang,'like','%'.$val.'%')
                                   ->orWhere("care_services.name_".$lang,'like','%'.$val.'%')

                                   ->groupBy('care_resvation.id')
                                   ->latest('care_resvation.id')->paginate();
          $type = 'health_care';
          return view('Reservation.index',compact('Reservations','type','val'));
    }

    public function care_cancle($reservation_id)
    {
        $reservation = \DB::table('care_resvation')
                    ->select('care.id as care_id','care.email as care_email','care.name_en as care_name','patients.id as patient_id','patients.email as patient_email',
                              'patients.name as patient_name','care.phone as care_phone','patients.phone as patient_phone','care_resvation.id as reservation_id')
                    ->join('care_sub_services','care_sub_services.id','care_resvation.care_sub_services_id')
                    ->join('care_services','care_services.id','care_sub_services.service_id')
                      ->join('care','care.id','care_services.care_id')
                      ->join('patients','patients.id','care_resvation.patient_id')
                    ->where('care_resvation.id',$reservation_id)
                    ->groupBy('care_resvation.id')
                    ->first();
     // return  $reservation->care_email;
          // dd( $reservation );
         $employee_email = \App\Setting::where('title','employee_email')->first();    //return $employee_email->value;
         if ($employee_email->value)
         {
            \Mail::to($employee_email->value)->send(new \App\Mail\CancleCareReservationMail($reservation));
         }

         \Mail::to($reservation->care_email)->send(new \App\Mail\CancleCareReservationMail($reservation));
         \Mail::to($reservation->patient_email)->send(new \App\Mail\CancleCareReservationMail($reservation));

         CareReservation::whereId($reservation_id)->delete();

         if( \Session::get('lang') == 'ar' )
           { \Session::flash('flash_message',' الحجز اتمسح ');   }
         else
           { \Session::flash('flash_message','reservation has delete' );  }

         return back();
    }
}
