<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\DocSubSpecialization;
use App\DocSpecialization;

class DocSubSpecializationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('lang');
    }

    public function index()
    {
        // $DocSubSpecialization = DocSubSpecialization::orderBy('text_en')->paginate();
        $get_DocSpecialization = DocSpecialization::orderBy('name_en')->get();
        if (\Session::get('lang') == 'ar')
        {
            $DocSpecialization = \App\DocSpecialization::orderBy('name_ar')->pluck('name_ar','id');
            $DocSpecialization = [ ''=>' اختار تخصص ' ] + collect($DocSpecialization)->toArray();
        }
        else
        {
            $DocSpecialization = \App\DocSpecialization::orderBy('name_en')->pluck('name_en','id');
            $DocSpecialization = [ ''=>' choose specialization ' ] + collect($DocSpecialization)->toArray();
        }
        return view('DocSubSpecialization.index',compact('get_DocSpecialization','DocSpecialization'));
    }

    public function search($val)
    {
         $DocSubSpecialization = DocSubSpecialization::where('text_en','like','%'.$val.'%')->orWhere('text_ar','like','%'.$val.'%')
                                          ->orWhereHas('get_specialization',function($query) use($val){
                                              $query->where('name_ar','like','%'.$val.'%')->orWhere('name_en','like','%'.$val.'%');
                                          })->orderBy('text_en')->paginate();
         return view('DocSubSpecialization.index',compact('DocSubSpecialization','val'));
    }


    public function store(Request $request)
    {
        $data = $request->validate([
          'text_en' => 'required',
          'text_ar' => 'required',
          'specialization_id' => 'required'
        ]);
        $DocSubSpecialization = DocSubSpecialization::create($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اضاف '.$DocSubSpecialization->name_ar.' تخصص');   }
        else
          { \Session::flash('flash_message','Sub Specialization '.$DocSubSpecialization->name_en.' has added');  }

        return redirect('DocSubSpecialization');
    }


    public function update(Request $request)
    {
        $data = $request->validate([
          'id' => 'required',
          'text_en' => 'required',
          'text_ar' => 'required',
          'specialization_id' => 'required'
        ]);
        $DocSubSpecialization = DocSubSpecialization::findOrFail($request->id);
        $DocSubSpecialization->update($data);
        if( \Session::get('lang') == 'ar' )
          { \Session::flash('flash_message',' اتعدل '.$DocSubSpecialization->name_ar.' تخصص');   }
        else
          { \Session::flash('flash_message','Sub Specialization '.$DocSubSpecialization->name_en.' has updated ');  }

        return redirect('DocSubSpecialization');
    }

    //-----api----
    public function get_subSpec_by_specId($specId)
    {
        if (\Session::get('lang') == 'ar')
        {
            return DocSubSpecialization::where('specialization_id',$specId)->select(['text_ar as name','id'])->get();
        }
        else {
          return DocSubSpecialization::where('specialization_id',$specId)->select(['text_en as name','id'])->get();
        }
    }

    public function showORhide($id)
    {
        $DocSubSpecialization = DocSubSpecialization::findOrFail($id);
           if( $DocSubSpecialization->status )
           {
                 $DocSubSpecialization->update(['status' => '0']);

                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' لن يظهر للعامة '.$DocSubSpecialization->name.' كير ');   }
                else
                  { \Session::flash('flash_message','Care '.$DocSubSpecialization->name.' is not public any more');  }
           }
           else
           {
                $DocSubSpecialization->update(['status' => '1']);
                if( \Session::get('lang') == 'ar' )
                  { \Session::flash('flash_message',' الان سيظهر للعمامة '.$DocSubSpecialization->name.' التخصص الفرعي ');   }
                else
                  { \Session::flash('flash_message','Sub Specialization '.$DocSubSpecialization->name.' now is public ');  }
           }

           return back();
    }

}
