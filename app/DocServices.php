<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocServices extends Model
{
    protected $table = 'services';
    protected $fillable = [
        'name_ar', 'name_en', 'status'
    ];

}
