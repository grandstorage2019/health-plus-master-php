<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Care extends Model
{
    protected $table = 'care';
    protected $fillable = [
        'specialization_id','name_en','name_ar','area_id','phone','email','link','from_time','to_time','location_comment_en','location_comment_ar',
        'about_en','about_ar','facebook','instagram','twitter','views','snapchat','logo','status','insurance_company_id','lat','lang'
    ];

    public function get_pictures()
    {
       return $this->hasMany(CarePicture::class,'care_id');
    }

    public function get_service()
    {
       return $this->hasMany(CareService::class,'care_id');
    }

    public function get_area()
    {
       return $this->belongsTo(Area::class,'area_id');
    }

    public function get_specialization()
    {
       return $this->belongsTo(CareSpecialization::class,'specialization_id');
    }

    public function get_insurance_company()
    {
       return $this->belongsTo(InsuranceCompany::class,'insurance_company_id');
    }

    public function get_starts()
    {
        $stars = \App\CareRating::where('care_id',$this->id)->avg('stars');
        return ($stars)? round($stars): 0 ;
    }

}
