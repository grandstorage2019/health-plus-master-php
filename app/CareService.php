<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareService extends Model
{
    const UPDATED_AT = null;
    protected $table = 'care_services';
    protected $fillable = [
        'care_id','name_en','name_ar','logo'
    ];

    public function get_subService()
    {
       return $this->hasMany(CareSubService::class,'service_id');
    }

}
