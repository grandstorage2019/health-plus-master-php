<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPatient extends Model
{
    const UPDATED_AT = null;
    protected $table = 'patient_notification';
    protected $fillable = [
      'message', 'image'  
    ];

}
