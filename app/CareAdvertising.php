<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareAdvertising extends Model
{
  protected $table = 'care_advertising';
  protected $fillable = [
      'title_en','title_ar','details_ar','details_en','image','link','is_promoted','status'
  ];
}
