<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationCare extends Model
{
    const UPDATED_AT = null;
    protected $table = 'care_notification';
    protected $fillable = [
        'message', 'image' 
    ];
}
