<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocLicense extends Model
{
    protected $table = 'doc_license';
    const UPDATED_AT = null;
    protected $fillable = [
        'doctor_id', 'image'
    ];

    public function getImageAttribute($value)
    {
        return asset('images/license/'.$value);
    }


}
