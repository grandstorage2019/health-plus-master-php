<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $dates = ['birth_day'];
    protected $fillable = [
        'type', 'firebase_token', 'status','model','app_version','os','fname_en','fname_ar','lname_en','lname_ar','email','secretary_email',
        'password', 'phone', 'birth_day','gender','country_id','facebook_id','google_id','registred','username','about_en',
        'about_ar','license','device_token', 'profile_image','title_ar','title_en','is_accapted_by_admin','price','plan_id',
        'specialization_id','level_ar','level_en'
    ];

    protected $hidden = [
        'password'
    ];

    public function get_country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }

    public function get_SubSpecialization()
    {
        return $this->belongsToMany(DocSubSpecialization::class,'doctor_subspecialization','doctor_id','subSpecialization_id');
    }

    public function get_subSpecialization_ids()
    {
        return \DB::table('doctor_subspecialization')->where('doctor_id',$this->id)->pluck('subSpecialization_id');
    }

    public function get_insurance_companies_ids()
    {
        return \DB::table('doctor_insurance_companies')->where('doctor_id',$this->id)->pluck('company_id');
    }

    public function get_Service()
    {
        return $this->belongsToMany(Service::class,'doctor_services','doctor_id','service_id');

    }

    public function get_ClicnicAddress()
    {
        return $this->hasOne(ClicnicAddress::class,'doctor_id');
    }

    public function get_ClinicPicture()
    {
        return $this->hasMany(ClinicPicture::class,'doctor_id');
    }

    public function get_InsuranceCompany()
    {
        return $this->belongsToMany(InsuranceCompany::class,'doctor_insurance_companies','doctor_id','company_id');
    }

    public function get_specialization()
    {
        return $this->belongsTo(DocSpecialization::class,'specialization_id');
    }

    public function get_specialization_mini()
    {
        return $this->belongsTo(DocSpecialization::class,'specialization_id')->select('name_ar','name_en');
    }

    public function get_reservationed_patient()
    {
        return $this->belongsToMany(Patient::class,'reservations','doctor_id','patient_id')->withPivot('date','type','time')
                          ->select('name','email','phone' ) ;
    }

    public function get_reservationed_patient_limit()
    {
        return $this->belongsToMany(Patient::class,'reservations','doctor_id','patient_id')->withPivot('date','type','time')
                          ->select('name','email','phone' )->limit(15) ;
    }

    public function get_doc_name()
    {
          if ($this->fname_en)
          {
              if (\Session::get('lang') == 'ar')
              {
                 return $this->fname_ar. ' '. $this->lname_ar;
              }
              else {
                return $this->fname_en. ' '. $this->lname_en;
              }
          }
          else{
              return __('page.Not registerd');
          }
    }

    public function get_starts()
    {
        $stars = \App\Doc_Rating::where('doctor_id',$this->id)->avg('stars');
        return ($stars)? round($stars): 0 ;
    }

    public function get_available_date()
    {
        return $this->hasOne(DoctorAvailableDate::class,'doctor_id');
    }

    public function get_Insurance_companies()
    {
       return $this->belongsToMany(InsuranceCompany::class,'doctor_insurance_companies','doctor_id','company_id');
    }

    public function get_plan()
    {
        return $this->belongsTo(Plan::class,'plan_id');
    }

    public function get_Licenses()
    {
        return $this->hasMany(DocLicense::class,'doctor_id');
    }

}
