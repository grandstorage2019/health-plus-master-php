<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Governorate extends Model
{
    protected $table = 'governorate';
    protected $fillable = [
        'country_id', 'name_en', 'status','name_ar'
    ];

}
