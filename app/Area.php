<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'area';
    protected $fillable = [
        'governorate_id', 'name_en', 'name_ar','status'
    ];

}
