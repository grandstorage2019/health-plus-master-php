<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;

class PushNotification //extends Model
{

        public static function send($tokens, $msg , $img=null,$type='')
        {
            $fields = array
                (
                "registration_ids" => $tokens,
                "priority" => 10,
                'data' => [
                  'title' => $msg,
                  'details' => $msg,
                  'image' => $img,
                  'notification_type' => $type
                ],
                'vibrate' => 1,
                'sound' => 1
            );
            $headers = array
                (
                'accept: application/json',
                'Content-Type: application/json',
                'Authorization: key=' .
                // 'AIzaSyDMeSqZgUzQaPeVEm-KsTusRUVGFiRbW80'
                'AAAAR1y0XWA:APA91bFDagi_2wfWO3cgb4WHn_yRJi_dKBhxsSlAY7_CqrtQt4meE_NtQCBoSPn14HINw10YsK2sRmazBY8T8g20J32GFqKz-T-Gm5H_-mRAb4phYZtmCHF0GEexHwM1U8b0LxHzSRI84HMnZuUP1Ix6K1P40uFLDQ'
                // 'AAAAB5pjU0w:APA91bHoAa-vBG04TlLc0TAsXlJhRHJvcYasr7d6FOmqcajXYIy5wNrtUQg9_blMU9NSUsRYDvSJvEOn1JQgwwnQxDDWIw9RUi3Vpq-h6VYcKHZt3kxMmGxi7udSNC1cZbdwfD2VP89k'

            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            //  var_dump($result);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }
}
