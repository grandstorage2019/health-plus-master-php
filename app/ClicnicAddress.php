<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClicnicAddress extends Model
{
      protected $table = 'clicnic_address';
      public $timestamps = false;
      protected $fillable = [
          'clinic_name_en', 'clinic_name_ar', 'clinic_number', 'area_id','lat','lang','building_number_ar','building_number_en','street_name_ar','street_name_en',   
          'floor_ar','floor_en','apartment_en','apartment_ar','landmark_en','landmark_ar','doctor_id','block_ar','block_en','flat_number'
      ];

      public function get_area()
      {
           return $this->belongsTo(Area::class,'area_id');
      }

}
