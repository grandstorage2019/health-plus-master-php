<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
   protected $table = 'contact_us';
    protected $fillable = [
        'doctor_id','message'
    ];

    public function get_doctor()
    {
       return $this->belongsTo(Doctor::class,'doctor_id');
    }

    public function get_Patient()
    {
       return $this->belongsTo(Patient::class,'patient_id');
    }




}
