<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doc_Rating extends Model
{
    protected $table = 'rating';
    protected $fillable = [
        'doctor_id', 'patient_id', 'stars','comment'
    ];
 
}
