<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CareRating extends Model
{
    protected $table = 'care_rating';
    const UPDATED_AT = null;
    protected $fillable = [
        'stars','patient_id','comment','care_id'
    ];
}
