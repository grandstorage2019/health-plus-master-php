-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2018 at 05:35 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `health_plus`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL,
  `title_en` varchar(225) CHARACTER SET utf8 NOT NULL,
  `title_ar` varchar(225) CHARACTER SET utf8 NOT NULL,
  `details_en` text CHARACTER SET utf8 NOT NULL,
  `details_ar` text CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `title_en`, `title_ar`, `details_en`, `details_ar`) VALUES
(1, 'Our Services2', '????', 'Our Services Our Services Our Services Our Services Our Services Our Services Our Services ', 'هيا ده السيرفيس سيبقا فين السيرفيس ده يا كبتن \nانا مش مسامح هيا ده السيرفيس سيبقا فين السيرفيس ده يا كبتن \nانا مش مسامح'),
(2, 'c', 'c', 'dd', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `lang` enum('en','ar') DEFAULT 'en',
  `remember_token` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `phone` varchar(220) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `super_admin` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `username`, `password`, `lang`, `remember_token`, `email`, `phone`, `role_id`, `super_admin`) VALUES
(1, 'admin', 'admin', '$2y$10$D.7.ErEs51IO4.4BI4WGdOhGxbKtJD.j/WoXvZh634UpdWHbewBUG', 'ar', 'DGySqhx7SlmLNG1x1CVpGjdcILmUvPTOHRqYqsr8rrisjreFnFPFIlhq6kA6', 'm@m.com', '201094943793', 0, 1),
(4, 'advertiser', 'adver', '$2y$10$TBDcjcxpc72Z2JtJcGjCN.5AxFs/XPBcR1A4D4uirQxE5HfJ8tafK', 'ar', 'D3MwIBKx0DMRnSZZlSSvStCD2FkPdYHiiaYltU67ttIeLdix1fluraQjs7Vs', 'm@m.com', '01098656745', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `governorate_id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `governorate_id`, `name_en`, `name_ar`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'el-nozha', 'النزهه', 0, '0000-00-00 00:00:00', '2018-05-08 13:16:41'),
(2, 1, 'elabbor', 'العبور', 1, '0000-00-00 00:00:00', '2018-05-08 13:16:42');

-- --------------------------------------------------------

--
-- Table structure for table `care`
--

CREATE TABLE `care` (
  `id` int(11) NOT NULL,
  `specialization_id` int(11) DEFAULT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `phone` varchar(225) DEFAULT NULL,
  `mobile` varchar(225) DEFAULT NULL,
  `from_time` time DEFAULT NULL,
  `to_time` time DEFAULT NULL,
  `location_comment_en` text,
  `location_comment_ar` varchar(225) DEFAULT NULL,
  `about_en` varchar(225) DEFAULT NULL,
  `about_ar` varchar(225) DEFAULT NULL,
  `facebook` varchar(225) DEFAULT NULL,
  `instagram` varchar(225) DEFAULT NULL,
  `twitter` varchar(225) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `snapchat` varchar(225) DEFAULT NULL,
  `logo` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care`
--

INSERT INTO `care` (`id`, `specialization_id`, `name_en`, `name_ar`, `area_id`, `phone`, `mobile`, `from_time`, `to_time`, `location_comment_en`, `location_comment_ar`, `about_en`, `about_ar`, `facebook`, `instagram`, `twitter`, `views`, `snapchat`, `logo`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'first name', 'اول اسم', 1, '26630534', '01121903748', '05:13:12', '09:15:14', 'location comment english', 'تعليق المكان', 'about place', 'عن المكان', 'www.facebook.com', 'www.instagram.com', 'www.twitter.com', 132, 'www.snapchat.com', '', 1, '2018-04-19 05:16:14', '2018-04-30 12:20:16'),
(2, 1, 'second name', 'تاني اسم', 1, '26630534', '01121903748', '05:13:12', '09:15:14', 'location comment englishw', 'wتعليق المكان', 'about placew', 'wعن المكان', 'www.facebook.comw', 'www.instagram.com', 'www.twitter.com', 132, 'www.snapchat.com', 'image.jpg', 1, '2018-04-19 05:16:14', '2018-04-30 12:20:16'),
(5, 1, 'ddd', 'dddd', 2, '222222', NULL, '14:22:00', '14:22:00', '2222', '2222', '2222', '222', '2222', '222', '222', 0, '22', '60293.PNG', 1, '2018-04-26 15:15:06', '2018-04-26 15:15:06'),
(6, 1, 'ddd', 'ddd', 1, 'ddd', NULL, '14:22:00', '14:22:00', '222', '2222', '222', '222', '222', '222', '22', 0, '222', '82743.PNG', 1, '2018-04-26 15:31:42', '2018-04-26 15:31:42'),
(8, 1, 'dd', 'ddd', 1, 'dddd', NULL, '14:22:00', '14:22:00', '22222', '22222', '22222', '222222', '222222', '22222', '2222', 0, '2222', '89428.PNG', 1, '2018-04-26 15:41:30', '2018-04-26 15:41:30'),
(9, 1, 'golds', 'جولدس', 1, '01026020563', NULL, '02:00:00', '10:00:00', '3a new street in ald street', '3ا الشارع الجديد في الشارع القديم', 'some info', 'شويت معلومات', 'face.com', 'insta.com', 'twtter.com', 0, 'snapchat.com', '83043.PNG', 1, '2018-04-30 07:57:29', '2018-04-30 07:57:29');

-- --------------------------------------------------------

--
-- Table structure for table `care_advertising`
--

CREATE TABLE `care_advertising` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `link` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `is_promoted` int(11) NOT NULL DEFAULT '0',
  `title_ar` varchar(225) NOT NULL,
  `title_en` varchar(225) NOT NULL,
  `details_ar` varchar(225) NOT NULL,
  `details_en` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_advertising`
--

INSERT INTO `care_advertising` (`id`, `image`, `link`, `status`, `is_promoted`, `title_ar`, `title_en`, `details_ar`, `details_en`, `created_at`, `updated_at`) VALUES
(1, '83552.jpg', 'www.google.com', 1, 1, 'عنوان عربي', 'english title', 'عنوان عربي', 'english title', '2018-04-19 00:00:00', '2018-04-22 12:40:48');

-- --------------------------------------------------------

--
-- Table structure for table `care_notification`
--

CREATE TABLE `care_notification` (
  `id` int(11) NOT NULL,
  `message` varchar(225) NOT NULL,
  `image` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_notification`
--

INSERT INTO `care_notification` (`id`, `message`, `image`, `created_at`) VALUES
(2, 'fefef', '28466.jpg', '2018-04-30 15:18:57');

-- --------------------------------------------------------

--
-- Table structure for table `care_offers`
--

CREATE TABLE `care_offers` (
  `id` int(11) NOT NULL,
  `care_id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `link` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_offers`
--

INSERT INTO `care_offers` (`id`, `care_id`, `image`, `link`, `status`, `created_at`, `updated_at`) VALUES
(1, 9, 'image4.png', '22.com', 1, '2018-04-12 00:00:00', '2018-04-30 13:37:02'),
(2, 9, 'image3.png', '2d2.com', 1, '2018-04-12 00:00:00', '2018-05-03 15:55:41');

-- --------------------------------------------------------

--
-- Table structure for table `care_pictures`
--

CREATE TABLE `care_pictures` (
  `id` int(11) NOT NULL,
  `care_id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_pictures`
--

INSERT INTO `care_pictures` (`id`, `care_id`, `image`, `created_at`) VALUES
(1, 1, 'image1.jpg', '2018-04-11 00:00:00'),
(2, 1, 'image2.jpg', '2018-04-04 00:00:00'),
(3, 1, 'image1.jpg', '2018-04-11 00:00:00'),
(4, 1, 'image2.jpg', '2018-04-04 00:00:00'),
(6, 8, '90203.PNG', '2018-04-26 15:41:30'),
(7, 9, '95942.PNG', '2018-04-30 07:57:29'),
(10, 9, '46250.png', '2018-04-30 09:48:14'),
(11, 9, '76781.PNG', '2018-04-30 09:48:26');

-- --------------------------------------------------------

--
-- Table structure for table `care_rating`
--

CREATE TABLE `care_rating` (
  `id` int(11) NOT NULL,
  `stars` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_at` datetime NOT NULL,
  `care_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_rating`
--

INSERT INTO `care_rating` (`id`, `stars`, `patient_id`, `comment`, `created_at`, `care_id`) VALUES
(1, 4, 1, 'sdfadf', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `care_resvation`
--

CREATE TABLE `care_resvation` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `care_sub_services_id` int(11) NOT NULL,
  `price` float NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `care_services`
--

CREATE TABLE `care_services` (
  `id` int(11) NOT NULL,
  `care_id` int(11) NOT NULL,
  `name_en` varchar(225) CHARACTER SET utf8 NOT NULL,
  `name_ar` varchar(225) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `care_services`
--

INSERT INTO `care_services` (`id`, `care_id`, `name_en`, `name_ar`, `logo`, `created_at`) VALUES
(1, 1, 'service 1', 'الخدمة 1 ', '', '2018-04-27 00:00:00'),
(2, 1, 'service 2', 'الخدمة 2', '', '2018-04-27 00:00:00'),
(3, 3, 'www', 'ddd', '29222.PNG', '2018-04-26 15:05:19'),
(4, 3, 'wwww', 'ddd', '99346.PNG', '2018-04-26 15:05:19'),
(5, 5, 'dd', 'dd', '76624.PNG', '2018-04-26 15:15:06'),
(6, 5, 'ddd', 'ddd', '94636.PNG', '2018-04-26 15:15:06'),
(7, 6, 'dd', 'dd', '38690.PNG', '2018-04-26 15:31:42'),
(9, 9, 'message', 'مساج', '72352.PNG', '2018-04-30 07:57:29'),
(10, 9, 'steem', 'بخار', '94056.png', '2018-04-30 07:57:29'),
(11, 9, 'rrrrrrrr', 'rrr', '13820.PNG', '2018-04-30 09:38:46'),
(12, 9, 'wdw', 'dddd', '49189.PNG', '2018-04-30 09:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `care_specialization`
--

CREATE TABLE `care_specialization` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `care_specialization`
--

INSERT INTO `care_specialization` (`id`, `name_ar`, `name_en`, `status`, `created_at`, `updated_at`) VALUES
(1, 'الاسبلاشن  1', 'care spc1', 0, '2018-04-02 00:00:00', '2018-05-08 16:19:38'),
(2, 'الاسبلاشن  2', 'spc2', 1, '2018-04-02 00:00:00', '2018-05-08 16:06:52');

-- --------------------------------------------------------

--
-- Table structure for table `care_sub_services`
--

CREATE TABLE `care_sub_services` (
  `id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `name_en` varchar(255) CHARACTER SET utf8 NOT NULL,
  `name_ar` varchar(225) CHARACTER SET utf8 NOT NULL,
  `quantity` varchar(225) NOT NULL,
  `price` float NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `care_sub_services`
--

INSERT INTO `care_sub_services` (`id`, `service_id`, `name_en`, `name_ar`, `quantity`, `price`, `status`) VALUES
(1, 1, 'sub 1 -1', '?? 1-1', '250', 30, 1),
(2, 1, 'sub 1-2', '?? 1 -2', '', 0, 1),
(3, 2, 'sub 12 -1', '?? 12-1', '250', 30, 1),
(4, 2, 'sub 13-2', '?? 13 -2', '', 0, 1),
(5, 6, '8888', 'jjjj', '555', 555, 1),
(6, 6, 'bbb', 'bb', '22', 2, 0),
(7, 9, 'hgh', 'fkkf', '222', 222, 0),
(8, 9, 'lflfl', 'fmmk', '44', 444, 1);

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `massage` text NOT NULL,
  `from` enum('doctor','patient') NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `clicnic_address`
--

CREATE TABLE `clicnic_address` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `clinic_name` varchar(225) DEFAULT NULL,
  `clinic_number` varchar(225) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `lang` float DEFAULT NULL,
  `building_number_ar` varchar(225) DEFAULT NULL,
  `building_number_en` varchar(225) DEFAULT NULL,
  `street_name_ar` varchar(225) DEFAULT NULL,
  `street_name_en` varchar(225) DEFAULT NULL,
  `floor_ar` varchar(225) DEFAULT NULL,
  `floor_en` varchar(225) DEFAULT NULL,
  `apartment_en` varchar(225) DEFAULT NULL,
  `apartment_ar` varchar(225) DEFAULT NULL,
  `landmark_en` varchar(225) DEFAULT NULL,
  `landmark_ar` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clicnic_address`
--

INSERT INTO `clicnic_address` (`id`, `doctor_id`, `clinic_name`, `clinic_number`, `area_id`, `lat`, `lang`, `building_number_ar`, `building_number_en`, `street_name_ar`, `street_name_en`, `floor_ar`, `floor_en`, `apartment_en`, `apartment_ar`, `landmark_en`, `landmark_ar`) VALUES
(1, 1, 'Clinic Name', '3432423423', 1, 34.2321, 23.231, 'رقم المبني', 'r2m almbna', 'اسم الشارع', 'street name', 'الدور الخامس', 'fifth floor', 'flat', 'شقه', 'land mark', 'لاند مارك'),
(2, 4, 'elmomom', '23', 1, NULL, NULL, 'ييي', 'ييي', 'عمان', '3aman', '2', '2', 'يصي', 'يي', NULL, NULL),
(3, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 5, 'dd', '22', 1, NULL, NULL, 'ddd', 'ddd', 'dd', 'ddd', '2', '22', '22', '22', NULL, NULL),
(5, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 21, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 23, 'العياده', '20', 1, NULL, NULL, '3', '3', 'المدينة', 'elmadena', '1', '1', 'ddw', 'dwdwdwd', 'dd', 'dd'),
(26, 25, 'ddd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `clinic_picture`
--

CREATE TABLE `clinic_picture` (
  `id` int(11) NOT NULL,
  `image` varchar(225) NOT NULL,
  `doctor_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `clinic_picture`
--

INSERT INTO `clinic_picture` (`id`, `image`, `doctor_id`) VALUES
(1, '19436.jpg', 1),
(2, '81031.jpg', 1),
(3, '70277.PNG', 9),
(11, '59068.PNG', 23),
(13, '93982.png', 23),
(14, '88066.jpg', 25),
(15, '62131.jpg', 25);

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `doctor_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 23, 'dwdwdwdwd wd wdwdw dw dw dwdw d w dw', '2018-05-03 00:00:00', '2018-05-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `name_en`, `name_ar`, `status`) VALUES
(1, 'Egypt', 'مصر', 1),
(2, 'kwit', 'كويت', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `is_accapted_by_admin` tinyint(1) NOT NULL DEFAULT '0',
  `device_token` varchar(225) DEFAULT NULL,
  `firebase_token` varchar(225) DEFAULT NULL,
  `os` varchar(60) DEFAULT NULL,
  `app_version` varchar(255) DEFAULT NULL,
  `model` varchar(225) DEFAULT NULL,
  `registred` tinyint(1) DEFAULT '0',
  `status` tinyint(1) DEFAULT '1',
  `fname_en` varchar(225) DEFAULT NULL,
  `fname_ar` varchar(225) DEFAULT NULL,
  `lname_en` varchar(225) DEFAULT NULL,
  `lname_ar` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `username` varchar(225) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `phone` varchar(225) DEFAULT NULL,
  `birth_day` datetime DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `facebook_id` varchar(225) DEFAULT NULL,
  `google_id` varchar(225) DEFAULT NULL,
  `license` varchar(225) DEFAULT NULL,
  `about_ar` varchar(225) DEFAULT NULL,
  `about_en` varchar(225) DEFAULT NULL,
  `title_en` varchar(225) DEFAULT NULL,
  `title_ar` varchar(225) DEFAULT NULL,
  `specialization_id` int(11) DEFAULT NULL,
  `profile_image` varchar(225) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `price` float NOT NULL DEFAULT '0',
  `plan_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `is_accapted_by_admin`, `device_token`, `firebase_token`, `os`, `app_version`, `model`, `registred`, `status`, `fname_en`, `fname_ar`, `lname_en`, `lname_ar`, `email`, `username`, `password`, `phone`, `birth_day`, `gender`, `facebook_id`, `google_id`, `license`, `about_ar`, `about_en`, `title_en`, `title_ar`, `specialization_id`, `profile_image`, `views`, `price`, `plan_id`, `created_at`, `updated_at`) VALUES
(23, 0, NULL, NULL, NULL, NULL, NULL, 0, 1, 'ahmed', 'احمد', 'ali', 'علي', 'ali@a.com', 'aliii', NULL, '0109494379345', '2018-05-29 00:00:00', 'male', NULL, NULL, '46722.jpg', 'بواب و خدام', 'servent and security', 'bowab', 'بواب', 2, '71392.PNG', 0, 200, 2, '2018-04-29 09:20:47', '2018-04-29 13:11:36'),
(25, 1, NULL, NULL, NULL, NULL, NULL, 0, 1, 'test', 'test', 'test', 'test', 'test@2grand.net', 'test', NULL, '0109494379345', '2018-05-23 00:00:00', 'female', NULL, NULL, '65358.jpg', 'ddd', 'ddd', 'ddd', 'dddd', 1, '50774.PNG', 0, 22, 2, '2018-05-03 15:48:39', '2018-05-03 15:49:04');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_available_date`
--

CREATE TABLE `doctor_available_date` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `from_date` datetime DEFAULT NULL,
  `to_date` datetime DEFAULT NULL,
  `is_life_time` tinyint(1) NOT NULL DEFAULT '0',
  `Sat_from_time` time(1) DEFAULT NULL,
  `Sat_to_time` time DEFAULT NULL,
  `Sat_type` enum('specific time','first reservation') DEFAULT NULL,
  `Sat_wating_time` time DEFAULT NULL,
  `Sat_num_resrvation` int(11) DEFAULT '0',
  `Sun_from_time` time DEFAULT NULL,
  `Sun_to_time` time DEFAULT NULL,
  `Sun_type` enum('specific time','first reservation') DEFAULT NULL,
  `Sun_wating_time` time DEFAULT NULL,
  `Sun_num_resrvation` int(11) DEFAULT '0',
  `Mon_from_time` time DEFAULT NULL,
  `Mon_to_time` time DEFAULT NULL,
  `Mon_type` enum('specific time','first reservation') DEFAULT NULL,
  `Mon_wating_time` time DEFAULT NULL,
  `Mon_num_resrvation` int(11) DEFAULT '0',
  `Tue_from_time` time DEFAULT NULL,
  `Tue_to_time` time DEFAULT NULL,
  `Tue_type` enum('specific time','first reservation') DEFAULT NULL,
  `Tue_wating_time` time DEFAULT NULL,
  `Tue_num_resrvation` int(11) DEFAULT '0',
  `Thu_from_time` time(1) DEFAULT NULL,
  `Thu_to_time` time DEFAULT NULL,
  `Thu_type` enum('specific time','first reservation') DEFAULT NULL,
  `Thu_wating_time` time DEFAULT NULL,
  `Thu_num_resrvation` int(11) DEFAULT '0',
  `Wed_from_time` time DEFAULT NULL,
  `Wed_to_time` time DEFAULT NULL,
  `Wed_type` enum('specific time','first reservation') DEFAULT NULL,
  `Wed_wating_time` time DEFAULT NULL,
  `Wed_num_resrvation` int(11) DEFAULT '0',
  `Fri_from_time` time DEFAULT NULL,
  `Fri_to_time` time DEFAULT NULL,
  `Fri_type` enum('specific time','first reservation') DEFAULT NULL,
  `Fri_wating_time` time DEFAULT NULL,
  `Fri_num_resrvation` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_available_date`
--

INSERT INTO `doctor_available_date` (`id`, `doctor_id`, `from_date`, `to_date`, `is_life_time`, `Sat_from_time`, `Sat_to_time`, `Sat_type`, `Sat_wating_time`, `Sat_num_resrvation`, `Sun_from_time`, `Sun_to_time`, `Sun_type`, `Sun_wating_time`, `Sun_num_resrvation`, `Mon_from_time`, `Mon_to_time`, `Mon_type`, `Mon_wating_time`, `Mon_num_resrvation`, `Tue_from_time`, `Tue_to_time`, `Tue_type`, `Tue_wating_time`, `Tue_num_resrvation`, `Thu_from_time`, `Thu_to_time`, `Thu_type`, `Thu_wating_time`, `Thu_num_resrvation`, `Wed_from_time`, `Wed_to_time`, `Wed_type`, `Wed_wating_time`, `Wed_num_resrvation`, `Fri_from_time`, `Fri_to_time`, `Fri_type`, `Fri_wating_time`, `Fri_num_resrvation`) VALUES
(1, 1, '2018-04-16 00:00:00', '2018-04-30 00:00:00', 0, '02:11:17.1', '06:17:24', 'specific time', '08:24:20', 0, '07:18:21', '05:29:34', 'specific time', '04:09:09', 0, '05:00:10', '04:22:40', 'first reservation', '06:25:16', 4, '07:36:46', '18:29:20', NULL, '04:15:38', 0, '10:57:20.9', '13:21:08', NULL, '09:22:21', 0, '09:42:22', '15:18:38', NULL, '00:30:00', 0, '06:37:12', '06:19:38', NULL, '15:22:41', 0),
(2, 11, '2018-04-01 00:00:00', '2018-04-28 00:00:00', 0, '14:00:00.0', '21:00:00', 'specific time', '00:00:30', NULL, NULL, NULL, NULL, NULL, 0, '03:00:00', '06:00:00', 'first reservation', NULL, 6, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(3, 12, NULL, NULL, 1, '15:03:00.0', '21:00:00', 'specific time', '00:00:40', NULL, '04:00:00', '08:00:00', 'first reservation', NULL, 6, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(4, 13, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '14:00:00', '15:00:00', 'specific time', '00:00:03', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(5, 15, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '02:00:00', '06:00:00', 'specific time', NULL, NULL, '15:00:00', '21:00:00', 'first reservation', NULL, 5, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(6, 16, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '15:00:00', '18:00:00', 'specific time', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(7, 17, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '15:00:00', '18:00:00', 'specific time', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(8, 18, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '15:00:00', '18:00:00', 'specific time', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(9, 19, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '15:00:00', '18:00:00', 'specific time', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(10, 20, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '02:00:00', '03:00:00', 'specific time', '00:00:30', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(11, 21, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '03:00:00', '06:00:00', 'specific time', '00:00:30', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(12, 22, NULL, NULL, 1, NULL, NULL, NULL, NULL, 0, '02:00:00', '04:00:00', 'specific time', '00:30:00', NULL, NULL, NULL, NULL, NULL, 0, '13:00:00', '17:00:00', 'specific time', '01:00:00', NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0),
(13, 23, '2018-04-03 00:00:00', '2018-04-21 00:00:00', 0, '03:00:00.0', '06:00:00', 'specific time', '00:20:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01:00:00', '03:00:00', 'first reservation', NULL, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 24, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 25, '2018-05-01 00:00:00', '2018-05-02 00:00:00', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '02:00:00', '05:30:00', 'specific time', '00:20:00', NULL, '02:00:00', '02:00:00', 'first reservation', NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_insurance_companies`
--

CREATE TABLE `doctor_insurance_companies` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_insurance_companies`
--

INSERT INTO `doctor_insurance_companies` (`id`, `doctor_id`, `company_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 4),
(4, 3, 5),
(5, 1, 5),
(6, 1, 6),
(7, 2, 6),
(8, 7, 1),
(9, 8, 1),
(12, 23, 1),
(13, 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_services`
--

CREATE TABLE `doctor_services` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_services`
--

INSERT INTO `doctor_services` (`id`, `doctor_id`, `service_id`) VALUES
(1, 1, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_subspecialization`
--

CREATE TABLE `doctor_subspecialization` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `subSpecialization_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor_subspecialization`
--

INSERT INTO `doctor_subspecialization` (`id`, `doctor_id`, `subSpecialization_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 4, 3),
(4, 5, 3),
(5, 6, 2),
(6, 7, 2),
(9, 23, 1),
(10, 25, 2);

-- --------------------------------------------------------

--
-- Table structure for table `favourites`
--

CREATE TABLE `favourites` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `governorate`
--

CREATE TABLE `governorate` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `governorate`
--

INSERT INTO `governorate` (`id`, `country_id`, `name_en`, `name_ar`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'cario', 'القاهرة', 1, '2018-04-03 00:00:00', '2018-05-08 13:26:45'),
(2, 1, 'giza', 'الجيزه', 0, '2018-04-03 00:00:00', '2018-05-08 13:26:45');

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE `insurance_companies` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `insurance_companies`
--

INSERT INTO `insurance_companies` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'insury', 1, '2018-04-04 00:00:00', '2018-04-05 00:00:00'),
(2, 'مشورتي', 1, '2018-04-04 00:00:00', '2018-04-04 00:00:00'),
(3, 'edited new insurance', 1, '2018-04-24 11:47:24', '2018-05-08 16:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `device_token` varchar(225) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `firebase_token` varchar(225) NOT NULL,
  `os` enum('android','ios') NOT NULL,
  `app_version` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `model` varchar(225) NOT NULL,
  `email` varchar(225) DEFAULT NULL,
  `username` varchar(225) DEFAULT NULL,
  `password` varchar(225) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `birth_day` datetime DEFAULT NULL,
  `gender` enum('male','female') DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `facebook_id` varchar(225) DEFAULT NULL,
  `google_id` varchar(225) DEFAULT NULL,
  `registred` tinyint(1) NOT NULL DEFAULT '0',
  `image` varchar(225) DEFAULT NULL,
  `added_by_doctor_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ucs2 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `device_token`, `name`, `firebase_token`, `os`, `app_version`, `status`, `model`, `email`, `username`, `password`, `phone`, `birth_day`, `gender`, `country_id`, `facebook_id`, `google_id`, `registred`, `image`, `added_by_doctor_id`, `created_at`, `updated_at`) VALUES
(89, 'FSDF', 'Mohamed Atef', '', 'android', '', 1, '123', 'mohamed@yahoo.com', 'neni123', NULL, NULL, '2018-04-19 14:11:11', 'male', 1, '2', '121', 1, NULL, NULL, '2018-04-19 09:19:23', '2018-04-19 04:22:21'),
(90, 'FSDF', 'Memo Atef', '', 'ios', '', 1, '123', 'mohamed@yahoo.com', 'neni123', NULL, NULL, '2018-04-19 14:11:11', 'male', 1, '2', 'd5oWnz55n_Y:APA91bFd8ogoScFpnX8MZ7057d1QTlbOVkTd5THNJETPUzFSyeoIA8L34JOdLM7q7D85ENoRhOppQQy6y1C1HUT7b5sVvDwgM1ton6Rff_2iuM3gP5IxTEQjKVy0vbMn5UVHLNzQMqyu', 1, NULL, NULL, '2018-04-19 09:19:23', '2018-04-19 04:22:21');

-- --------------------------------------------------------

--
-- Table structure for table `patient_advertising`
--

CREATE TABLE `patient_advertising` (
  `id` int(11) NOT NULL,
  `title_en` varchar(225) NOT NULL,
  `title_ar` varchar(225) NOT NULL,
  `image` varchar(225) NOT NULL,
  `link` varchar(225) NOT NULL,
  `is_promited` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_advertising`
--

INSERT INTO `patient_advertising` (`id`, `title_en`, `title_ar`, `image`, `link`, `is_promited`, `status`, `created_at`, `updated_at`) VALUES
(1, 'english title', 'عنوان عربي', '45452.jpg', 'www.google.com', 1, 1, '2018-04-22 11:18:39', '2018-04-22 12:25:26');

-- --------------------------------------------------------

--
-- Table structure for table `patient_notification`
--

CREATE TABLE `patient_notification` (
  `id` int(11) NOT NULL,
  `message` varchar(225) NOT NULL,
  `image` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient_notification`
--

INSERT INTO `patient_notification` (`id`, `message`, `image`, `created_at`) VALUES
(1, 'ddddd', '92588.png', '2018-04-30 15:39:10');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `related_to` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `title`, `related_to`, `created_at`) VALUES
(1, 'DashBoard', 'DashBoard', '0000-00-00 00:00:00'),
(2, 'Country', 'location', '0000-00-00 00:00:00'),
(3, 'Governorate', 'location', '0000-00-00 00:00:00'),
(4, 'Area', 'location', '0000-00-00 00:00:00'),
(5, 'DocSpecialization', 'Specialization', '0000-00-00 00:00:00'),
(6, 'DocSubSpecialization', 'Specialization', '0000-00-00 00:00:00'),
(7, 'CareSpecialization', 'Specialization', '0000-00-00 00:00:00'),
(8, 'PatientAdvertising', 'Advertising', '0000-00-00 00:00:00'),
(9, 'CareAdvertising', 'Advertising', '0000-00-00 00:00:00'),
(10, 'InsuranceCompany', 'InsuranceCompany', '0000-00-00 00:00:00'),
(11, 'Plan', 'Plan', '0000-00-00 00:00:00'),
(12, 'Patient', 'Patient', '0000-00-00 00:00:00'),
(13, 'Doctor_index', 'Doctor', '0000-00-00 00:00:00'),
(14, 'Doctor_create', 'Doctor', '0000-00-00 00:00:00'),
(15, 'Doctor_show', 'Doctor', '0000-00-00 00:00:00'),
(16, 'Doctor_edit', 'Doctor', '0000-00-00 00:00:00'),
(17, 'Care_index', 'Care', '0000-00-00 00:00:00'),
(18, 'Care_show', 'Care', '0000-00-00 00:00:00'),
(19, 'Care_create', 'Care', '0000-00-00 00:00:00'),
(20, 'Care_edit', 'Care', '0000-00-00 00:00:00'),
(21, 'CareSubServices', 'CareSubServices', '0000-00-00 00:00:00'),
(22, 'CareOffer', 'CareOffer', '0000-00-00 00:00:00'),
(23, 'NotificationCare', 'Notification', '0000-00-00 00:00:00'),
(24, 'NotificationPatient', 'Notification', '0000-00-00 00:00:00'),
(25, 'AboutUs', 'more', '2018-05-03 00:00:00'),
(26, 'ContactUs', 'more', '2018-05-03 00:00:00'),
(27, 'Setting', 'more', '2018-05-03 00:00:00'),
(28, 'Doctor_delete', 'Doctor', '0000-00-00 00:00:00'),
(29, 'Care_delete', 'Care', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `details_en` text NOT NULL,
  `details_ar` text NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `name_ar`, `name_en`, `details_en`, `details_ar`, `status`, `created_at`, `updated_at`) VALUES
(1, 'الخطة 1', 'plan 1', 'pay 10%', 'ادفع 10%', 1, '2018-04-24 13:15:30', '2018-05-08 16:20:03'),
(2, 'خطة 2', 'plan 2', 'pay with once', 'ادفع يالمره', 1, '2018-04-25 07:20:27', '2018-04-25 07:20:27');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `stars` int(11) NOT NULL,
  `comment` varchar(225) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `doctor_id`, `patient_id`, `stars`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, 89, 4, 'First Comment', '2018-04-16 00:00:00', '2018-04-30 00:00:00'),
(2, 1, 90, 3, 'second comments comments', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` int(11) NOT NULL,
  `doctor_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `type` enum('specific time','first reservation') NOT NULL,
  `time` time DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `doctor_id`, `patient_id`, `date`, `type`, `time`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-04-18', 'specific time', '10:42:00', '2018-04-18 00:00:00', '2018-04-25 00:00:00'),
(2, 1, 2, '2018-04-18', 'specific time', '15:42:00', '2018-04-18 00:00:00', '2018-04-26 00:00:00'),
(8, 1, 2, '2018-04-18', 'specific time', '10:12:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 1, 3, '2018-04-18', 'specific time', '13:42:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 1, 90, '2018-04-19', 'specific time', '20:19:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(225) NOT NULL,
  `comment` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `comment`, `created_at`, `updated_at`) VALUES
(3, 'super admin', 'have all premmions', '2018-05-02 08:56:27', '2018-05-02 08:56:27'),
(4, 'adverting', 'only for control adverting', '2018-05-02 12:15:10', '2018-05-02 12:15:10'),
(5, 'ddd', 'dddd', '2018-05-03 14:08:15', '2018-05-03 14:08:15');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `role_id`, `permission_id`) VALUES
(3, 3, 3),
(4, 3, 4),
(6, 3, 6),
(7, 3, 7),
(8, 3, 8),
(9, 3, 9),
(10, 3, 10),
(11, 3, 11),
(12, 3, 12),
(13, 3, 13),
(14, 3, 14),
(15, 3, 15),
(16, 3, 16),
(17, 3, 17),
(18, 3, 19),
(19, 3, 18),
(20, 3, 16),
(21, 3, 21),
(22, 3, 22),
(23, 3, 23),
(24, 3, 24),
(25, 3, 1),
(26, 3, 2),
(27, 3, 5),
(28, 4, 8),
(29, 4, 9),
(30, 5, 23),
(31, 5, 24),
(32, 5, 25),
(33, 5, 26),
(34, 5, 27);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `name_ar`, `name_en`, `status`, `created_at`, `updated_at`) VALUES
(1, 'خدمه 1', 'service 1', 1, '2018-04-04 00:00:00', '2018-04-04 00:00:00'),
(2, 'خدمه 2', 'service 2', 1, '2018-04-04 00:00:00', '2018-04-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(225) NOT NULL,
  `value` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `value`) VALUES
(1, 'first_page_adverting_image', '25638.PNG'),
(2, 'employee_email', 'mployee@e.com'),
(3, 'first_page_adverting_link', 'www.advertising.com');

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE `specialization` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(225) NOT NULL,
  `name_en` varchar(225) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `specialization`
--

INSERT INTO `specialization` (`id`, `name_ar`, `name_en`, `status`, `created_at`, `updated_at`) VALUES
(1, 'الاسبلاشن  1', 'spc1', 1, '2018-04-02 00:00:00', '2018-05-08 15:48:22'),
(2, 'sss', 'spc2', 0, '2018-04-02 00:00:00', '2018-05-08 16:00:24');

-- --------------------------------------------------------

--
-- Table structure for table `sub_specialization`
--

CREATE TABLE `sub_specialization` (
  `id` int(11) NOT NULL,
  `text_en` varchar(225) CHARACTER SET utf8 NOT NULL,
  `text_ar` varchar(225) CHARACTER SET utf8 NOT NULL,
  `specialization_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_specialization`
--

INSERT INTO `sub_specialization` (`id`, `text_en`, `text_ar`, `specialization_id`, `status`) VALUES
(1, 'sub spica 1', 'ساب تخصص 1', 2, 1),
(2, 'sub spical 2', 'صب تخصص 2', 1, 1),
(3, 'new sub spica', 'جديد  سب تخصص', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care`
--
ALTER TABLE `care`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_advertising`
--
ALTER TABLE `care_advertising`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_notification`
--
ALTER TABLE `care_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_offers`
--
ALTER TABLE `care_offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_pictures`
--
ALTER TABLE `care_pictures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_rating`
--
ALTER TABLE `care_rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_services`
--
ALTER TABLE `care_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_specialization`
--
ALTER TABLE `care_specialization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `care_sub_services`
--
ALTER TABLE `care_sub_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clicnic_address`
--
ALTER TABLE `clicnic_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clinic_picture`
--
ALTER TABLE `clinic_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_id` (`doctor_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_available_date`
--
ALTER TABLE `doctor_available_date`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_insurance_companies`
--
ALTER TABLE `doctor_insurance_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_services`
--
ALTER TABLE `doctor_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor_subspecialization`
--
ALTER TABLE `doctor_subspecialization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favourites`
--
ALTER TABLE `favourites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `governorate`
--
ALTER TABLE `governorate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `patient_advertising`
--
ALTER TABLE `patient_advertising`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_notification`
--
ALTER TABLE `patient_notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doctor_id` (`doctor_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `specialization`
--
ALTER TABLE `specialization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_specialization`
--
ALTER TABLE `sub_specialization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `care`
--
ALTER TABLE `care`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `care_advertising`
--
ALTER TABLE `care_advertising`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `care_notification`
--
ALTER TABLE `care_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `care_offers`
--
ALTER TABLE `care_offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `care_pictures`
--
ALTER TABLE `care_pictures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `care_rating`
--
ALTER TABLE `care_rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `care_services`
--
ALTER TABLE `care_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `care_specialization`
--
ALTER TABLE `care_specialization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `care_sub_services`
--
ALTER TABLE `care_sub_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `clicnic_address`
--
ALTER TABLE `clicnic_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `clinic_picture`
--
ALTER TABLE `clinic_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `doctor_available_date`
--
ALTER TABLE `doctor_available_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `doctor_insurance_companies`
--
ALTER TABLE `doctor_insurance_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `doctor_services`
--
ALTER TABLE `doctor_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `doctor_subspecialization`
--
ALTER TABLE `doctor_subspecialization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `favourites`
--
ALTER TABLE `favourites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `governorate`
--
ALTER TABLE `governorate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `patient_advertising`
--
ALTER TABLE `patient_advertising`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `patient_notification`
--
ALTER TABLE `patient_notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `specialization`
--
ALTER TABLE `specialization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sub_specialization`
--
ALTER TABLE `sub_specialization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
