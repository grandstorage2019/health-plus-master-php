<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'user'],function(){
    Route::post('/add_device_info', 'Api\UserController@add_device_info');
});

Route::group(['prefix'=>'ad'],function(){
    Route::get('/start_ad', 'Api\AdController@start_ad');
    Route::get('/close_ad', 'Api\AdController@close_ad');
});

Route::group(['prefix'=>'partner'],function(){
    Route::get('/', 'Api\PartnerController@index');
});

Route::group(['prefix'=>'distributionPlaces'],function(){
    Route::get('/', 'Api\DistributionPlacesController@index');
});

Route::group(['prefix'=>'magazines'],function(){
    Route::post('/list', 'Api\MagazineController@index');
    Route::get('/list/current_year/{page}/{limit}', 'Api\MagazineController@index_current_year');
    Route::get('/{id}', 'Api\MagazineController@show');
    Route::get('/increment_views/{id}', 'Api\MagazineController@increment_views');
    Route::post('/switch_Bookshelf', 'Api\MagazineController@switch_Bookshelf');
});

Route::group(['prefix'=>'bookshelf'],function(){
    Route::get('/list/{user_id}/{page}', 'Api\BookshelController@index');
    Route::post('/switch_Bookshelf', 'Api\BookshelController@switch_Bookshelf');
});

Route::group(['prefix'=>'contactUs'],function(){
    Route::post('/', 'Api\ContactUsController@store');
});

Route::group(['prefix'=>'popularQuestions'],function(){
    Route::get('/list/{page}/{limit}', 'Api\PopularQuestionsController@index');
});
