<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('test',function(){
  return '3ahsh ya wa7sh' ;
});

Route::get('sent_reservation_mails/{resrvation_id}', 'Api\SettingController@sent_reservation_mails');
Route::get('sent_care_reservation_mails/{care_resrvation_id}', 'Api\SettingController@sent_care_reservation_mails');

Route::get('cancle_reservations/{resrvation_id}', 'Api\SettingController@cancle_reservations');
Route::get('sent_cancle_care_reservation_mails/{resrvation_id}', 'Api\SettingController@sent_cancle_care_reservation_mails');



//
// Route::group(['prefix'=>'user'],function(){
//     Route::post('/add_device_info', 'Api\UserController@add_device_info');
// });
//
// Route::group(['prefix'=>'ad'],function(){
//     Route::get('/start_ad', 'Api\AdController@start_ad');
//     Route::get('/close_ad', 'Api\AdController@close_ad');
// });
//
// Route::group(['prefix'=>'partner'],function(){
//     Route::get('/', 'Api\PartnerController@index');
// });
//
// Route::group(['prefix'=>'distributionPlaces'],function(){
//     Route::get('/', 'Api\DistributionPlacesController@index');
// });
//
// Route::group(['prefix'=>'magazines'],function(){
//     Route::get('/download', 'Api\MagazineController@download');
//     Route::post('/list', 'Api\MagazineController@index');
//     Route::post('/list/by_year', 'Api\MagazineController@index_current_year');
//     Route::post('/show', 'Api\MagazineController@show');
//     Route::post('/increment_views', 'Api\MagazineController@increment_views');
//     Route::post('/add_Bookshelf', 'Api\MagazineController@switch_Bookshelf');
//     Route::get('/download_pdf', 'Api\MagazineController@download_pdf');
// });
//
// Route::group(['prefix'=>'bookshelf'],function(){
//     Route::post('/list', 'Api\BookshelController@index');
//     Route::post('/assign_Bookshelf', 'Api\BookshelController@assign_Bookshelf');
// });
//
// Route::group(['prefix'=>'contactUs'],function(){
//     Route::post('/', 'Api\ContactUsController@store');
// });
//
// Route::group(['prefix'=>'popularQuestions'],function(){
//     Route::post('/list', 'Api\PopularQuestionsController@index');
// });
//
// Route::group(['prefix'=>'setting'],function(){
//     Route::get('/who_we_are', 'Api\SettingController@who_we_are');
//     Route::get('/main_page', 'Api\SettingController@main_page');
// });
//
// Route::group(['prefix'=>'Favourite'],function(){
//     Route::post('/list', 'Api\FavouriteController@list');
//     Route::post('/switch_Favourite', 'Api\FavouriteController@switch_Favourite');
// });

// Route::group(['prefix'=>'AreaController'],function(){
//     Route::get('/list', 'Api\AreaController@index');
// });
