<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('Country');
});

Route::get('privacy_policy',function(){
  return view('privacy_policy');
});

Auth::routes();


//----login----
Route::get('/login', 'Auth\LoginController@admin_login_form')->name('login');
Route::post('/login', 'Auth\LoginController@admin_login');

Route::get('/setLang/{get_lang}', 'LocalizationController@setLang');

Route::get('/',function(){
  return view('landing.index');
});

Route::get('/home',function(){
    return redirect('Country');
});

Route::get('/DashBoard', 'DashBoardController@index');

//=================locations======================================
Route::group(['prefix'=>'Country'],function(){
    Route::get('/', 'CountryController@index');
    Route::get('/search/{val}', 'UserController@search');
    Route::post('/', 'CountryController@store');
    Route::post('/update', 'CountryController@update');
    Route::get('/showORhide/{id}', 'CountryController@showORhide');
});

Route::group(['prefix'=>'Governorate'],function(){
    Route::get('/', 'GovernorateController@index');
    Route::post('/', 'GovernorateController@store');
    Route::get('/get_by_country_id/{val}', 'GovernorateController@get_by_country_id');
});

Route::group(['prefix'=>'Area'],function(){
    Route::get('/', 'AreaController@index');
    Route::post('/', 'AreaController@store');
    Route::get('/get_governora_by_country_id_pluck/{country_id}', 'AreaController@get_governora_by_country_id_pluck');
    Route::get('/get_area_by_id/{governorate_id}', 'AreaController@get_area_by_id');
});
//=================End locations==================================

//=================Specializations==========================
Route::group(['prefix'=>'DocSpecialization'],function(){
    Route::get('/', 'DocSpecializationController@index');
    Route::get('/search/{val}', 'DocSpecializationController@search');
    Route::post('/', 'DocSpecializationController@store');
    Route::post('/update', 'DocSpecializationController@update');
    Route::get('/showORhide/{id}', 'DocSpecializationController@showORhide');
});

Route::group(['prefix'=>'DocSubSpecialization'],function(){
    Route::get('/', 'DocSubSpecializationController@index');
    Route::get('/search/{val}', 'DocSubSpecializationController@search');
    Route::post('/', 'DocSubSpecializationController@store');
    Route::post('/update', 'DocSubSpecializationController@update');
    Route::get('/showORhide/{id}', 'DocSubSpecializationController@showORhide');
});

Route::group(['prefix'=>'CareSpecialization'],function(){
    Route::get('/', 'CareSpecializationController@index');
    Route::get('/search/{val}', 'CareSpecializationController@search');
    Route::post('/', 'CareSpecializationController@store');
    Route::post('/update', 'CareSpecializationController@update');
    Route::get('/showORhide/{id}', 'CareSpecializationController@showORhide');
});

Route::group(['prefix'=>'CareSubServices'],function(){
    Route::get('/', 'CareSubServiceController@index');
    Route::post('/', 'CareSubServiceController@store');
    //--api---
    Route::get('/get_services/{care_id}', 'CareSubServiceController@get_services');
    Route::get('/get_CareSubService/{services_id}', 'CareSubServiceController@get_CareSubService');
});
//=================End Specializations==========================


//=================Advertising==========================
Route::group(['prefix'=>'PatientAdvertising'],function(){
    Route::get('/', 'PatientAdvertisingController@index');
    Route::get('/search/{val}', 'PatientAdvertisingController@search');
    Route::post('/', 'PatientAdvertisingController@store');
    Route::post('/update', 'PatientAdvertisingController@update');
    Route::get('/delete/{id}', 'PatientAdvertisingController@destroy');
});

Route::group(['prefix'=>'CareAdvertising'],function(){
    Route::get('/', 'CareAdvertisingController@index');
    Route::get('/search/{val}', 'CareAdvertisingController@search');
    Route::post('/', 'CareAdvertisingController@store');
    Route::post('/update', 'CareAdvertisingController@update');
    Route::get('/delete/{id}', 'CareAdvertisingController@destroy');
});
//=================End Advertising==========================

//=================End Insurance Company==========================
Route::group(['prefix'=>'InsuranceCompany'],function(){
    Route::get('/', 'InsuranceCompanyController@index');
    Route::get('/search/{val}', 'InsuranceCompanyController@search');
    Route::post('/', 'InsuranceCompanyController@store');
    Route::post('/update', 'InsuranceCompanyController@update');
    Route::get('/showORhide/{id}', 'InsuranceCompanyController@showORhide');
});
//=================End Insurance Company==========================

//=================End Plan==========================
Route::group(['prefix'=>'Plan'],function(){
    Route::get('/', 'PlanController@index');
    Route::get('/search/{val}', 'PlanController@search');
    Route::post('/', 'PlanController@store');
    Route::post('/update', 'PlanController@update');
    Route::get('/showORhide/{id}', 'PlanController@showORhide');
});
//=================End Plan==========================

//=================Patient==========================
Route::group(['prefix'=>'Patient'],function(){
    Route::get('/', 'PatientController@index');
    Route::get('/search/{val}', 'PatientController@search');
    Route::get('/{id}', 'PatientController@show');
    Route::get('showORhide/{id}', 'PatientController@showORhide');
});
//=================End Patient==========================

Route::group(['prefix'=>'Reservation'],function(){
     Route::get('/health', 'ReservationController@health_plus_index')->name('health_Reservation');
     Route::get('/health/search/{val}', 'ReservationController@health_plus_search') ;

     Route::get('/care', 'ReservationController@care_index')->name('care_Reservation');
     Route::get('/care/search/{val}', 'ReservationController@care_search') ;
     Route::get('/care_cancle/{id}', 'ReservationController@care_cancle') ;
});

Route::group(['prefix'=>'CanclePatient'],function(){
     Route::get('/', 'CanclePatientController@index');
     Route::get('/search/{val}', 'CanclePatientController@search');
     Route::get('/delete/{id}', 'CanclePatientController@destroy');
});

Route::group(['prefix'=>'CancleCa`re'],function(){
     Route::get('/', 'CancleCareController@index');
     Route::get('/search/{val}', 'CancleCareController@search');
     Route::get('/delete/{id}', 'CancleCareController@destroy');
});

//=================Doctor==========================
Route::group(['prefix'=>'Doctor'],function(){
    Route::get('/', 'DoctorController@index');
    Route::get('/create', 'DoctorController@create');
    Route::post('/', 'DoctorController@store');
    Route::get('/search/{val}', 'DoctorController@search');
    Route::get('/{id}', 'DoctorController@show');
    Route::get('edit/{id}', 'DoctorController@edit');
    Route::patch('/{id}', 'DoctorController@update');
    Route::get('delete/{id}', 'DoctorController@destroy');
    Route::get('reservations/{doctor_id}', 'DoctorController@reservations_page');
    Route::get('reservations/cancle/{reservation_id}', 'DoctorController@cancle_reservations');
    //--api--
    Route::get('get_SubSpecialization_api/{specId}', 'DocSubSpecializationController@get_subSpec_by_specId');
});

Route::group(['prefix'=>'DoctorServices'],function(){
     Route::get('/', 'DocServicesController@index');
     Route::post('/', 'DocServicesController@store');
});

//=================End Doctor==========================

//=================Care==========================
Route::group(['prefix'=>'Care'],function(){
    Route::get('/', 'CareController@index');
    Route::get('/create', 'CareController@create');
    Route::get('/edit/{id}', 'CareController@edit');
    Route::post('/', 'CareController@store');
    Route::patch('/{id}', 'CareController@update');
    Route::get('/search/{val}', 'CareController@search');
    Route::get('/{id}', 'CareController@show');
    Route::get('showORhide/{id}', 'CareController@showORhide');
    Route::get('delete/{id}', 'CareController@destroy');
});

Route::group(['prefix'=>'CareOffer'],function(){
    Route::get('/', 'CareOfferController@index');
    Route::post('/update', 'CareOfferController@update');
    Route::post('/', 'CareOfferController@store');
    Route::get('/search/{val}', 'CareOfferController@search');
    Route::get('/delete/{id}', 'CareOfferController@destroy');
    Route::get('/showORhide/{id}', 'CareOfferController@showORhide');
});

//=================End Doctor==========================

//=================NotificationCare==========================
Route::group(['prefix'=>'NotificationCare'],function(){
    Route::get('/', 'NotificationCareController@index');
    Route::post('/', 'NotificationCareController@store');
    Route::get('/search/{val}', 'NotificationCareController@search');
    Route::get('delete/{id}', 'NotificationCareController@destroy');
    Route::get('pushAgin/{id}', 'NotificationCareController@push_agin');
});

Route::group(['prefix'=>'NotificationPatient'],function(){
    Route::get('/', 'NotificationPatientController@index');
    Route::post('/', 'NotificationPatientController@store');
    Route::get('/search/{val}', 'NotificationPatientController@search');
    Route::get('delete/{id}', 'NotificationPatientController@destroy');
    Route::get('pushAgin/{id}', 'NotificationPatientController@push_agin');
});


//=================End NotificationCare==========================

//=================Role==========================
Route::group(['prefix'=>'Role'],function(){
    Route::resource('/', 'RoleController');
    Route::get('/edit/{id}', 'RoleController@edit');
    Route::patch('/{id}', 'RoleController@update');
    Route::get('/search/{val}', 'RoleController@search');
});

Route::group(['prefix'=>'Admin'],function(){
    Route::resource('/', 'AdminController');
    Route::get('/edit/{id}', 'AdminController@edit');
    Route::patch('/{id}', 'AdminController@update');
    Route::get('/search/{val}', 'AdminController@search');
    Route::get('/delete/{id}', 'AdminController@destroy');
});


//=================End Role==========================

//=================More==========================
Route::group(['prefix'=>'AboutUs'],function(){
    Route::resource('/', 'AboutUsController');
    Route::post('update', 'AboutUsController@update');
    Route::get('/search/{val}', 'AboutUsController@search');
    Route::get('/delete/{id}', 'AboutUsController@destroy');
});

Route::group(['prefix'=>'ContactUs'],function(){
    Route::get('/', 'ContactUsController@index');
    Route::get('/delete/{id}', 'ContactUsController@destroy');
});

Route::group(['prefix'=>'Setting'],function(){
    Route::get('/', 'SettingController@index');
    Route::post('/', 'SettingController@store');
});


Route::get('current_time',function(){
      return date("Y-m-d h:i:sa");
});

//=================End Role==========================
//
// Route::get('/demo', function () {
//   $reservation = \DB::table('care_resvation')
//               ->select('care.id as care_id','care.email as care_email','care.name_en as care_name','patients.id as patient_id','patients.email as patient_email',
//                         'patients.name as patient_name','care.phone as care_phone','patients.phone as patient_phone','care_resvation.id as reservation_id')
//               ->join('care_sub_services','care_sub_services.id','care_resvation.care_sub_services_id')
//               ->join('care_services','care_services.id','care_sub_services.service_id')
//                 ->join('care','care.id','care_services.care_id')
//                 ->join('patients','patients.id','care_resvation.patient_id')
//               ->where('care_resvation.id',1)
//               ->groupBy('care_resvation.id')
//               ->first();
//               // return '10';
//               // dd($reservation);
// // return $reservation;
//     return new App\Mail\CancleCareReservationMail($reservation);
// });
