<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Doctor;


class DoctorController extends Controller
{

    public function add_device_info(Request $request)
    {
          $data = \Validator::make($request->all(), [
            'firebase_token' => 'required', //verification
            'app_version'    => 'required',
            'model'          => 'required',
            'os'             => 'required',
         ]);
         if ($data->fails()) {
                return response()->json($data->messages(), 200);
         }

         $patient = Doctor::create( $request->all() );
         return $patient;
    }

    public function register(Request $request,$id)
    {
          $data = \Validator::make($request->all(), [
            'email' => 'required|email|unique:doctors',
            'password'  => 'required',
            'gender'    => 'required',
            'birth_day' => 'required|date',
            'phone'  => 'required',
            'country_id'  => 'required',
         ]);
         if ($data->fails()) {
                return response()->json($data->messages(), 200);
         }
         $request->request->add(['password' => \Hash::make($request->password)]); //dd($request->password );
         $request->request->add(['registred'=> 1 ]);
         $doctor = Doctor::findOrFail($id);
         $doctor->update( $request->all() );
         return $doctor;
    }

    public function email_signIn(Request $request )
    {
            $data = \Validator::make($request->all(), [
              'email' => 'required|email',
              'password'  => 'required',
           ]);
           if ($data->fails()) {
                  return response()->json($data->messages(), 200);
           }
           $doctor = Doctor::where('email',$request->email)->first();
           if (  $doctor  )
           {
                if ( \Hash::check($request->password,$doctor->password) )
                {
                    return response()->json(['id'=>$doctor->id]);
                }
                else { return 'false'; }
           }
           else { return 'false'; }
    }

    public function sign_by_facebook(Request $request ,$id)
    {
          $data = \Validator::make($request->all(), [
            'facebook_id'  => 'required|unique:patients,id,'.$id,
            'gender'    => 'required',
            'birth_day' => 'required|date',
            'phone'  => 'required',
            'country_id'  => 'required',
         ]);
         if ($data->fails()) {
                return response()->json($data->messages(), 200);
         }
         $patient = Patient::findOrFail($id);
         $request->request->add(['registred'=> 1 ]);
         $patient->update( $request->all() );
         return $patient;
    }

    public function sign_by_google(Request $request ,$id)
    {
          $data = \Validator::make($request->all(), [
            'google_id' => 'required|unique:patients,id,'.$id,
            'gender'    => 'required',
            'birth_day' => 'required',
            'phone'  => 'required',
            'country_id'  => 'required',
         ]);
         if ($data->fails()) {
                return response()->json($data->messages(), 200);
         }
         $patient = Patient::findOrFail($id);
         $request->request->add(['registred'=> 1 ]);
         $patient->update( $request->all() );
         return $patient;
    }

    


    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
